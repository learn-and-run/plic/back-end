package fr.eplicgames.back.score;

import fr.eplicgames.back.utils.Score;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ScoreTests {
    @Test
    public void ScoreIs100WhenSpendTimeIsLessThanExpectedTimeTest() {
        final int expected = 100;

        // Parameter is in milliseconds, here 20 sec
        final Integer spendTime = 20000;
        final Integer expectedTime = 30000;

        final int actual = Score.calculateScore(spendTime, expectedTime);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void ScoreIs50WhenSpendTimeIsDoubleThanExpectedTimeTest() {
        final int expected = 50;

        final Integer spendTime = 2;
        final Integer expectedTime = 1;

        final int actual = Score.calculateScore(spendTime, expectedTime);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void ScoreIs50WhenSpendTimeIsMoreThanDoubleThanExpectedTimeTest() {
        final int expected = 50;

        final Integer spendTime = 3;
        final Integer expectedTime = 1;

        final int actual = Score.calculateScore(spendTime, expectedTime);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void ScoreIs75WhenSpendTimeIsMoreThanExpectedTimeByHalfTimeTest() {
        final int expected = 75;

        final Integer spendTime = 15;
        final Integer expectedTime = 10;

        final int actual = Score.calculateScore(spendTime, expectedTime);

        Assert.assertEquals(expected, actual);
    }
}
