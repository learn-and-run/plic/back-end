package fr.eplicgames.back;

import org.junit.jupiter.api.Test;

import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;

@SpringBootTest
class BackApplicationTests {

	@Mock
	private DataSource ds;
	@Mock
	private Connection c;
	@Mock
	private PreparedStatement stmt;
	@Mock
	private ResultSet rs;

	@Test
	void contextLoads() {
		assertEquals(0, 0);
	}

}
