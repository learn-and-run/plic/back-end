create table room_types
(
    uuid uuid        not null
        constraint room_types_pk
            primary key,
    type varchar(16) not null
);

alter table room_types
    owner to wotluaxugsadcl;

create unique index room_types_type_uindex
    on room_types (type);

create unique index room_types_uuid_uindex
    on room_types (uuid);

create table scenes
(
    uuid uuid        not null
        constraint scenes_pk
            primary key,
    name varchar(32) not null
);

alter table scenes
    owner to wotluaxugsadcl;

create unique index scenes_name_uindex
    on scenes (name);

create unique index scenes_uuid_uindex
    on scenes (uuid);

create table character
(
    id          bigserial                                                       not null
        constraint character_pkey
            primary key,
    name        varchar(32)   default 'Jack'::character varying                 not null
        constraint uk_tkj83ytk14o17nvecy96gjik0
            unique,
    description varchar(1000) default 'no description'::character varying       not null,
    background  varchar(32)   default 'rgb(255,255,255,0.5)'::character varying not null,
    in_game     boolean       default false                                     not null,
    sorted      integer       default 0                                         not null,
    small_image varchar(256)                                                    not null,
    big_image   varchar(256)                                                    not null
);

alter table character
    owner to wotluaxugsadcl;

create table circuits
(
    id           bigserial    not null
        constraint circuits_pkey
            primary key,
    module       varchar(255),
    name         varchar(64)  not null
        constraint uk_e4sm0xfi5tgdg15hk97wb2cc2
            unique,
    picture_name varchar(264) not null,
    scene_uuid   uuid         not null
        constraint circuits_scene_uuid__fk
            references scenes
            on update cascade on delete restrict
);

alter table circuits
    owner to wotluaxugsadcl;

create table persistent_logins
(
    series    varchar(64) not null
        constraint persistent_logins_pkey
            primary key,
    last_used timestamp   not null,
    token     varchar(64) not null,
    username  varchar(64) not null
);

alter table persistent_logins
    owner to wotluaxugsadcl;

create table questions
(
    id          bigserial    not null
        constraint questions_pkey
            primary key,
    bad_answer  varchar(255),
    clue1       varchar(255),
    clue2       varchar(255),
    clue3       varchar(255),
    clue4       varchar(255),
    difficulty  integer      not null,
    explanation varchar(255) not null,
    level       varchar(255) not null,
    module      varchar(255) not null,
    name        varchar(128) not null,
    question    varchar(255) not null,
    response    varchar(256) not null
);

alter table questions
    owner to wotluaxugsadcl;

create table rooms
(
    id            bigserial         not null
        constraint rooms_pkey
            primary key,
    module        varchar(255)      not null,
    question_id   bigint            not null
        constraint fkods0a3oy5jxkwco2btckpim6e
            references questions,
    expected_time integer default 0 not null,
    type_uuid     uuid              not null
        constraint fk7lvly7f0s2chg3jxo6revp7dp
            references room_types
);

alter table rooms
    owner to wotluaxugsadcl;

create table circuits_rooms
(
    circuit_id bigint not null
        constraint fkghallc6554l0iwkccp1k4od6d
            references circuits,
    room_id    bigint not null
        constraint fkey1qc2iyhgd871kt673j7woy1
            references rooms,
    constraint circuits_rooms_pkey
        primary key (room_id, circuit_id)
);

alter table circuits_rooms
    owner to wotluaxugsadcl;

create table user_account
(
    id           bigserial             not null
        constraint user_account_pkey
            primary key,
    active       boolean default false not null,
    email        varchar(320)
        constraint uk_hl02wv5hym99ys465woijmfib
            unique,
    firstname    varchar(32)           not null,
    lastname     varchar(32)           not null,
    password     varchar(60)           not null,
    pseudo       varchar(32)           not null
        constraint uk_l3mjqdikxbc1m88nhv0siupnh
            unique,
    user_type    varchar(255)          not null,
    character_id bigint  default 1     not null
        constraint fk9yyq70vy0ijirem4agpn7xsyb
            references character
);

alter table user_account
    owner to wotluaxugsadcl;

create table activation_email
(
    id      varchar(36)  not null
        constraint activation_email_pkey
            primary key,
    email   varchar(320) not null,
    user_id bigint
        constraint fkrcrxn77c7j6yxl9mem898jabf
            references user_account on delete cascade
);

alter table activation_email
    owner to wotluaxugsadcl;

create table pending_game_session
(
    id         bigserial not null
        constraint pending_game_session_pkey
            primary key,
    enter_time timestamp,
    circuit_id bigint    not null
        constraint fkknbf632c370a29ek2o2ojbkmd
            references circuits,
    room_id    bigint
        constraint fkdn52gfdtc904qijk9t7q0kqn6
            references rooms,
    user_id    bigint    not null
        constraint uk_88aibh7p7wku2bongklkk20pe
            unique
        constraint fkgb074bpeco1wjjwo8ygawk51t
            references user_account on delete cascade,
    spend_time timestamp
);

alter table pending_game_session
    owner to wotluaxugsadcl;

create table reset_password
(
    id      varchar(36) not null
        constraint reset_password_pkey
            primary key,
    user_id bigint
        constraint fk5chl4uq6ua30pb0gdgoh5agyo
            references user_account on delete cascade
);

alter table reset_password
    owner to wotluaxugsadcl;

create table scores
(
    id            bigserial                     not null
        constraint scores_pkey
            primary key,
    best_score    integer default 0             not null,
    current_score integer default 0             not null,
    circuit_id    bigint                        not null
        constraint fklgt8vyx464xmdpuby23nu14v4
            references circuits on delete cascade,
    user_id       bigint                        not null
        constraint fk8kekloxdhtyq1s6mx51g4d60w
            references user_account on delete cascade,
    spend_time    integer default 0             not null,
    best_time     integer default '-1'::integer not null
);

alter table scores
    owner to wotluaxugsadcl;



create table user_admin
(
    user_id bigint not null
        constraint user_admin_pkey
            primary key
        constraint fk3j39pigemx88dvw0pri0c8c83
            references user_account on delete cascade
);

alter table user_admin
    owner to wotluaxugsadcl;

create table user_invitation
(
    id          bigint not null
        constraint user_invitation_pkey
            primary key,
    relation_id bigint
        constraint fk4f2cw4j68jhwnni371ol5d2rs
            references user_account on delete cascade,
    user_id     bigint
        constraint fk5qimloud0wcgqcah4dwwo0rml
            references user_account on delete cascade
);

alter table user_invitation
    owner to wotluaxugsadcl;

create table user_parent
(
    user_id bigint not null
        constraint user_parent_pkey
            primary key
        constraint fks32f61a4b6kpqcisrbt3b3579
            references user_account on delete cascade
);

alter table user_parent
    owner to wotluaxugsadcl;

create table user_professor
(
    user_id bigint not null
        constraint user_professor_pkey
            primary key
        constraint fkl0hw91h0eb7ofv4aer7fn8ixd
            references user_account on delete cascade
);

alter table user_professor
    owner to wotluaxugsadcl;

create table user_relation
(
    id          bigint not null
        constraint user_relation_pkey
            primary key,
    relation_id bigint
        constraint fko3j36a9p4g6s6ur1lsum1l81q
            references user_account on delete cascade,
    user_id     bigint
        constraint fklte3lpp8q8lcukw38l886eaau
            references user_account on delete cascade
);

alter table user_relation
    owner to wotluaxugsadcl;

create table user_student
(
    user_id    bigint       not null
        constraint user_student_pkey
            primary key
        constraint fkikauh5heg4m7w4p21d6r7c2nm
            references user_account on delete cascade,
    level_type varchar(255) not null
);

alter table user_student
    owner to wotluaxugsadcl;

create table game_preferences
(
    id                   bigserial                                      not null
        constraint game_preferences_pk
            primary key,
    controller_dualshock boolean    default false                       not null,
    identity             varchar(9) default 'PSEUDO'::character varying not null,
    retro                boolean    default false                       not null,
    user_id              bigint                                         not null
        constraint fk4ou5lswqkf0uf6qduah8b5hx7
            references user_account on delete cascade
);

alter table game_preferences
    owner to wotluaxugsadcl;

create unique index game_preferences_id_uindex
    on game_preferences (id);

create table objects
(
    uuid    uuid                  not null
        constraint objects_pk
            primary key,
    name    varchar(32)           not null,
    folder  varchar(128)          not null,
    in_game boolean default false not null
);

alter table objects
    owner to wotluaxugsadcl;

create unique index objects_name_uindex
    on objects (name);

create unique index objects_uuid_uindex
    on objects (uuid);

create table rooms_objects
(
    uuid        uuid                       not null
        constraint rooms_objects_pk
            primary key,
    room_id     bigint                     not null
        constraint rooms_objects_room_id__fk
            references rooms
            on update cascade on delete cascade,
    object_uuid uuid                       not null
        constraint rooms_objects_object_uuid__fk
            references objects
            on update cascade on delete cascade,
    position_x  double precision default 0 not null,
    position_y  double precision default 0 not null,
    position_z  double precision default 0 not null,
    rotation_x  double precision default 0 not null,
    rotation_y  double precision default 0 not null,
    rotation_z  double precision default 0 not null
);

alter table rooms_objects
    owner to wotluaxugsadcl;

create unique index rooms_objects_uuid_uindex
    on rooms_objects (uuid);
