package fr.eplicgames.back.exception;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Abstract class to handle CodeMessageException and write error
 */
public abstract class ExceptionController {

    @ExceptionHandler(CodeMessageException.class)
    public void handle(HttpServletRequest request, HttpServletResponse response, CodeMessageException exception) throws IOException {
        writeError(request, response, exception.getError());
    }

    public static void writeError(HttpServletRequest request, HttpServletResponse response, CodeMessageExceptionEnum codeMessageExceptionEnum) throws IOException {
        int code = codeMessageExceptionEnum.getCode();
        response.setStatus(code);

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
        data.put("status", code);
        data.put("error", HttpStatus.valueOf(code).getReasonPhrase());
        data.put("message", codeMessageExceptionEnum.getMessage());
        data.put("path", request.getRequestURI());

        response.getOutputStream().println(new ObjectMapper().writeValueAsString(data));
    }

}
