package fr.eplicgames.back.exception;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.servlet.http.HttpServletResponse;

/**
 * Custom enumeration for CodeMessageException
 */
@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public enum CodeMessageExceptionEnum {

    CHARACTER_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This character does not exist"),
    PSEUDO_ALREADY_TAKEN(HttpServletResponse.SC_CONFLICT, "This pseudo is already taken"),
    EMAIL_ALREADY_USED(HttpServletResponse.SC_CONFLICT, "This email is already used"),
    CIRCUIT_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This circuit name already exist"),
    CIRCUIT_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This circuit id does not exist"),
    CIRCUIT_ALREADY_CONTAINS_ROOM(HttpServletResponse.SC_CONFLICT, "This circuit already contains this room"),
    CIRCUIT_DOES_NOT_CONTAIN_ROOM(HttpServletResponse.SC_CONFLICT, "This circuit does not contain this room"),
    QUESTION_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This question name already exist"),
    QUESTION_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This question id does not exist"),
    ROOM_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This room already exist by question id"),
    ROOM_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This room does not exist"),
    GAME_SESSION_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "No game session exists"),
    WRONG_ACTIVATION_LINK(HttpServletResponse.SC_NOT_FOUND, "This activation link is unavailable"),
    WRONG_RESET_PASSWORD_LINK(HttpServletResponse.SC_NOT_FOUND, "This reset password link is unavailable"),
    ERROR_SENDING_EMAIL(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An error occurred trying sending the email"),
    USER_NOT_LOGIN(HttpServletResponse.SC_UNAUTHORIZED, "You must be authenticated !"),
    WRONG_CREDENTIALS(HttpServletResponse.SC_BAD_REQUEST, "Bad credentials !"),
    ILLEGAL_ACCESS(HttpServletResponse.SC_FORBIDDEN, "You cannot be here !"),
    SESSION_EXPIRED(HttpServletResponse.SC_GONE, "Your session has expired !"),
    MISSING_CLASS_STUDENT(HttpServletResponse.SC_BAD_REQUEST, "Error, missing level class for student creation"),
    UNKNOWN_RELATION(HttpServletResponse.SC_NOT_FOUND, "This relation does not exist"),
    UNKNOWN_INVITATION(HttpServletResponse.SC_NOT_FOUND, "This invitation does not exist"),
    UNKNOWN_USER(HttpServletResponse.SC_NOT_FOUND, "This user does not exist"),
    INVITATION_TO_YOURSELF(HttpServletResponse.SC_BAD_REQUEST, "You cannot add yourself"),
    RELATION_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This relation already exists"),
    INVITATION_ALREADY_SEND(HttpServletResponse.SC_CONFLICT, "This invitation has been already send"),
    OBJECT_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This object name already exists"),
    OBJECT_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This object does not exist"),
    SCENE_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This scene name already exists"),
    SCENE_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This scene does not exist"),
    ROOM_TYPE_ALREADY_EXIST(HttpServletResponse.SC_CONFLICT, "This room type name already exists"),
    ROOM_TYPE_NOT_EXIST(HttpServletResponse.SC_BAD_REQUEST, "This room type does not exist");

    final Integer code;
    final String message;
}
