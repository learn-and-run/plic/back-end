package fr.eplicgames.back.view.game;

import fr.eplicgames.back.domain.entity.GamePreferenceEntity;
import fr.eplicgames.back.domain.service.GamePreferenceService;
import fr.eplicgames.back.domain.service.PendingGameSessionService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.game.dto.GetGamePreferencesDtoResponse;
import fr.eplicgames.back.view.game.dto.UpdateGamePreferencesDtoRequest;
import fr.eplicgames.back.view.game.dto.UpdateGamePreferencesDtoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

/**
 * REST Controller for game interactions
 */
@RestController
@Tag(name = "game", description = "the Game API")
@RequestMapping(path = "/api/game")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameController extends ExceptionController {

    final GamePreferenceService gamePreferenceService;
    final PendingGameSessionService pendingGameSessionService;

    @Operation(
            summary = "Enter in a circuit",
            description = "Enter in a circuit if it exist and create a session to it.",
            tags = {"game"}
    )
    @PostMapping("/circuit/enter/{circuitId}")
    public void enterCircuit(@PathVariable("circuitId") final Long circuitId) throws CodeMessageException {
        pendingGameSessionService.enterCircuit(circuitId);
    }

    @Operation(
            summary = "Quit a circuit",
            description = "Quit a circuit if it exist and remove its game session.",
            tags = {"game"}
    )
    @DeleteMapping("/circuit/quit")
    public void quitCircuit() throws CodeMessageException {
        pendingGameSessionService.leaveGameSession();
    }

    @Operation(
            summary = "Finish a circuit and update best score",
            description = "Finish a circuit if it exist, remove its game session and update best score " +
                    "if current score is higher.",
            tags = {"game"}
    )
    @DeleteMapping("/circuit/finish")
    public void finishCircuit() throws CodeMessageException {
        pendingGameSessionService.updateBestScore();
        pendingGameSessionService.updateBestTime();
        pendingGameSessionService.leaveGameSession();
    }

    @Operation(
            summary = "Enter in a room",
            description = "Enter in a room if it exist and assign a timestamp",
            tags = {"game"}
    )
    @PostMapping("/room/enter/{roomId}")
    public void enterRoom(@PathVariable("roomId") final Long roomId) throws CodeMessageException {
        pendingGameSessionService.enterRoom(roomId);
    }

    @Operation(
            summary = "Leave a room",
            description = "Leave a room and calculate the score thanks to the time and boolean value",
            tags = {"game"}
    )
    @PostMapping("/room/finish/{success}")
    public void enterRoom(@PathVariable("success") final Boolean success) throws CodeMessageException {
        pendingGameSessionService.leaveRoomAndCalculateScore(success);
    }

    @Operation(
            summary = "Get user game preferences",
            description = "Get all preferences of the current user in the game",
            tags = {"game"}
    )
    @GetMapping("/preferences")
    public GetGamePreferencesDtoResponse getGamePreferences() throws CodeMessageException {
        final GamePreferenceEntity gamePreferenceEntity = gamePreferenceService.getGamePreferences();
        return new GetGamePreferencesDtoResponse(
                gamePreferenceEntity.controllerDualShock,
                gamePreferenceEntity.retro,
                gamePreferenceEntity.identity
        );
    }

    @Operation(
            summary = "Update user game preferences",
            description = "Update all preferences of the current user in the game and returned new preferences." +
                    "Identity must be PSEUDO, FIRSTNAME or FULLNAME",
            tags = {"game"}
    )
    @PatchMapping("/preferences")
    public UpdateGamePreferencesDtoResponse updateGamePreferences(
            final @RequestBody UpdateGamePreferencesDtoRequest updateGamePreferencesDtoRequest
    ) throws CodeMessageException {

        final GamePreferenceEntity newGamePreferenceEntity = new GamePreferenceEntity(
                null,
                null,
                updateGamePreferencesDtoRequest.controllerDualShock4,
                updateGamePreferencesDtoRequest.retro,
                updateGamePreferencesDtoRequest.identity
        );

        final GamePreferenceEntity gamePreferenceEntity = gamePreferenceService.updateGamePreferences(
                newGamePreferenceEntity
        );

        return new UpdateGamePreferencesDtoResponse(
                gamePreferenceEntity.controllerDualShock,
                gamePreferenceEntity.retro,
                gamePreferenceEntity.identity
        );
    }
}
