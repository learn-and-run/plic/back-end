package fr.eplicgames.back.view.game.dto;

import fr.eplicgames.back.sql_type.IdentityType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateGamePreferencesDtoRequest {
    boolean controllerDualShock4;
    boolean retro;
    IdentityType identity;
}
