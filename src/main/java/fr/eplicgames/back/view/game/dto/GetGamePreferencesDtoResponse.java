package fr.eplicgames.back.view.game.dto;

import fr.eplicgames.back.sql_type.IdentityType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetGamePreferencesDtoResponse {
    final boolean controllerDualShock4;
    final boolean retro;
    final IdentityType identity;
}
