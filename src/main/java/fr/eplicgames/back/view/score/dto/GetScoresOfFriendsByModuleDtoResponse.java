package fr.eplicgames.back.view.score.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * Score info Data Transfer Object to get scores of friends by module
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetScoresOfFriendsByModuleDtoResponse {

    final List<FriendDtoResponse> friends;
    final ModuleType moduleType;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class FriendDtoResponse {
        final Integer ladder;
        final String pseudo;
        final Integer average;
        final List<CircuitDtoResponse> circuits;

        @AllArgsConstructor
        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class CircuitDtoResponse {
            final String name;
            final Integer bestScore;

            final Integer spendTime;
            final Integer bestTime;
        }
    }
}
