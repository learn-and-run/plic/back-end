package fr.eplicgames.back.view.score.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Score info Data Transfer Object to get scores from front
 */
@Data
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetScoreOfPseudoOnCircuitDtoRequest {
    @Size(max = 32)
    @Pattern(regexp = "^[^@]+$")
    String pseudo;

    @Size(max = 64)
    String circuitName;
}
