package fr.eplicgames.back.view.score;

import fr.eplicgames.back.domain.entity.FriendScoreEntity;
import fr.eplicgames.back.domain.entity.ScoreEntity;
import fr.eplicgames.back.domain.service.ScoreService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.view.score.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST Controller for score
 */
@RestController
@Tag(name = "score", description = "the Score API")
@RequestMapping(path = "/api/score")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ScoreController {

    final ScoreService scoreService;

    @Operation(
            summary = "Get scores of the specific user on the specific circuit.",
            description = "Get scores of the specific user on the specific circuit.",
            tags = {"score"}
    )
    @PostMapping("")
    public GetScoreOfPseudoOnCircuitDtoResponse getScoresOfPseudoOnCircuit(
            @RequestBody @Valid final GetScoreOfPseudoOnCircuitDtoRequest getScoreOfPseudoOnCircuitDtoRequest
    ) throws CodeMessageException {

        final ScoreEntity scoreEntity = scoreService.getScoresOfPseudoOnCircuit(
                getScoreOfPseudoOnCircuitDtoRequest.pseudo,
                getScoreOfPseudoOnCircuitDtoRequest.circuitName
        );

        return new GetScoreOfPseudoOnCircuitDtoResponse(
                scoreEntity.circuitModel.getId(),
                scoreEntity.bestScore,
                scoreEntity.currentScore,
                scoreEntity.spendTime,
                scoreEntity.bestTime
        );
    }

    @Operation(
            summary = "Get scores of the specific user on a global module.",
            description = "Get scores of the specific user on a global module.",
            tags = {"score"}
    )
    @GetMapping("/module")
    public GetScoreByModuleAndPseudoDtoResponse getScoreByModuleAndPseudo(
            final @Valid GetScoreByModuleAndPseudoDtoRequest getScoreByModuleAndPseudoDtoRequest
    ) throws CodeMessageException {
        final List<ScoreEntity> scoreEntities = scoreService.getScoreByPseudoAndModule(
                getScoreByModuleAndPseudoDtoRequest.getPseudo(),
                getScoreByModuleAndPseudoDtoRequest.getModuleType()
        );

        return new GetScoreByModuleAndPseudoDtoResponse(scoreEntities.stream().map(scoreEntity ->
                new GetScoreByModuleAndPseudoDtoResponse.ScoreDtoResponse(
                        scoreEntity.circuitModel.getId(),
                        scoreEntity.circuitModel.getName(),
                        scoreEntity.circuitModel.getModule(),
                        scoreEntity.bestScore,
                        scoreEntity.spendTime,
                        scoreEntity.bestTime
                )).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Get all scores of all friends ordered by ladder classement on the specific module.",
            description = "Get all scores of all friends ordered by ladder classement on the specific module.",
            tags = {"score"}
    )
    @GetMapping("/friends")
    public GetScoresOfFriendsByModuleDtoResponse getScoresOfFriendsByModule(final @Valid ModuleType moduleType) throws CodeMessageException {
        final List<FriendScoreEntity> friendScoreEntities = scoreService.getFriendsScoreOrderedByModule(moduleType);

        final List<GetScoresOfFriendsByModuleDtoResponse.FriendDtoResponse> friendsDtoResponse = new ArrayList<>();

        for (int i = 0; i < friendScoreEntities.size(); ++i)
            friendsDtoResponse.add(new GetScoresOfFriendsByModuleDtoResponse.FriendDtoResponse(
                    i + 1,
                    friendScoreEntities.get(i).pseudo,
                    friendScoreEntities.get(i).average,
                    friendScoreEntities.get(i).scores.stream().map(score ->
                            new GetScoresOfFriendsByModuleDtoResponse.FriendDtoResponse.CircuitDtoResponse(
                                    score.circuitName,
                                    score.bestScore,
                                    score.spendTime,
                                    score.bestTime
                            )).collect(Collectors.toList())

            ));

        return new GetScoresOfFriendsByModuleDtoResponse(friendsDtoResponse, moduleType);
    }
}
