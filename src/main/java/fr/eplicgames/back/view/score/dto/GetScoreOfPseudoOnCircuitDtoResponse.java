package fr.eplicgames.back.view.score.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * Score info Data Transfer Object to get scores to front
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetScoreOfPseudoOnCircuitDtoResponse {
    Long circuitId;

    Integer bestScore;
    Integer currentScore;

    Integer spendTime;
    Integer bestTime;
}
