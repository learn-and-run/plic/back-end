package fr.eplicgames.back.view.score.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetScoreByModuleAndPseudoDtoResponse {
    final List<ScoreDtoResponse> scores;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class ScoreDtoResponse {
        final Long circuitId;
        final String circuitName;
        final ModuleType moduleType;
        final Integer score;
        final Integer spendTime;
        final Integer bestTime;
    }
}
