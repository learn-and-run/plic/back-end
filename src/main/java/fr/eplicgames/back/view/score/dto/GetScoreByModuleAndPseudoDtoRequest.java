package fr.eplicgames.back.view.score.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Score info Data Transfer Object to get scores by module
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GetScoreByModuleAndPseudoDtoRequest {

    @Size(max = 32)
    @Pattern(regexp = "^[^@]+$")
    String pseudo;

    @NotNull
    ModuleType moduleType;
}
