package fr.eplicgames.back.view.relation.dto;

import fr.eplicgames.back.domain.entity.relation.RelationEntity;
import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetRelationsDtoResponse {

    final List<RelationDtoResponse> relations;

    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class RelationDtoResponse {

        final Long id;
        final UserDtoResponse user;
        final UserType userType;

        public RelationDtoResponse(RelationEntity relationEntity) {
            this.id = relationEntity.id;
            this.userType = relationEntity.userEntity.userType;

            switch (userType) {
                case STUDENT:
                    user = new StudentDtoResponse((StudentEntity) relationEntity.userEntity);
                    break;
                case PARENT:
                    user = new ParentDtoResponse((ParentEntity) relationEntity.userEntity);
                    break;
                case PROFESSOR:
                    user = new ProfessorDtoResponse((ProfessorEntity) relationEntity.userEntity);
                    break;
                default:
                    user = null;
            }
        }

        @FieldDefaults(level = AccessLevel.PUBLIC)
        public abstract static class UserDtoResponse {

            final String pseudo;
            final String firstname;
            final String lastname;
            final String email;
            final UserType userType;
            final CharacterDtoResponse character;

            public UserDtoResponse(final UserEntity userEntity) {
                this.pseudo = userEntity.pseudo;
                this.firstname = userEntity.firstname;
                this.lastname = userEntity.lastname;
                this.email = userEntity.email;
                this.userType = userEntity.userType;
                this.character = new CharacterDtoResponse(userEntity.character.id, userEntity.character.name);
            }

            @AllArgsConstructor
            @FieldDefaults(level = AccessLevel.PUBLIC)
            public static class CharacterDtoResponse {

                final Long id;
                final String name;
            }
        }

        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class StudentDtoResponse extends UserDtoResponse {

            final LevelType levelType;

            public StudentDtoResponse(final StudentEntity studentEntity) {
                super(studentEntity);
                this.levelType = studentEntity.levelType;
            }
        }

        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class ParentDtoResponse extends UserDtoResponse {

            public ParentDtoResponse(final ParentEntity parentEntity) {
                super(parentEntity);
            }
        }

        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class ProfessorDtoResponse extends UserDtoResponse {

            public ProfessorDtoResponse(final ProfessorEntity professorEntity) {
                super(professorEntity);
            }
        }
    }
}
