package fr.eplicgames.back.view.relation;

import fr.eplicgames.back.domain.entity.relation.InvitationEntity;
import fr.eplicgames.back.domain.entity.relation.RelationEntity;
import fr.eplicgames.back.domain.service.RelationService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.sql_type.UserType;
import fr.eplicgames.back.view.relation.dto.AcceptInvitationDtoRequest;
import fr.eplicgames.back.view.relation.dto.GetPendingInvitationsDtoResponse;
import fr.eplicgames.back.view.relation.dto.GetRelationsDtoResponse;
import fr.eplicgames.back.view.relation.dto.SendInvitationDtoRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST Controller for user relation
 */
@RestController
@Tag(name = "user", description = "the User API")
@RequestMapping(path = "/api/user/relation")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RelationController extends ExceptionController {

    final RelationService relationService;

    @Operation(
            summary = "Send an invitation to another user",
            description = "Send an invitation to another user",
            tags = {"user"}
    )
    @PostMapping("/add")
    public void sendInvitation(
            @RequestBody @Valid final SendInvitationDtoRequest sendInvitationDtoRequest
    ) throws CodeMessageException {
        relationService.sendInvitation(sendInvitationDtoRequest.username);
    }

    @Operation(
            summary = "Get all pending invitations",
            description = "Get all pending invitations",
            tags = {"user"}
    )
    @GetMapping("/pending")
    public GetPendingInvitationsDtoResponse getPendingInvitations() {
        final List<InvitationEntity> invitationEntities = relationService.getPendingInvitations();

        return new GetPendingInvitationsDtoResponse(
                invitationEntities.stream().map(invitationEntity ->
                        new GetPendingInvitationsDtoResponse.PendingInvitationDtoResponse(
                                invitationEntity.id,
                                new GetPendingInvitationsDtoResponse
                                        .PendingInvitationDtoResponse
                                        .UserDtoResponse(
                                        invitationEntity.userEntity.pseudo,
                                        invitationEntity.userEntity.firstname,
                                        invitationEntity.userEntity.lastname,
                                        invitationEntity.userEntity.email,
                                        invitationEntity.userEntity.userType,
                                        new GetPendingInvitationsDtoResponse
                                                .PendingInvitationDtoResponse
                                                .UserDtoResponse
                                                .CharacterDtoResponse(
                                                invitationEntity.userEntity.character.id,
                                                invitationEntity.userEntity.character.name
                                        )
                                ),
                                invitationEntity.userEntity.userType)
                ).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Validate one invitation",
            description = "Validate one invitation",
            tags = {"user"}
    )
    @PostMapping("/validate")
    public void acceptInvitation(
            @RequestBody @Valid final AcceptInvitationDtoRequest acceptInvitationDtoRequest
    ) throws CodeMessageException {
        relationService.acceptInvitation(acceptInvitationDtoRequest.id);
    }

    @Operation(
            summary = "Delete an invitation",
            description = "Delete an invitation by giving its id",
            tags = {"user"}
    )
    @DeleteMapping("/decline/{invitationId}")
    public void declineInvitation(@PathVariable("invitationId") final Long invitationId) throws CodeMessageException {
        relationService.removeInvitation(invitationId);
    }

    @Operation(
            summary = "Get all the current relation for the current user",
            description = "Get all the current relation for the current user",
            tags = {"user"}
    )
    @GetMapping("/all")
    public GetRelationsDtoResponse getRelations(final UserType userType) {
        final List<RelationEntity> relationEntities = relationService.getRelations(userType);
        return new GetRelationsDtoResponse(
                relationEntities
                        .stream()
                        .map(GetRelationsDtoResponse.RelationDtoResponse::new)
                        .collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Delete an existing relation",
            description = "Delete an existing relation with the specific relation id in both ways",
            tags = {"user"}
    )
    @DeleteMapping("/delete/{relationId}")
    public void deleteRelation(@PathVariable("relationId") final Long relationId) throws CodeMessageException {
        relationService.removeRelation(relationId);
    }
}
