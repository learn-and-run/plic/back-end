package fr.eplicgames.back.view.relation.dto;

import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetPendingInvitationsDtoResponse {
    final List<PendingInvitationDtoResponse> pendingInvitations;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class PendingInvitationDtoResponse {
        final Long id;
        final UserDtoResponse user;
        final UserType userType;

        @AllArgsConstructor
        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class UserDtoResponse {
            final String pseudo;
            final String firstname;
            final String lastname;
            final String email;
            final UserType userType;
            final CharacterDtoResponse character;

            @AllArgsConstructor
            @FieldDefaults(level = AccessLevel.PUBLIC)
            public static class CharacterDtoResponse {
                Long id;
                String name;
            }
        }
    }
}
