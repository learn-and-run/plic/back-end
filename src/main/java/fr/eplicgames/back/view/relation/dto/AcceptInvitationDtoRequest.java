package fr.eplicgames.back.view.relation.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class AcceptInvitationDtoRequest {
    @NotNull
    Long id;
}
