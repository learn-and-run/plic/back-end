package fr.eplicgames.back.view.content.circuit.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateCircuitDtoRequest {
    @NotNull
    Long id;
    @NotBlank
    @Size(max = 64)
    String name;
    @NotBlank
    @Size(max = 264)
    String pictureName;
    @NotNull
    ModuleType module;
    @NotNull
    UUID scene_uuid;
}
