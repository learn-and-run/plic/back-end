package fr.eplicgames.back.view.content.roomtype.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateRoomTypeDtoResponse {
    final UUID uuid;
    final String type;
}
