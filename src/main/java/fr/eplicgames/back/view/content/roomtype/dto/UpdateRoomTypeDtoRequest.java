package fr.eplicgames.back.view.content.roomtype.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateRoomTypeDtoRequest {
    @NotNull
    UUID uuid;

    @NotBlank
    @Size(max = 16)
    String type;
}
