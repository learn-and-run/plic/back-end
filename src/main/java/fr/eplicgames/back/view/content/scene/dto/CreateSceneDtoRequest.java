package fr.eplicgames.back.view.content.scene.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateSceneDtoRequest {
    @NotBlank
    @Size(max = 32)
    String name;
}
