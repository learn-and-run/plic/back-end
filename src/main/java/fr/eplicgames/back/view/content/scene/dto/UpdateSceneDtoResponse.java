package fr.eplicgames.back.view.content.scene.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateSceneDtoResponse {
    final UUID uuid;
    final String name;
}
