package fr.eplicgames.back.view.content.circuit;

import fr.eplicgames.back.domain.entity.ScoreEntity;
import fr.eplicgames.back.domain.entity.content.CircuitEntity;
import fr.eplicgames.back.domain.entity.content.SceneEntity;
import fr.eplicgames.back.domain.service.ScoreService;
import fr.eplicgames.back.domain.service.content.CircuitService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.view.content.circuit.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST Controller for circuit
 */
@RestController
@Tag(name = "circuit", description = "the Circuit API")
@RequestMapping(path = "/api/circuit")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CircuitController extends ExceptionController {

    final CircuitService circuitService;
    // TODO: remove it from controller when the project will become in better architecture
    final UserRepository userRepository;
    final ScoreService scoreService;

    @Operation(
            summary = "Get all circuits associated to a specific module",
            description = "Get all circuits associated to a specific module that can be GENERAL, MATHS, FRANCAIS, " +
                    "SVT, PHYSIQUE, CHIMIE, ANGLAIS, HISTOIRE or GEOGRAPHIE or TOUS for all circuits",
            tags = {"circuit"}
    )
    @GetMapping("/all/{module}")
    public GetAllCircuitByModuleDtoResponse getAllCircuitByModule(@PathVariable("module") final ModuleType module) {

        final List<CircuitEntity> circuitEntities = circuitService.getAllCircuitOfModule(module);

        final Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getName());
        final String pseudo = userRepository.findById(userId).orElseThrow(IllegalArgumentException::new).getPseudo();

        return new GetAllCircuitByModuleDtoResponse(
                circuitEntities.stream().map(circuitEntity -> {
                            ScoreEntity scores;
                            try {
                                scores = scoreService.getScoresOfPseudoOnCircuit(
                                        pseudo,
                                        circuitEntity.name
                                );
                            } catch (final CodeMessageException e) {
                                scores = new ScoreEntity(
                                        pseudo,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null
                                );
                            }

                            return new GetAllCircuitByModuleDtoResponse.CircuitDtoResponse(
                                    circuitEntity.id,
                                    circuitEntity.name,
                                    circuitEntity.pictureName,
                                    circuitEntity.module,
                                    circuitEntity.scene.name,
                                    circuitEntity.scene.uuid,
                                    scores.bestScore,
                                    scores.spendTime
                            );
                        }
                ).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Create a circuit",
            description = "Create a circuit if the name does not exist.",
            tags = {"circuit"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateCircuitDtoResponse createCircuit(
            @RequestBody @Valid final CreateCircuitDtoRequest createCircuitDtoRequest
    ) throws CodeMessageException {
        final CircuitEntity circuitEntity = new CircuitEntity(
                null,
                createCircuitDtoRequest.name,
                createCircuitDtoRequest.pictureName,
                createCircuitDtoRequest.module,
                new SceneEntity(
                        createCircuitDtoRequest.scene_uuid,
                        null
                ),
                null
        );

        final CircuitEntity createdCircuitEntity = circuitService.createCircuitIfNotExist(circuitEntity);
        return new CreateCircuitDtoResponse(
                createdCircuitEntity.id,
                createdCircuitEntity.name,
                createdCircuitEntity.pictureName,
                createdCircuitEntity.module,
                createdCircuitEntity.scene.name,
                createdCircuitEntity.scene.uuid
        );
    }

    @Operation(
            summary = "Update a circuit",
            description = "Update a circuit if it exist or specify by error response",
            tags = {"circuit"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateCircuitDtoResponse updateCircuit(
            @RequestBody @Valid final UpdateCircuitDtoRequest updateCircuitDtoRequest
    ) throws CodeMessageException {

        final CircuitEntity circuitEntity = new CircuitEntity(
                updateCircuitDtoRequest.id,
                updateCircuitDtoRequest.name,
                updateCircuitDtoRequest.pictureName,
                updateCircuitDtoRequest.module,
                new SceneEntity(
                        updateCircuitDtoRequest.scene_uuid, null
                ),
                null
        );
        final CircuitEntity updatedCircuitEntity = circuitService.updateCircuit(circuitEntity);
        return new UpdateCircuitDtoResponse(
                updatedCircuitEntity.id,
                updatedCircuitEntity.name,
                updatedCircuitEntity.pictureName,
                updatedCircuitEntity.module,
                updatedCircuitEntity.scene.name
        );
    }

    @Operation(
            summary = "Delete a circuit",
            description = "Delete a circuit from database, BE CAREFUL !!!!! THE CIRCUIT IS LOST FOREVER!!!!",
            tags = {"circuit"}
    )
    @DeleteMapping("/delete/{id}")
    @IsAdmin
    public DeleteCircuitDtoResponse deleteCircuit(@PathVariable("id") final Long id)
            throws CodeMessageException {
        final CircuitEntity circuitEntity = circuitService.deleteCircuit(id);
        return new DeleteCircuitDtoResponse(
                circuitEntity.id,
                circuitEntity.name,
                circuitEntity.pictureName,
                circuitEntity.module,
                circuitEntity.scene.name
        );
    }

    @Operation(
            summary = "Update list of room associated to a circuit",
            description = "Update list of room associated to a circuit",
            tags = {"circuit"}
    )
    @IsAdmin
    @PatchMapping("/update/rooms")
    public UpdateRoomsOfCircuitDtoResponse updateRoomsOfCircuit(
            final @RequestBody UpdateRoomsOfCircuitDtoRequest updateRoomsOfCircuitDtoRequest
    ) throws CodeMessageException {
        final CircuitEntity circuitEntity = circuitService.updateRoomsOfCircuit(
                updateRoomsOfCircuitDtoRequest.circuitId, updateRoomsOfCircuitDtoRequest.roomIds
        );

        return new UpdateRoomsOfCircuitDtoResponse(
                circuitEntity.id,
                circuitEntity.rooms.stream().map(roomEntity -> roomEntity.id).collect(Collectors.toList())
        );
    }
}
