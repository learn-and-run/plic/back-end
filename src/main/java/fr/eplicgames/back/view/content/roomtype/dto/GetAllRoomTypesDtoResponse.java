package fr.eplicgames.back.view.content.roomtype.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetAllRoomTypesDtoResponse {
    final List<RoomTypeDtoResponse> types;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class RoomTypeDtoResponse {
        final UUID uuid;
        final String type;
    }
}
