package fr.eplicgames.back.view.content.circuit.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateCircuitDtoResponse {
    final Long id;
    final String name;
    final String pictureName;
    final ModuleType module;
    final String scene;
    final UUID scene_uuid;
}
