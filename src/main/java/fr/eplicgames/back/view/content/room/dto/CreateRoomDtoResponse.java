package fr.eplicgames.back.view.content.room.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateRoomDtoResponse {
    final Long id;
    final Long questionId;
    final ModuleType module;
    // In milliseconds
    final Integer expectedTime;
    final String type;
    final UUID typeUuid;
}
