package fr.eplicgames.back.view.content.question.dto;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateQuestionIfNotExistDtoRequest {
    @NotBlank
    String name;
    @NotBlank
    String question;
    @NotBlank
    String response;
    String badAnswer;
    @NotBlank
    String explanation;
    String clue1;
    String clue2;
    String clue3;
    String clue4;
    @NotNull
    ModuleType module;
    @NotNull
    LevelType level;
    @NotNull
    Integer difficulty;
}
