package fr.eplicgames.back.view.content.question.dto;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateQuestionDtoRequest {
    @NotNull
    Long id;
    @NotBlank
    String name;
    @NotBlank
    String question;
    @NotBlank
    String response;
    String badAnswer;
    @NotBlank
    String explanation;
    String clue1;
    String clue2;
    String clue3;
    String clue4;
    @NotNull
    ModuleType module;
    @NotNull
    LevelType level;
    @NotNull
    Integer difficulty;
}
