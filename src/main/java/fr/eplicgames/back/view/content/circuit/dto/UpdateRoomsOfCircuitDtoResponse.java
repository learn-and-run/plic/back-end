package fr.eplicgames.back.view.content.circuit.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateRoomsOfCircuitDtoResponse {
    final Long circuitId;
    final List<Long> roomIds;
}
