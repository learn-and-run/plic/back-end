package fr.eplicgames.back.view.content.object.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateObjectDtoResponse {
    final UUID uuid;
    final String name;
    final String folder;
    final Boolean inGame;
}
