package fr.eplicgames.back.view.content.scene;

import fr.eplicgames.back.domain.entity.content.SceneEntity;
import fr.eplicgames.back.domain.service.content.SceneService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.view.content.scene.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * REST Controller for scene
 */
@RestController
@Tag(name = "scene", description = "the Scene API")
@RequestMapping(path = "/api/scene")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SceneController extends ExceptionController {
    final SceneService sceneService;


    @Operation(
            summary = "Create a scene",
            description = "Create a scene in database",
            tags = {"scene"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateSceneDtoResponse createScene(
            @RequestBody @Valid final CreateSceneDtoRequest createSceneDtoRequest
    ) throws CodeMessageException {
        final SceneEntity sceneEntity = new SceneEntity(
                null, createSceneDtoRequest.name
        );

        final SceneEntity newSceneEntity = sceneService.createScene(sceneEntity);
        return new CreateSceneDtoResponse(
                newSceneEntity.uuid, newSceneEntity.name
        );
    }

    @Operation(
            summary = "Update a scene",
            description = "Update a scene in database",
            tags = {"scene"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateSceneDtoResponse updateScene(
            @RequestBody @Valid final UpdateSceneDtoRequest updateSceneDtoRequest
    ) throws CodeMessageException {
        final SceneEntity sceneEntity = new SceneEntity(
                updateSceneDtoRequest.uuid, updateSceneDtoRequest.name
        );

        final SceneEntity newSceneEntity = sceneService.updateScene(sceneEntity);
        return new UpdateSceneDtoResponse(newSceneEntity.uuid, newSceneEntity.name);
    }

    @Operation(
            summary = "Delete a scene",
            description = "Delete a scene with the given uuid",
            tags = {"scene"}
    )
    @DeleteMapping("/delete/{uuid}")
    @IsAdmin
    public void deleteScene(@PathVariable("uuid") final String uuid) throws CodeMessageException {
        sceneService.deleteSceneByUuid(UUID.fromString(uuid));
    }

    @Operation(
            summary = "Get all scenes",
            description = "Get all scenes available or not in game",
            tags = {"scene"}
    )
    @GetMapping("/all")
    @IsAdmin
    public GetAllScenesDtoResponse getAllScenes() {
        return new GetAllScenesDtoResponse(sceneService.getScenes().stream().map(sceneEntity ->
                new GetAllScenesDtoResponse.SceneDtoResponse(
                        sceneEntity.uuid,
                        sceneEntity.name
                )).collect(Collectors.toList())
        );
    }
}
