package fr.eplicgames.back.view.content.roomtype;

import fr.eplicgames.back.domain.entity.content.RoomTypeEntity;
import fr.eplicgames.back.domain.service.content.RoomTypeService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.view.content.roomtype.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * REST Controller for roomType
 */
@RestController
@Tag(name = "roomType", description = "the RoomType API")
@RequestMapping(path = "/api/roomType")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomTypeController extends ExceptionController {
    final RoomTypeService roomTypeService;


    @Operation(
            summary = "Create a roomType",
            description = "Create a roomType in database",
            tags = {"roomType"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateRoomTypeDtoResponse createRoomType(
            @RequestBody @Valid final CreateRoomTypeDtoRequest createRoomTypeDtoRequest
    ) throws CodeMessageException {
        final RoomTypeEntity roomTypeEntity = new RoomTypeEntity(
                null, createRoomTypeDtoRequest.type
        );

        final RoomTypeEntity newRoomTypeEntity = roomTypeService.createRoomType(roomTypeEntity);
        return new CreateRoomTypeDtoResponse(
                newRoomTypeEntity.uuid, newRoomTypeEntity.type
        );
    }

    @Operation(
            summary = "Update a roomType",
            description = "Update a roomType in database",
            tags = {"roomType"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateRoomTypeDtoResponse updateRoomType(
            @RequestBody @Valid final UpdateRoomTypeDtoRequest updateRoomTypeDtoRequest
    ) throws CodeMessageException {
        final RoomTypeEntity roomTypeEntity = new RoomTypeEntity(
                updateRoomTypeDtoRequest.uuid, updateRoomTypeDtoRequest.type
        );

        final RoomTypeEntity newRoomTypeEntity = roomTypeService.updateRoomType(roomTypeEntity);
        return new UpdateRoomTypeDtoResponse(newRoomTypeEntity.uuid, newRoomTypeEntity.type);
    }

    @Operation(
            summary = "Delete a roomType",
            description = "Delete a roomType with the given uuid",
            tags = {"roomType"}
    )
    @DeleteMapping("/delete/{uuid}")
    @IsAdmin
    public void deleteRoomType(@PathVariable("uuid") final String uuid) throws CodeMessageException {
        roomTypeService.deleteRoomTypeByUuid(UUID.fromString(uuid));
    }

    @Operation(
            summary = "Get all roomTypes",
            description = "Get all roomTypes available or not in game",
            tags = {"roomType"}
    )
    @GetMapping("/all")
    @IsAdmin
    public GetAllRoomTypesDtoResponse getAllRoomTypes() {
        return new GetAllRoomTypesDtoResponse(roomTypeService.getRoomTypes().stream().map(roomTypeEntity ->
                new GetAllRoomTypesDtoResponse.RoomTypeDtoResponse(
                        roomTypeEntity.uuid,
                        roomTypeEntity.type
                )).collect(Collectors.toList())
        );
    }
}
