package fr.eplicgames.back.view.content.roomtype.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateRoomTypeDtoRequest {
    @NotBlank
    @Size(max = 16)
    String type;
}
