package fr.eplicgames.back.view.content.scene.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateSceneDtoRequest {
    @NotNull
    UUID uuid;

    @NotBlank
    @Size(max = 32)
    String name;
}
