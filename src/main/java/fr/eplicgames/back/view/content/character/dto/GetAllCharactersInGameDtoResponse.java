package fr.eplicgames.back.view.content.character.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetAllCharactersInGameDtoResponse {
    final List<CharacterDtoResponse> characters;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class CharacterDtoResponse {
        final Integer id;
        final String name;
        final String description;
        final String background;
    }
}
