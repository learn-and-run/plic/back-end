package fr.eplicgames.back.view.content.room.dto;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetRoomsByCircuitIdDtoResponse {
    final List<RoomDtoResponse> rooms;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class RoomDtoResponse {
        final Long id;
        final QuestionDtoResponse question;
        final ModuleType module;
        // In milliseconds
        final Integer expectedTime;
        final String type;
        final UUID typeUuid;

        @AllArgsConstructor
        @FieldDefaults(level = AccessLevel.PUBLIC)
        public static class QuestionDtoResponse {
            String name;

            String question;
            String response;
            String badAnswer;
            String explanation;

            String clue1;
            String clue2;
            String clue3;
            String clue4;

            ModuleType module;
            LevelType level;
            Integer difficulty;
        }
    }
}
