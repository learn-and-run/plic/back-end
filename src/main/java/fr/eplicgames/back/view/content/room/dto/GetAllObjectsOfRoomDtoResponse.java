package fr.eplicgames.back.view.content.room.dto;

import fr.eplicgames.back.utils.Vector3D;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetAllObjectsOfRoomDtoResponse {
    final Long roomId;
    final List<ObjectDtoResponse> objects;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class ObjectDtoResponse {
        final UUID uuid;
        final String name;
        final String folder;
        final Boolean inGame;

        final Vector3D<Float> position;
        final Vector3D<Float> rotation;
    }
}
