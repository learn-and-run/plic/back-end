package fr.eplicgames.back.view.content.character;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.domain.service.content.CharacterService;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.content.character.dto.GetAllCharactersDtoResponse;
import fr.eplicgames.back.view.content.character.dto.GetAllCharactersInGameDtoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * REST Controller for characters
 */
@RestController
@Tag(name = "character", description = "the Character API")
@RequestMapping(path = "/api/character")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterController extends ExceptionController {

    final CharacterService characterService;

    @Operation(
            summary = "Get all the characters in the database",
            description = "Get all the characters in the database",
            tags = {"character"}
    )
    @GetMapping("/all")
    public GetAllCharactersDtoResponse getAllCharacters() {
        final List<CharacterEntity> characterEntities = characterService.getAllCharacters();
        return new GetAllCharactersDtoResponse(
                characterEntities.stream().map(characterEntity ->
                        new GetAllCharactersDtoResponse.CharacterDtoResponse(
                                characterEntity.id.intValue(),
                                characterEntity.name,
                                characterEntity.description,
                                characterEntity.background,
                                characterEntity.inGame,
                                characterEntity.smallImage,
                                characterEntity.bigImage
                        )
                ).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Get all the characters present in game",
            description = "Get all the characters in the database presents in game correctly ordered in game",
            tags = {"character"}
    )
    @GetMapping("/all/game")
    public GetAllCharactersInGameDtoResponse getAllCharactersInGame() {
        final List<CharacterEntity> characterEntities = characterService.getAllCharactersInGame();
        return new GetAllCharactersInGameDtoResponse(
                characterEntities.stream().map(characterEntity ->
                        new GetAllCharactersInGameDtoResponse.CharacterDtoResponse(
                                characterEntity.id.intValue(),
                                characterEntity.name,
                                characterEntity.description,
                                characterEntity.background
                        )
                ).collect(Collectors.toList())
        );
    }
}
