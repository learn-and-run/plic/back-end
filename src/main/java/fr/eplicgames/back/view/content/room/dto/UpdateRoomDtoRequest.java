package fr.eplicgames.back.view.content.room.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateRoomDtoRequest {
    @NotNull
    Long id;
    @NotNull
    Long questionId;
    @NotNull
    ModuleType module;
    // In milliseconds
    @NotNull
    Integer expectedTime;
    @NotNull
    UUID typeUuid;
}
