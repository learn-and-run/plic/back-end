package fr.eplicgames.back.view.content.room.dto;

import fr.eplicgames.back.utils.Vector3D;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateAllObjectsOfRoomDtoRequest {
    Long roomId;
    List<ObjectDtoRequest> objects;

    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class ObjectDtoRequest {
        UUID uuid;
        Vector3D<Float> position;
        Vector3D<Float> rotation;
    }
}
