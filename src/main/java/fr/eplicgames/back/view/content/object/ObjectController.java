package fr.eplicgames.back.view.content.object;

import fr.eplicgames.back.domain.entity.content.ObjectEntity;
import fr.eplicgames.back.domain.service.content.ObjectService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.view.content.object.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * REST Controller for object
 */
@RestController
@Tag(name = "object", description = "the Object API")
@RequestMapping(path = "/api/object")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ObjectController extends ExceptionController {
    final ObjectService objectService;


    @Operation(
            summary = "Create an object",
            description = "Create an object in database",
            tags = {"object"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateObjectDtoResponse createObject(
            @RequestBody @Valid final CreateObjectDtoRequest createObjectDtoRequest
    ) throws CodeMessageException {
        final ObjectEntity objectEntity = new ObjectEntity(
                null, createObjectDtoRequest.name, createObjectDtoRequest.folder, createObjectDtoRequest.inGame
        );

        final ObjectEntity newObjectEntity = objectService.createObject(objectEntity);
        return new CreateObjectDtoResponse(
                newObjectEntity.uuid, newObjectEntity.name, newObjectEntity.folder, newObjectEntity.inGame
        );
    }

    @Operation(
            summary = "Update an object",
            description = "Update an object in database",
            tags = {"object"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateObjectDtoResponse updateObject(
            @RequestBody @Valid final UpdateObjectDtoRequest updateObjectDtoRequest
    ) throws CodeMessageException {
        final ObjectEntity objectEntity = new ObjectEntity(
                updateObjectDtoRequest.uuid,
                updateObjectDtoRequest.name,
                updateObjectDtoRequest.folder,
                updateObjectDtoRequest.inGame
        );

        final ObjectEntity newObjectEntity = objectService.updateObject(objectEntity);
        return new UpdateObjectDtoResponse(
                newObjectEntity.uuid, newObjectEntity.name, newObjectEntity.folder, newObjectEntity.inGame
        );
    }

    @Operation(
            summary = "Delete an object",
            description = "Delete an object with the given uuid",
            tags = {"object"}
    )
    @DeleteMapping("/delete/{uuid}")
    @IsAdmin
    public void deleteObject(@PathVariable("uuid") final String uuid) throws CodeMessageException {
        objectService.deleteObjectByUuid(UUID.fromString(uuid));
    }

    @Operation(
            summary = "Get all objects",
            description = "Get all objects available or not in game",
            tags = {"object"}
    )
    @GetMapping("/all")
    @IsAdmin
    public GetAllObjectsDtoResponse getAllObjects() {
        return new GetAllObjectsDtoResponse(objectService.getObjects().stream().map(objectEntity ->
                new GetAllObjectsDtoResponse.ObjectDtoResponse(
                        objectEntity.uuid,
                        objectEntity.name,
                        objectEntity.folder,
                        objectEntity.inGame
                )).collect(Collectors.toList())
        );
    }


}
