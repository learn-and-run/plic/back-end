package fr.eplicgames.back.view.content.question.dto;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateQuestionDtoResponse {
    final Long id;
    final String name;

    final String question;
    final String response;
    final String badAnswer;
    final String explanation;

    final String clue1;
    final String clue2;
    final String clue3;
    final String clue4;

    final ModuleType module;
    final LevelType level;
    final Integer difficulty;
}
