package fr.eplicgames.back.view.content.scene.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetAllScenesDtoResponse {
    final List<SceneDtoResponse> scenes;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class SceneDtoResponse {
        final UUID uuid;
        final String name;
    }
}
