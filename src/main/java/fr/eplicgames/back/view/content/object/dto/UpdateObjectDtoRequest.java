package fr.eplicgames.back.view.content.object.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateObjectDtoRequest {
    @NotNull
    UUID uuid;

    @NotBlank
    @Size(max = 32)
    String name;

    @NotBlank
    @Size(max = 32)
    String folder;

    @NotNull
    Boolean inGame;
}
