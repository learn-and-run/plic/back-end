package fr.eplicgames.back.view.content.room;

import fr.eplicgames.back.domain.entity.content.ObjectEntity;
import fr.eplicgames.back.domain.entity.content.RoomEntity;
import fr.eplicgames.back.domain.entity.content.RoomObjectEntity;
import fr.eplicgames.back.domain.entity.content.RoomTypeEntity;
import fr.eplicgames.back.domain.service.content.RoomService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.view.content.room.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST Controller for room
 */
@RestController
@Tag(name = "room", description = "the Room API")
@RequestMapping(path = "/api/room")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomController extends ExceptionController {

    final RoomService roomService;

    @Operation(
            summary = "Get all rooms associated to a specific circuit",
            description = "Get all rooms associated to a specific circuit by given the circuit id",
            tags = {"room"}
    )
    @GetMapping("/circuit/{id}")
    public GetRoomsByCircuitIdDtoResponse getRoomsByCircuitId(@PathVariable("id") final Long id) {

        final List<RoomEntity> roomEntities = roomService.getRoomsByCircuitId(id);

        return new GetRoomsByCircuitIdDtoResponse(roomEntities.stream().map(roomEntity ->
                new GetRoomsByCircuitIdDtoResponse.RoomDtoResponse(
                        roomEntity.id,
                        new GetRoomsByCircuitIdDtoResponse.RoomDtoResponse.QuestionDtoResponse(
                                roomEntity.question.name,
                                roomEntity.question.question,
                                roomEntity.question.response,
                                roomEntity.question.badAnswer,
                                roomEntity.question.explanation,
                                roomEntity.question.clue1,
                                roomEntity.question.clue2,
                                roomEntity.question.clue3,
                                roomEntity.question.clue4,
                                roomEntity.question.module,
                                roomEntity.question.level,
                                roomEntity.question.difficulty
                        ),
                        roomEntity.module,
                        roomEntity.expectedTime,
                        roomEntity.roomType.type,
                        roomEntity.roomType.uuid
                )).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Get all rooms associated to a specific module",
            description = "Get all rooms associated to a specific module that can be GENERAL, MATHS, FRANCAIS, " +
                    "SVT, PHYSIQUE, CHIMIE, ANGLAIS, HISTOIRE or GEOGRAPHIE or TOUS if we want to get all rooms",
            tags = {"room"}
    )
    @GetMapping("/all/{module}")
    public GetRoomsByModuleDtoResponse getRoomsByModule(@PathVariable("module") final ModuleType module) {
        final List<RoomEntity> roomEntities = roomService.getRoomsByModule(module);
        return new GetRoomsByModuleDtoResponse(roomEntities.stream().map(roomEntity ->
                new GetRoomsByModuleDtoResponse.RoomDtoResponse(
                        roomEntity.id,
                        roomEntity.question.id,
                        roomEntity.module,
                        roomEntity.expectedTime,
                        roomEntity.roomType.type,
                        roomEntity.roomType.uuid
                )
        ).collect(Collectors.toList()));
    }

    @Operation(
            summary = "Create a room",
            description = "Create a room if it does not exist.",
            tags = {"room"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateRoomDtoResponse createRoom(@RequestBody @Valid final CreateRoomDtoRequest createRoomDtoRequest)
            throws CodeMessageException {

        final RoomEntity roomEntity = new RoomEntity(
                null,
                createRoomDtoRequest.module,
                null,
                createRoomDtoRequest.expectedTime,
                new RoomTypeEntity(
                        createRoomDtoRequest.typeUuid,
                        null
                )
        );
        final RoomEntity createdRoomEntity = roomService.createRoom(roomEntity, createRoomDtoRequest.questionId);
        return new CreateRoomDtoResponse(
                createdRoomEntity.id,
                createdRoomEntity.question.id,
                createdRoomEntity.module,
                createdRoomEntity.expectedTime,
                createdRoomEntity.roomType.type,
                createdRoomEntity.roomType.uuid
        );
    }

    @Operation(
            summary = "Update a room",
            description = "Update a the room if it exists and if the question id is not already assign to another room",
            tags = {"room"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateRoomDtoResponse updateRoom(@RequestBody @Valid final UpdateRoomDtoRequest updateRoomDtoRequest)
            throws CodeMessageException {
        final RoomEntity roomEntity = new RoomEntity(
                updateRoomDtoRequest.id,
                updateRoomDtoRequest.module,
                null,
                updateRoomDtoRequest.expectedTime,
                new RoomTypeEntity(
                        updateRoomDtoRequest.typeUuid,
                        null
                )
        );
        final RoomEntity updatedRoomEntity = roomService.updateRoom(roomEntity, updateRoomDtoRequest.questionId);
        return new UpdateRoomDtoResponse(
                updatedRoomEntity.id,
                updatedRoomEntity.question.id,
                updatedRoomEntity.module,
                updatedRoomEntity.expectedTime,
                updatedRoomEntity.roomType.type,
                updatedRoomEntity.roomType.uuid
        );
    }

    @Operation(
            summary = "Delete a room",
            description = "Delete a the room if it exists or specify it by error",
            tags = {"room"}
    )
    @DeleteMapping("/delete/{id}")
    @IsAdmin
    public void deleteRoom(@PathVariable("id") final Long id)
            throws CodeMessageException {
        roomService.deleteRoom(id);
    }

    @Operation(
            summary = "Get all objects of the room",
            description = "Get all objects of the room with the specific id",
            tags = {"room"}
    )
    @GetMapping("/object/all/{roomId}")
    public GetAllObjectsOfRoomDtoResponse GetAllObjectsOfRoomDtoResponse(
            @PathVariable("roomId") final Long roomId
    ) throws CodeMessageException {
        return new GetAllObjectsOfRoomDtoResponse(
                roomId,
                roomService.getAllObjectsOfRoom(roomId).stream().map(roomObjectEntity ->
                        new GetAllObjectsOfRoomDtoResponse.ObjectDtoResponse(
                                roomObjectEntity.object.uuid,
                                roomObjectEntity.object.name,
                                roomObjectEntity.object.folder,
                                roomObjectEntity.object.inGame,
                                roomObjectEntity.position,
                                roomObjectEntity.rotation
                        )
                ).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Update all objects of the room",
            description = "Update all objects of the room with the specific id",
            tags = {"room"}
    )
    @PatchMapping("/object/update")
    public UpdateAllObjectsOfRoomDtoResponse updateAllObjectsOfRoom(
            @RequestBody @Valid final UpdateAllObjectsOfRoomDtoRequest updateAllObjectsOfRoomDtoRequest
    ) throws CodeMessageException {
        final List<RoomObjectEntity> roomObjectEntities = roomService.updateObjectsOfRoom(
                updateAllObjectsOfRoomDtoRequest.objects.stream().map(objectDtoRequest ->
                        new RoomObjectEntity(
                                new ObjectEntity(
                                        objectDtoRequest.uuid, null, null, null
                                ),
                                objectDtoRequest.position,
                                objectDtoRequest.rotation
                        )
                ).collect(Collectors.toList()),
                updateAllObjectsOfRoomDtoRequest.roomId
        );

        return new UpdateAllObjectsOfRoomDtoResponse(
                updateAllObjectsOfRoomDtoRequest.roomId,
                roomObjectEntities.stream().map(roomObjectEntity ->
                        new UpdateAllObjectsOfRoomDtoResponse.ObjectDtoResponse(
                                roomObjectEntity.object.uuid,
                                roomObjectEntity.object.name,
                                roomObjectEntity.object.folder,
                                roomObjectEntity.object.inGame,
                                roomObjectEntity.position,
                                roomObjectEntity.rotation
                        )
                ).collect(Collectors.toList())
        );
    }
}
