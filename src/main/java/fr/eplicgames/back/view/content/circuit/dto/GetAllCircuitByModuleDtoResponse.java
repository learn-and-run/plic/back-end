package fr.eplicgames.back.view.content.circuit.dto;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GetAllCircuitByModuleDtoResponse {

    final List<CircuitDtoResponse> circuits;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class CircuitDtoResponse {
        final Long id;
        final String name;
        final String pictureName;
        final ModuleType module;
        final String scene;
        final UUID scene_uuid;
        final Integer bestScore;
        final Integer spendTime;
    }
}
