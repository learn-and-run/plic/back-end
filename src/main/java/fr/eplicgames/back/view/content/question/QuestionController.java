package fr.eplicgames.back.view.content.question;

import fr.eplicgames.back.domain.entity.content.QuestionEntity;
import fr.eplicgames.back.domain.service.content.QuestionService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.view.content.question.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


/**
 * REST Controller for questions
 */
@RestController
@Tag(name = "question", description = "the Question API")
@RequestMapping(path = "/api/question")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class QuestionController extends ExceptionController {

    final QuestionService questionService;

    @Operation(
            summary = "Get all questions associated to a specific module",
            description = "Get all questions associated to a specific module that can be GENERAL, MATHS, FRANCAIS, " +
                    "SVT, PHYSIQUE, CHIMIE, ANGLAIS, HISTOIRE or GEOGRAPHIE or TOUS if we want to get all questions",
            tags = {"question"}
    )
    @GetMapping("/all/{module}")
    public GetAllQuestionsByModuleDtoResponse getAllQuestionsByModule(@PathVariable("module") final ModuleType module) {
        final List<QuestionEntity> questionEntities = questionService.getAllQuestionByModule(module);
        return new GetAllQuestionsByModuleDtoResponse(questionEntities.stream().map(questionEntity ->
                new GetAllQuestionsByModuleDtoResponse.QuestionDtoResponse(
                        questionEntity.id,
                        questionEntity.name,
                        questionEntity.question,
                        questionEntity.response,
                        questionEntity.badAnswer,
                        questionEntity.explanation,
                        questionEntity.clue1,
                        questionEntity.clue2,
                        questionEntity.clue3,
                        questionEntity.clue4,
                        questionEntity.module,
                        questionEntity.level,
                        questionEntity.difficulty
                )).collect(Collectors.toList())
        );
    }

    @Operation(
            summary = "Create a question",
            description = "Create a question if the name does not exist.",
            tags = {"question"}
    )
    @PostMapping("/create")
    @IsAdmin
    public CreateQuestionIfNotExistDtoResponse createQuestionIfNotExist(
            @RequestBody @Valid final CreateQuestionIfNotExistDtoRequest createQuestionIfNotExistDtoRequest
    ) throws CodeMessageException {
        final QuestionEntity questionEntity = new QuestionEntity(
                null,
                createQuestionIfNotExistDtoRequest.name,
                createQuestionIfNotExistDtoRequest.question,
                createQuestionIfNotExistDtoRequest.response,
                createQuestionIfNotExistDtoRequest.badAnswer,
                createQuestionIfNotExistDtoRequest.explanation,
                createQuestionIfNotExistDtoRequest.clue1,
                createQuestionIfNotExistDtoRequest.clue2,
                createQuestionIfNotExistDtoRequest.clue3,
                createQuestionIfNotExistDtoRequest.clue4,
                createQuestionIfNotExistDtoRequest.module,
                createQuestionIfNotExistDtoRequest.level,
                createQuestionIfNotExistDtoRequest.difficulty
        );

        final QuestionEntity createdQuestion = questionService.createQuestionIfNotExists(questionEntity);

        return new CreateQuestionIfNotExistDtoResponse(
                createdQuestion.id,
                createdQuestion.name,
                createdQuestion.question,
                createdQuestion.response,
                createdQuestion.badAnswer,
                createdQuestion.explanation,
                createdQuestion.clue1,
                createdQuestion.clue2,
                createdQuestion.clue3,
                createdQuestion.clue4,
                createdQuestion.module,
                createdQuestion.level,
                createdQuestion.difficulty
        );
    }

    @Operation(
            summary = "Update a question",
            description = "Update a question if it exists.",
            tags = {"question"}
    )
    @PatchMapping("/update")
    @IsAdmin
    public UpdateQuestionDtoResponse updateQuestion(
            @RequestBody @Valid final UpdateQuestionDtoRequest updateQuestionDtoRequest
    ) throws CodeMessageException {
        final QuestionEntity questionEntity = new QuestionEntity(
                updateQuestionDtoRequest.id,
                updateQuestionDtoRequest.name,
                updateQuestionDtoRequest.question,
                updateQuestionDtoRequest.response,
                updateQuestionDtoRequest.badAnswer,
                updateQuestionDtoRequest.explanation,
                updateQuestionDtoRequest.clue1,
                updateQuestionDtoRequest.clue2,
                updateQuestionDtoRequest.clue3,
                updateQuestionDtoRequest.clue4,
                updateQuestionDtoRequest.module,
                updateQuestionDtoRequest.level,
                updateQuestionDtoRequest.difficulty
        );

        final QuestionEntity updatedQuestionEntity = questionService.updateQuestion(questionEntity);
        return new UpdateQuestionDtoResponse(
                updatedQuestionEntity.id,
                updatedQuestionEntity.name,
                updatedQuestionEntity.question,
                updatedQuestionEntity.response,
                updatedQuestionEntity.badAnswer,
                updatedQuestionEntity.explanation,
                updatedQuestionEntity.clue1,
                updatedQuestionEntity.clue2,
                updatedQuestionEntity.clue3,
                updatedQuestionEntity.clue4,
                updatedQuestionEntity.module,
                updatedQuestionEntity.level,
                updatedQuestionEntity.difficulty
        );
    }

    @Operation(
            summary = "Delete a question",
            description = "Delete a question if it exists.",
            tags = {"question"}
    )

    @DeleteMapping("/delete/{id}")
    @IsAdmin
    public void deleteQuestion(@PathVariable("id") final Long id)
            throws CodeMessageException {
        questionService.deleteQuestion(id);
    }
}
