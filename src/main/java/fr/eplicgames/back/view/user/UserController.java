package fr.eplicgames.back.view.user;

import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.user.dto.IsAdminDtoResponse;
import fr.eplicgames.back.view.user.dto.IsAuthenticatedDtoResponse;
import fr.eplicgames.back.view.user.dto.IsFullyAuthenticatedDtoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for user
 */
@RestController
@Tag(name = "user", description = "the User API")
@RequestMapping(path = "/api/user")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserController extends ExceptionController {

    @Operation(
            summary = "Permit to know if the user is authenticated",
            description = "Permit to know if the user is authenticated",
            tags = {"user"}
    )
    @GetMapping("/is_auth")
    public IsAuthenticatedDtoResponse isAuthenticated(final Authentication authentication) {
        return new IsAuthenticatedDtoResponse(
                authentication != null && !(authentication instanceof AnonymousAuthenticationToken)
        );
    }

    @Operation(
            summary = "Permit to know if the user is fully authenticated",
            description = "Permit to know if the user is fully authenticated",
            tags = {"user"}
    )
    @GetMapping("/is_fully_auth")
    public IsFullyAuthenticatedDtoResponse isFullyAuthenticated(final Authentication authentication) {
        return new IsFullyAuthenticatedDtoResponse(
                authentication != null
                        && !(authentication instanceof AnonymousAuthenticationToken
                        || authentication instanceof RememberMeAuthenticationToken)
        );
    }

    @Operation(
            summary = "Permit to know if the user is an admin",
            description = "Permit to know if the user is an admin",
            tags = {"user"}
    )
    @GetMapping("/is_admin")
    public IsAdminDtoResponse isAdmin(final Authentication authentication) {
        return new IsAdminDtoResponse(
                authentication != null
                        && authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN")));
    }
}
