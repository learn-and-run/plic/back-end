package fr.eplicgames.back.view.user;

import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.domain.service.user.InfoService;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.user.dto.GetInfosDtoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for user info
 */
@RestController
@Tag(name = "user", description = "the User API")
@RequestMapping(path = "/api/user")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InfoController extends ExceptionController {

    final InfoService infoService;

    @Operation(summary = "Get the current connected user", description = "Get the current connected user", tags = {"user"})
    @GetMapping("/info")
    public GetInfosDtoResponse getInfos() {
        final UserEntity userEntity = infoService.getCurrentUserInfo();
        return new GetInfosDtoResponse(userEntity);
    }

}
