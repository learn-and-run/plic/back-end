package fr.eplicgames.back.view.user.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateEmailDtoRequest {
    @NotBlank
    @Email
    String email;
}
