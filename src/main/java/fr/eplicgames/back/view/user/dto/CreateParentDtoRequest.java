package fr.eplicgames.back.view.user.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class CreateParentDtoRequest {
    @Size(max = 32)
    @Pattern(regexp = "^[^@]+$")
    String pseudo;

    @NotBlank
    @Size(max = 32)
    String firstname;

    @NotBlank
    @Size(max = 32)
    String lastname;

    @NotBlank
    @Size(min = 5, max = 72)
    @Pattern(regexp = "^(?=.*\\d)(?=(.*\\W))(?=.*[a-zA-Z])(?!.*\\s).+$")
    String password;

    @NotBlank
    @Email
    String email;
}
