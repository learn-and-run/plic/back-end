package fr.eplicgames.back.view.user;

import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.service.user.CreateService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.user.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * REST Controller for user
 */
@RestController
@Tag(name = "user", description = "the User API")
@RequestMapping(path = "/api/user")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateController extends ExceptionController {

    final CreateService createService;

    @Operation(
            summary = "Create a new student user in the database",
            description = "Create a new student user in the database",
            tags = {"user"}
    )
    @PostMapping("/create_student")
    public CreateStudentDtoResponse createStudent(
            @RequestBody @Valid final CreateStudentDtoRequest createStudentDtoRequest
    ) throws CodeMessageException {
        final StudentEntity studentEntity = new StudentEntity(
                null,
                createStudentDtoRequest.pseudo,
                createStudentDtoRequest.firstname,
                createStudentDtoRequest.lastname,
                createStudentDtoRequest.email,
                createStudentDtoRequest.password,
                null,
                false,
                createStudentDtoRequest.levelType
        );
        final StudentEntity createdStudentEntity = createService.createStudent(studentEntity);
        return new CreateStudentDtoResponse(
                createdStudentEntity.id,
                createStudentDtoRequest.email,
                createdStudentEntity.pseudo,
                createdStudentEntity.firstname,
                createdStudentEntity.lastname,
                createdStudentEntity.levelType
        );
    }

    @Operation(
            summary = "Create a new parent user in the database",
            description = "Create a new parent user in the database",
            tags = {"user"}
    )
    @PostMapping("/create_parent")
    public CreateParentDtoResponse createParent(
            @RequestBody @Valid final CreateParentDtoRequest createParentDtoRequest
    ) throws CodeMessageException {
        final ParentEntity parentEntity = new ParentEntity(
                null,
                createParentDtoRequest.pseudo,
                createParentDtoRequest.firstname,
                createParentDtoRequest.lastname,
                createParentDtoRequest.email,
                createParentDtoRequest.password,
                null,
                false
        );
        final ParentEntity createdParentEntity = createService.createParent(parentEntity);
        return new CreateParentDtoResponse(
                createdParentEntity.id,
                createParentDtoRequest.email,
                createdParentEntity.pseudo,
                createdParentEntity.firstname,
                createdParentEntity.lastname
        );
    }

    @Operation(
            summary = "Create a new professor user in the database",
            description = "Create a new professor user in the database",
            tags = {"user"}
    )
    @PostMapping("/create_professor")
    public CreateProfessorDtoResponse createProfessor(
            @RequestBody @Valid final CreateProfessorDtoRequest createProfessorDtoRequest
    ) throws CodeMessageException {
        final ProfessorEntity professorEntity = new ProfessorEntity(
                null,
                createProfessorDtoRequest.pseudo,
                createProfessorDtoRequest.firstname,
                createProfessorDtoRequest.lastname,
                createProfessorDtoRequest.email,
                createProfessorDtoRequest.password,
                null,
                false
        );
        final ProfessorEntity createdProfessorEntity = createService.createProfessor(professorEntity);
        return new CreateProfessorDtoResponse(
                createdProfessorEntity.id,
                createProfessorDtoRequest.email,
                createdProfessorEntity.pseudo,
                createdProfessorEntity.firstname,
                createdProfessorEntity.lastname
        );
    }

    @Operation(
            summary = "Activate a user with its custom link",
            description = "Activate a user with its custom link",
            tags = {"user"}
    )
    @GetMapping("/activate/{id}") //TODO change to patch when front end is ready
    public void activeUser(@PathVariable("id") final String id) throws CodeMessageException {
        createService.activateUser(id);
    }

}
