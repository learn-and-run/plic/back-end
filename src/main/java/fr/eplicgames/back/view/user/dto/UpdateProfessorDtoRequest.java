package fr.eplicgames.back.view.user.dto;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateProfessorDtoRequest {
    @Size(max = 32)
    @Pattern(regexp = "^[^@]+$")
    String pseudo;

    @NotBlank
    @Size(max = 32)
    String firstname;

    @NotBlank
    @Size(max = 32)
    String lastname;
}
