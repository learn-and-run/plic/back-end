package fr.eplicgames.back.view.user.dto;

import fr.eplicgames.back.sql_type.LevelType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.*;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateStudentDtoRequest {
    @Size(max = 32)
    @Pattern(regexp = "^[^@]+$")
    String pseudo;

    @NotBlank
    @Size(max = 32)
    String firstname;

    @NotBlank
    @Size(max = 32)
    String lastname;

    @NotNull
    LevelType levelType;
}
