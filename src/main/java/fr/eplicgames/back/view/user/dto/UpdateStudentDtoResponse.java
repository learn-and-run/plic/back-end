package fr.eplicgames.back.view.user.dto;

import fr.eplicgames.back.sql_type.LevelType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateStudentDtoResponse {
    final String pseudo;
    final String firstname;
    final String lastname;
    final LevelType levelType;
}
