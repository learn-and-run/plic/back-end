package fr.eplicgames.back.view.user;

import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.service.user.UpdateService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.ExceptionController;
import fr.eplicgames.back.view.user.dto.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * REST Controller for user update
 */
@RestController
@Tag(name = "user", description = "the User API")
@RequestMapping(path = "/api/user")
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateController extends ExceptionController {

    final UpdateService updateService;

    @Operation(
            summary = "Update the character of the current user",
            description = "Update the character of the current user",
            tags = {"user"}
    )
    @PatchMapping("/update/character/{id}")
    public void updateCharacter(@PathVariable final Long id) throws CodeMessageException {
        updateService.updateCharacter(id);
    }

    @Operation(
            summary = "Update the current user (student)",
            description = "Update the current user (student)",
            tags = {"user"}
    )
    @PatchMapping("/update/student")
    public UpdateStudentDtoResponse updateStudent(
            @RequestBody @Valid final UpdateStudentDtoRequest updateStudentDtoRequest
    ) throws CodeMessageException {
        final StudentEntity studentEntity = new StudentEntity(
                null,
                updateStudentDtoRequest.pseudo,
                updateStudentDtoRequest.firstname,
                updateStudentDtoRequest.lastname,
                null,
                null,
                null,
                null,
                updateStudentDtoRequest.levelType
        );

        final StudentEntity updatedStudentEntity = updateService.updateStudent(studentEntity);

        return new UpdateStudentDtoResponse(
                updatedStudentEntity.pseudo,
                updatedStudentEntity.firstname,
                updatedStudentEntity.lastname,
                updatedStudentEntity.levelType
        );
    }

    @Operation(
            summary = "Update the current user (parent)",
            description = "Update the current user (parent)",
            tags = {"user"}
    )
    @PatchMapping("/update/parent")
    public UpdateParentDtoResponse updateParent(
            @RequestBody @Valid final UpdateParentDtoRequest updateParentDtoRequest
    ) throws CodeMessageException {
        //TODO update if parent entity has specifications
        final ParentEntity parentEntity = new ParentEntity(
                null,
                updateParentDtoRequest.pseudo,
                updateParentDtoRequest.firstname,
                updateParentDtoRequest.lastname,
                null,
                null,
                null,
                null
        );
        final ParentEntity updatedParentEntity = (ParentEntity) updateService.updateUser(parentEntity);
        return new UpdateParentDtoResponse(
                updatedParentEntity.pseudo,
                updatedParentEntity.firstname,
                updatedParentEntity.lastname
        );
    }

    @Operation(
            summary = "Update the current user (professor)",
            description = "Update the current user (professor)",
            tags = {"user"}
    )
    @PatchMapping("/update/professor")
    public UpdateProfessorDtoResponse updateProfessor(
            @RequestBody @Valid final UpdateProfessorDtoRequest updateProfessorDtoRequest
    ) throws CodeMessageException {
        //TODO update if professor entity has specifications
        final ProfessorEntity professorEntity = new ProfessorEntity(
                null,
                updateProfessorDtoRequest.pseudo,
                updateProfessorDtoRequest.firstname,
                updateProfessorDtoRequest.lastname,
                null,
                null,
                null,
                null
        );
        final ProfessorEntity updatedProfessorEntity = (ProfessorEntity) updateService.updateUser(professorEntity);
        return new UpdateProfessorDtoResponse(
                updatedProfessorEntity.pseudo,
                updatedProfessorEntity.firstname,
                updatedProfessorEntity.lastname
        );
    }

    @Operation(
            summary = "Send a link to update the email of the current user",
            description = "Send a link to update the email of the current user",
            tags = {"user"}
    )
    @PatchMapping("/update/email")
    public void updateEmail(@RequestBody @Valid final UpdateEmailDtoRequest updateEmailDtoRequest)
            throws CodeMessageException {
        updateService.updateEmail(updateEmailDtoRequest.email);
    }

    @Operation(
            summary = "Validate the new email with custom link",
            description = "Validate the new email with custom link",
            tags = {"user"}
    )
    @GetMapping("/update/email/{id}") //TODO change to patch when front end is ready
    public void validateNewEmail(@PathVariable("id") final String id) throws CodeMessageException {
        updateService.validateNewEmail(id);
    }

    @Operation(
            summary = "Update the password of the current user",
            description = "Update the password of the current user",
            tags = {"user"}
    )
    @PatchMapping("/update/password")
    public void updatePassword(
            @RequestBody @Valid final UpdatePasswordDtoRequest updatePasswordDtoRequest, final HttpSession session
    ) throws CodeMessageException {
        updateService.updatePassword(updatePasswordDtoRequest.password, session);
    }

    @Operation(
            summary = "Ask a link to reset a password",
            description = "Ask a link to reset a password",
            tags = {"user"}
    )
    @PostMapping("/reset")
    public void askResetPassword(@RequestBody @Valid final AskResetPasswordDtoRequest askResetPasswordDtoRequest)
            throws CodeMessageException {
        updateService.askResetPassword(askResetPasswordDtoRequest.email);
    }

    @Operation(
            summary = "Update the password from the custom link",
            description = "Update the password from the custom link",
            tags = {"user"}
    )
    @PatchMapping("/reset/{id}")
    public void resetPassword(@PathVariable("id") final String id,
                              @RequestBody @Valid final ResetPasswordDtoRequest resetPasswordDtoRequest,
                              final HttpSession session) throws CodeMessageException {
        updateService.resetPassword(id, resetPasswordDtoRequest.password, session);
    }

    @Operation(
            summary = "Delete the current user",
            description = "Delete the current user",
            tags = {"user"}
    )
    @DeleteMapping("/delete")
    public void deleteUser(final HttpSession session) throws CodeMessageException {
        updateService.deleteUser(session);
    }
}
