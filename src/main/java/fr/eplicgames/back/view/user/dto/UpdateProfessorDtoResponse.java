package fr.eplicgames.back.view.user.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UpdateProfessorDtoResponse {
    final String pseudo;
    final String firstname;
    final String lastname;
}
