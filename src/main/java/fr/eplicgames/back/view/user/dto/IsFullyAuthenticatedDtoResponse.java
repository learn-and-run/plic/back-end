package fr.eplicgames.back.view.user.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class IsFullyAuthenticatedDtoResponse {
    boolean isFullyAuthenticated;
}
