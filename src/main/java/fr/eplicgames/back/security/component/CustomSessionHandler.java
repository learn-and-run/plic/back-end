package fr.eplicgames.back.security.component;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * Custom Session Handler
 */
@Component
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomSessionHandler {

    final SessionRegistry sessionRegistry;

    public void expireUserSessions(String id) {
        for (Object principal : sessionRegistry.getAllPrincipals()) {
            if (principal instanceof User) {
                UserDetails userDetails = (UserDetails) principal;
                if (userDetails.getUsername().equals(id)) {
                    for (SessionInformation sessionInformation : sessionRegistry.getAllSessions(userDetails, true)) {
                        sessionInformation.expireNow();
                    }
                }
            }
        }
    }

}
