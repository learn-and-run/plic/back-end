package fr.eplicgames.back.security.component;

import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.exception.ExceptionController;

import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import java.io.IOException;

/**
 * Custom Session Expired Event
 */
public class CustomSessionInformationExpiredEvent implements SessionInformationExpiredStrategy {

    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent sessionInformationExpiredEvent) throws IOException {
        sessionInformationExpiredEvent.getRequest().getSession().invalidate();
        ExceptionController.writeError(sessionInformationExpiredEvent.getRequest(), sessionInformationExpiredEvent.getResponse(), CodeMessageExceptionEnum.SESSION_EXPIRED);
    }

}
