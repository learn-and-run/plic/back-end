package fr.eplicgames.back.security.component;

import fr.eplicgames.back.security.repository.CustomJdbcTokenRepositoryImpl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Custom Logout Success Handler
 */
@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    @Value("${server.servlet.remember.cookie.name}")
    String cookieRemember;

    final CustomJdbcTokenRepositoryImpl repository;

    public CustomLogoutSuccessHandler(DataSource dataSource) {
        this.repository = new CustomJdbcTokenRepositoryImpl();
        this.repository.setDataSource(dataSource);
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies == null)
            return;

        for (Cookie cookie : cookies) {
            if (cookieRemember.equals(cookie.getName())) {
                repository.removeUserToken(authentication.getName(), cookie.getValue());
            }
        }
    }

}
