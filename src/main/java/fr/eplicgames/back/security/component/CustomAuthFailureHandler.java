package fr.eplicgames.back.security.component;

import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.exception.ExceptionController;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Custom Authentication Failure Handler
 */
@Component
public class CustomAuthFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        httpServletResponse.setHeader("Set-Cookie", "SessionId=; Max-Age=0; Path=/");
        ExceptionController.writeError(httpServletRequest, httpServletResponse, CodeMessageExceptionEnum.WRONG_CREDENTIALS);
    }

}
