package fr.eplicgames.back.security.component;

import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.exception.ExceptionController;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Custom Unauthorized Handler
 */
@Component
public class CustomUnauthorizedHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        ExceptionController.writeError(httpServletRequest, httpServletResponse, CodeMessageExceptionEnum.USER_NOT_LOGIN);
    }

}
