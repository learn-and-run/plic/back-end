package fr.eplicgames.back.security.component;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * Custom Authentication Success Handler
 */
@Component
public class CustomAuthSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        Collection<String> cookieStrings = response.getHeaders("Set-Cookie");

        boolean isFirst = true;
        for (String cookieString : cookieStrings) {
            if (isFirst) {
                isFirst = false;
                response.setHeader("Set-Cookie", cookieString + "; SameSite=None");
                continue;
            }
            response.addHeader("Set-Cookie", cookieString + "; SameSite=None");
        }
    }

}
