package fr.eplicgames.back.security.model;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Entity of persistent_logins table
 */
@Entity
@Table(name = "persistent_logins")
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class PersistentLoginsModel {

    @Id
    @Column(length = 64)
    String series;
    @Column(length = 64, nullable = false)
    String token;
    @Column(length = 64, nullable = false)
    String username;
    @Column(nullable = false)
    Timestamp last_used;

}
