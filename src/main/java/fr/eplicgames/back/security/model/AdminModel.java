package fr.eplicgames.back.security.model;

import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Entity of admin table
 */
@Data
@Entity
@Table(name = "user_admin")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminModel {

    @Id
    Long id;

    @MapsId
    @JoinColumn(name = "user_id")
    @OneToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel userModel;

}
