package fr.eplicgames.back.security.service;

import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.security.model.AdminModel;
import fr.eplicgames.back.security.repository.AdminRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom UserDetailsService for Swagger
 */
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomSwaggerUserDetailsService implements UserDetailsService {

    final AdminRepository adminRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) {
        final AdminModel admin = adminRepository.findAdminByPseudoOrEmail(username);
        if (admin == null)
            throw new UsernameNotFoundException(username);

        final UserModel user = admin.getUserModel();

        final List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getUserType().toString()));
        authorities.add(new SimpleGrantedAuthority("ADMIN"));

        final boolean isActive = user.getActive();
        return new User(user.getId().toString(), user.getPassword(), isActive, isActive, isActive, isActive, authorities);
    }

}
