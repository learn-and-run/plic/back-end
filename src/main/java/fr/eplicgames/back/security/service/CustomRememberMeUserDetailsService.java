package fr.eplicgames.back.security.service;

import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.security.repository.AdminRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomRememberMeUserDetailsService implements UserDetailsService {

    final UserRepository userRepository;
    final AdminRepository adminRepository;

    @Override
    public UserDetails loadUserByUsername(final String id) {
        final Optional<UserModel> optionalUserEntity = userRepository.findById(Long.valueOf(id));
        if (!optionalUserEntity.isPresent())
            throw new UsernameNotFoundException(id);

        final UserModel user = optionalUserEntity.get();

        final List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getUserType().toString()));

        if (adminRepository.isAdmin(user.getId()) != null)
            authorities.add(new SimpleGrantedAuthority("ADMIN"));

        final boolean isActive = user.getActive();
        return new User(id, user.getPassword(), isActive, isActive, isActive, isActive, authorities);
    }

}
