package fr.eplicgames.back.security.annotation;

import org.springframework.security.access.prepost.PreAuthorize;

@PreAuthorize("hasAuthority('ADMIN')")
public @interface IsAdmin {
}
