package fr.eplicgames.back.security.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

/**
 * Custom Remember Me Token Repository
 */
public class CustomJdbcTokenRepositoryImpl extends JdbcTokenRepositoryImpl {

    public void removeUserToken(String username, String rememberMeCookie) {
        String[] cookieTokens = this.decodeCookie(rememberMeCookie);
        if (cookieTokens.length != 2)
            throw new InvalidCookieException("Cookie token did not contain 2 tokens, but contained '" + Arrays.asList(cookieTokens) + "'");
        else {
            JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
            if (jdbcTemplate != null) {
                String removeUserTokenSql = "delete from persistent_logins where username = ? and series = ? and token = ?";
                jdbcTemplate.update(removeUserTokenSql, username, cookieTokens[0], cookieTokens[1]);
            }
        }
    }

    private String[] decodeCookie(String cookieValue) throws InvalidCookieException {
        StringBuilder cookieValueBuilder = new StringBuilder(cookieValue);
        for(int j = 0; j < cookieValueBuilder.length() % 4; ++j) {
            cookieValueBuilder.append("=");
        }
        cookieValue = cookieValueBuilder.toString();

        try {
            Base64.getDecoder().decode(cookieValue.getBytes());
        }
        catch (IllegalArgumentException var7) {
            throw new InvalidCookieException("Cookie token was not Base64 encoded; value was '" + cookieValue + "'");
        }

        String cookieAsPlainText = new String(Base64.getDecoder().decode(cookieValue.getBytes()));
        String[] tokens = StringUtils.delimitedListToStringArray(cookieAsPlainText, ":");

        for(int i = 0; i < tokens.length; ++i) {
            try {
                tokens[i] = URLDecoder.decode(tokens[i], StandardCharsets.UTF_8.toString());
            }
            catch (UnsupportedEncodingException var6) {
                this.logger.error(var6.getMessage(), var6);
            }
        }

        return tokens;
    }

}
