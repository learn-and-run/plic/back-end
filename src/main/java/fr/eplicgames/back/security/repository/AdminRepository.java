package fr.eplicgames.back.security.repository;

import fr.eplicgames.back.security.model.AdminModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for admin entity
 */
@Repository
public interface AdminRepository extends JpaRepository<AdminModel, Long> {

    @Query(value = "SELECT a FROM AdminModel a WHERE a.userModel.pseudo = ?1 OR a.userModel.email = ?1")
    AdminModel findAdminByPseudoOrEmail(String username);

    @Query(value = "SELECT a.userModel.id FROM AdminModel a WHERE a.userModel.id = ?1")
    Long isAdmin(Long id);

}
