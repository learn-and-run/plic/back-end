package fr.eplicgames.back.security.configuration;

import fr.eplicgames.back.security.service.CustomSwaggerUserDetailsService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Order(1)
@Configuration
@EnableWebSecurity
public class SwaggerSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${springdoc.api-docs.path}")
    String apiPath;
    @Value("${springdoc.swagger-ui.path}")
    String swaggerPath;

    final CustomSwaggerUserDetailsService customSwaggerUserDetailsService;
    final BCryptPasswordEncoder bCryptPasswordEncoder;

    public SwaggerSecurityConfiguration(CustomSwaggerUserDetailsService customSwaggerUserDetailsService,
                                        BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.customSwaggerUserDetailsService = customSwaggerUserDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Configure HTTP security for Swagger
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().and()
                .authorizeRequests()
                    .antMatchers("/images/**").permitAll()
                    .antMatchers(swaggerPath, "/swagger-ui/**", apiPath, apiPath + "/*").fullyAuthenticated()
                    .anyRequest().denyAll().and()
                .formLogin().permitAll()
                    .loginProcessingUrl("/login")
                    .defaultSuccessUrl(swaggerPath, true).and()
                .logout().permitAll();
    }

    /**
     * Configure the authentication service for Swagger
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customSwaggerUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

}
