package fr.eplicgames.back.security.configuration;

import fr.eplicgames.back.security.component.*;
import fr.eplicgames.back.security.repository.CustomJdbcTokenRepositoryImpl;
import fr.eplicgames.back.security.service.CustomPersistentTokenBasedRememberMeServices;
import fr.eplicgames.back.security.service.CustomRememberMeUserDetailsService;
import fr.eplicgames.back.security.service.CustomUserDetailsService;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;

/**
 * Security configuration
 */
@Order(0)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${server.servlet.session.cookie.name}")
    String cookieSession;
    @Value("${server.servlet.remember.cookie.name}")
    String cookieRemember;
    @Value("${server.servlet.remember.timeout}")
    Integer timeoutRemember;
    @Value("${server.servlet.remember.key}")
    String key;

    /**
     * Password Encoder
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Allow CORS
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                    .addMapping("/api/**")
                        .allowedOrigins("*")
                        .allowedHeaders("*")
                        .allowedMethods("*")
                        .allowCredentials(true);
            }
        };
    }

    @Bean
    public CustomJdbcTokenRepositoryImpl persistentTokenRepository() {
        CustomJdbcTokenRepositoryImpl db = new CustomJdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices() {
        PersistentTokenBasedRememberMeServices services = new CustomPersistentTokenBasedRememberMeServices(key, customRememberMeUserDetailsService, persistentTokenRepository());
        services.setTokenValiditySeconds(timeoutRemember);
        services.setCookieName(cookieRemember);
        return services;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    final DataSource dataSource;
    final CustomUserDetailsService customUserDetailsService;
    final CustomRememberMeUserDetailsService customRememberMeUserDetailsService;
    final CustomAuthSuccessHandler customAuthSuccessHandler;
    final CustomAuthFailureHandler customAuthFailureHandler;
    final CustomUnauthorizedHandler customUnauthorizedHandler;
    final CustomLogoutSuccessHandler customLogoutSuccessHandler;

    public SecurityConfiguration(DataSource datasource,
                                 CustomUserDetailsService customUserDetailsService,
                                 CustomRememberMeUserDetailsService customRememberMeUserDetailsService,
                                 CustomAuthSuccessHandler customAuthSuccessHandler,
                                 CustomAuthFailureHandler customAuthFailureHandler,
                                 CustomUnauthorizedHandler customUnauthorizedHandler,
                                 CustomLogoutSuccessHandler customLogoutSuccessHandler) {
        this.dataSource = datasource;
        this.customUserDetailsService = customUserDetailsService;
        this.customRememberMeUserDetailsService = customRememberMeUserDetailsService;
        this.customAuthSuccessHandler = customAuthSuccessHandler;
        this.customAuthFailureHandler = customAuthFailureHandler;
        this.customUnauthorizedHandler = customUnauthorizedHandler;
        this.customLogoutSuccessHandler = customLogoutSuccessHandler;
    }

    /**
     * Configure HTTP security
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
            .antMatcher("/api/**").csrf().disable()
                .sessionManagement()
                    .maximumSessions(100)
                    .maxSessionsPreventsLogin(false)
                    .expiredSessionStrategy(new CustomSessionInformationExpiredEvent())
                    .sessionRegistry(sessionRegistry()).and()
                    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
                .authorizeRequests()
                    .antMatchers("/api/user/create_*", "/api/user/activate/*", "/api/user/reset",
                            "/api/user/reset/*", "/api/user/update/email/*", "/api/user/is_auth",
                            "/api/user/is_fully_auth", "/api/user/is_admin", "/api/character/*").permitAll()
                    .antMatchers("/api/user/update/*", "/api/user/delete").fullyAuthenticated()
                    .anyRequest().authenticated().and()
                .formLogin().loginProcessingUrl("/api/login").permitAll()
                    .successHandler(customAuthSuccessHandler)
                    .failureHandler(customAuthFailureHandler).and()
                .logout()
                    .logoutSuccessHandler(customLogoutSuccessHandler)
                    .logoutUrl("/api/logout").deleteCookies(cookieSession).invalidateHttpSession(true).permitAll().and()
                .exceptionHandling().authenticationEntryPoint(customUnauthorizedHandler).and()
                .rememberMe().rememberMeServices(persistentTokenBasedRememberMeServices());
    }

    /**
     * Configure the authentication service
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addResourceHandlers(final ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/images/**").addResourceLocations("classpath:/images/");
            }
        };
    }
}
