package fr.eplicgames.back.persistence.model;

import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Entity of table scores
 */
@Data
@Entity
@Table(name = "scores")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ScoreModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user_id", nullable = false)
    UserModel user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "circuit_id", nullable = false)
    CircuitModel circuit;

    @ColumnDefault("0")
    @Column(nullable = false)
    Integer bestScore;

    @ColumnDefault("0")
    @Column(nullable = false)
    Integer currentScore;

    @ColumnDefault("0")
    @Column(nullable = false)
    Integer spendTime;

    @ColumnDefault("-1")
    @Column(nullable = false)
    Integer bestTime;
}
