package fr.eplicgames.back.persistence.model.user;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_professor")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProfessorModel implements Serializable, ActualUser {

    private static final long serialVersionUID = 6332880161984757400L;
    @Id
    Long userId;

    @MapsId
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.EAGER)
    UserModel userModel;

}
