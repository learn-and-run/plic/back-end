package fr.eplicgames.back.persistence.model;

import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.sql_type.IdentityType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(name = "game_preferences")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GamePreferenceModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel user;

    @Column(name = "controller_dualshock", nullable = false, columnDefinition = "boolean default false")
    Boolean controllerDualShock = false;

    @Column(name = "retro", nullable = false, columnDefinition = "boolean default false")
    Boolean retro = false;

    @Enumerated(EnumType.STRING)
    @Column(name = "identity", nullable = false, columnDefinition = "varchar(9) default 'PSEUDO'::character varying")
    IdentityType identity = IdentityType.PSEUDO;
}
