package fr.eplicgames.back.persistence.model.user;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_parent")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ParentModel implements Serializable, ActualUser {

    private static final long serialVersionUID = -6458753386625963018L;
    @Id
    Long userId;

    @MapsId
    @JoinColumn(name = "user_id")
    @OneToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel userModel;

}
