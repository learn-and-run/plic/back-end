package fr.eplicgames.back.persistence.model.content;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "room_types")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomTypeModel {
    @Id
    UUID uuid;

    @Column(nullable = false, unique = true)
    String type;

    public RoomTypeModel() {
        this.uuid = UUID.randomUUID();
    }

    public RoomTypeModel(final String type) {
        this();
        this.type = type;
    }

    public RoomTypeModel(final UUID uuid, final String type) {
        this.uuid = uuid;
        this.type = type;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
