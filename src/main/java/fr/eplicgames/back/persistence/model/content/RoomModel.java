package fr.eplicgames.back.persistence.model.content;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "rooms")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomModel implements Serializable {
    private static final long serialVersionUID = -3316989954647535718L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    ModuleType module;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id", nullable = false)
    QuestionModel question;

    // In milliseconds
    @ColumnDefault(value = "0")
    @NotNull
    Integer expectedTime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_uuid", nullable = false)
    RoomTypeModel roomType;
}
