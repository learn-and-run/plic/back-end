package fr.eplicgames.back.persistence.model;

import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Entity of table pending_game_room_session
 * This table permits to know when a player enter in a room to calculate its score when he leave it
 */
@Data
@Entity
@Table(name = "pending_game_session")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PendingGameSessionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, unique = true)
    UserModel user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    RoomModel room;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "circuit_id", nullable = false)
    CircuitModel circuit;

    Timestamp enterTime;

    Timestamp spendTime;
}
