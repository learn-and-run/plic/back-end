package fr.eplicgames.back.persistence.model.user;

import fr.eplicgames.back.sql_type.LevelType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_student")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentModel implements Serializable, ActualUser {

    private static final long serialVersionUID = 6317701577847207837L;
    @Id
    Long userId;

    @MapsId
    @JoinColumn(name = "user_id")
    @OneToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel userModel;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    LevelType levelType;

}
