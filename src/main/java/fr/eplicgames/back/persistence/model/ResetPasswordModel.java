package fr.eplicgames.back.persistence.model;

import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Entity of reset_password table
 */
@Data
@Entity
@Table(name = "reset_password")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResetPasswordModel {

    @Id
    @Column(length = 36)
    String id;
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user_id")
    @OneToOne(fetch = FetchType.LAZY)
    UserModel user;
}
