package fr.eplicgames.back.persistence.model;

import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Entity of activation_email table
 */
@Data
@Entity
@Table(name = "activation_email")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ActivationEmailModel {

    @Id
    @Column(length = 36)
    String id;
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.LAZY)
    UserModel user;
    @Column(length = 320, nullable = false)
    String email;

}
