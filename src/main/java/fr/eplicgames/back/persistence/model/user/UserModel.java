package fr.eplicgames.back.persistence.model.user;

import fr.eplicgames.back.persistence.model.content.CharacterModel;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * Entity of table users
 */
@Data
@Entity
@Table(name = "user_account")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(length = 32, nullable = false, unique = true)
    String pseudo;
    @Column(length = 32, nullable = false)
    String firstname;
    @Column(length = 32, nullable = false)
    String lastname;
    @Column(length = 320, unique = true)
    String email;
    @Column(length = 60, nullable = false)
    String password;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    UserType userType;
    @JoinColumn(name = "character_id", columnDefinition = "bigint default 1", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    CharacterModel character;
    @Column(columnDefinition = "boolean default false", nullable = false)
    Boolean active;

}
