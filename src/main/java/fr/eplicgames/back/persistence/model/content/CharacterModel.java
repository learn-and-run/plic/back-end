package fr.eplicgames.back.persistence.model.content;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Entity of table characters
 */
@Data
@Entity
@Table(name = "character")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(length = 32, nullable = false, unique = true)
    String name;

    @ColumnDefault("'no description'")
    @Column(name = "description", nullable = false, length = 1000)
    String description;

    @ColumnDefault("false")
    @Column(name = "in_game", nullable = false)
    Boolean inGame;

    @ColumnDefault("0")
    @Column(name = "sorted", nullable = false)
    Integer sorted;

    @ColumnDefault("'rgb(255,255,255,0.5)'")
    @Column(name = "background", nullable = false, length = 32)
    String background;

    @Column(name = "small_image", nullable = false, length = 256)
    String smallImage;
    @Column(name = "big_image", nullable = false, length = 256)
    String bigImage;
}
