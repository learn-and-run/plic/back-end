package fr.eplicgames.back.persistence.model.content;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Entity
@Table(name = "questions")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class QuestionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false, length = 128)
    String name;

    @Column(nullable = false)
    String question;
    @Column(length = 256, nullable = false)
    String response;
    String badAnswer;
    @Column(nullable = false)
    String explanation;
    String clue1;
    String clue2;
    String clue3;
    String clue4;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    ModuleType module;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    LevelType level;
    @Column(nullable = false)
    Integer difficulty;

}
