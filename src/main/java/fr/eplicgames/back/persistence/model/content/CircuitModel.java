package fr.eplicgames.back.persistence.model.content;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Model of table circuit
 */
@Getter
@Setter
@Entity
@Table(name = "circuits")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CircuitModel implements Serializable {
    private static final long serialVersionUID = 3857965446864285910L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(length = 64, nullable = false, unique = true)
    String name;
    @Column(length = 264, nullable = false)
    String pictureName;

    @Enumerated(EnumType.STRING)
    ModuleType module;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scene_uuid", nullable = false)
    SceneModel scene;

    @ManyToMany
    @JoinTable(name = "circuits_rooms", joinColumns = @JoinColumn(name = "circuit_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id"))
    Set<RoomModel> rooms;
}
