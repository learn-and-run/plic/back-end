package fr.eplicgames.back.persistence.model.relation;

import fr.eplicgames.back.persistence.model.user.UserModel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Entity for users invitation
 */
@Data
@Entity
@Table(name = "user_invitation")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InvitationModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @JoinColumn(name = "user_id")
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel userModel;

    @JoinColumn(name = "relation_id")
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    UserModel relationUserModel;

}
