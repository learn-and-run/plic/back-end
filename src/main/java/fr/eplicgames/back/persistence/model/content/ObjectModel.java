package fr.eplicgames.back.persistence.model.content;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "objects")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ObjectModel {
    @Id
    UUID uuid;

    @Column(nullable = false, unique = true, length = 32)
    String name;

    @Column(nullable = false, length = 128)
    String folder;

    @Column(nullable = false)
    boolean inGame;

    /**
     * Default constructor, do not use it
     */
    protected ObjectModel() {
        this.uuid = UUID.randomUUID();
        this.inGame = false;
    }

    /**
     * Constructor of object model when it does not exist
     *
     * @param name The name of the object
     */
    public ObjectModel(final String name, final String folder) {
        this();
        this.name = name;
        this.folder = folder;
    }

    /**
     * Constructor of a new object the object model
     *
     * @param name   The name of the object
     * @param inGame Precise if the object is available in game
     */
    public ObjectModel(final String name, final String folder, final boolean inGame) {
        this(name, folder);
        this.inGame = inGame;
    }

    /**
     * Constructor of object model when we know the uuid
     *
     * @param uuid   The uuid of the model
     * @param name   The name of the object
     * @param inGame Precise if the object is available in game
     */
    public ObjectModel(final UUID uuid, final String name, final String folder, final boolean inGame) {
        this(name, folder, inGame);
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public boolean isInGame() {
        return inGame;
    }

    public void setInGame(final boolean inGame) {
        this.inGame = inGame;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(final String folder) {
        this.folder = folder;
    }
}
