package fr.eplicgames.back.persistence.model.content;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "rooms_objects")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomObjetModel {
    @Id
    UUID uuid;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    RoomModel room;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_uuid")
    ObjectModel object;

    @Column(name = "position_x", nullable = false)
    Float positionX;

    @Column(name = "position_y", nullable = false)
    Float positionY;

    @Column(name = "position_z", nullable = false)
    Float positionZ;

    @Column(name = "rotation_x", nullable = false)
    Float rotationX;

    @Column(name = "rotation_y", nullable = false)
    Float rotationY;

    @Column(name = "rotation_z", nullable = false)
    Float rotationZ;


    public RoomObjetModel() {
        this.uuid = UUID.randomUUID();
    }

    public RoomObjetModel(final RoomModel room,
                          final ObjectModel object,
                          final Float positionX,
                          final Float positionY,
                          final Float positionZ,
                          final Float rotationX,
                          final Float rotationY,
                          final Float rotationZ) {
        this();
        this.room = room;
        this.object = object;
        this.positionX = positionX;
        this.positionY = positionY;
        this.positionZ = positionZ;
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
    }

    public RoomObjetModel(final UUID uuid,
                          final RoomModel room,
                          final ObjectModel object,
                          final Float positionX,
                          final Float positionY,
                          final Float positionZ,
                          final Float rotationX,
                          final Float rotationY,
                          final Float rotationZ) {
        this.uuid = UUID.randomUUID();
        this.room = room;
        this.object = object;
        this.positionX = positionX;
        this.positionY = positionY;
        this.positionZ = positionZ;
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public RoomModel getRoom() {
        return room;
    }

    public void setRoom(final RoomModel room) {
        this.room = room;
    }

    public ObjectModel getObject() {
        return object;
    }

    public void setObject(final ObjectModel object) {
        this.object = object;
    }

    public Float getPositionX() {
        return positionX;
    }

    public void setPositionX(final Float positionX) {
        this.positionX = positionX;
    }

    public Float getPositionY() {
        return positionY;
    }

    public void setPositionY(final Float positionY) {
        this.positionY = positionY;
    }

    public Float getPositionZ() {
        return positionZ;
    }

    public void setPositionZ(final Float positionZ) {
        this.positionZ = positionZ;
    }

    public Float getRotationX() {
        return rotationX;
    }

    public void setRotationX(final Float rotationX) {
        this.rotationX = rotationX;
    }

    public Float getRotationY() {
        return rotationY;
    }

    public void setRotationY(final Float rotationY) {
        this.rotationY = rotationY;
    }

    public Float getRotationZ() {
        return rotationZ;
    }

    public void setRotationZ(final Float rotationZ) {
        this.rotationZ = rotationZ;
    }
}
