package fr.eplicgames.back.persistence.model.content;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "scenes")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SceneModel {
    @Id
    UUID uuid;

    @Column(nullable = false, unique = true)
    String name;

    public SceneModel() {
        this.uuid = UUID.randomUUID();
    }

    public SceneModel(final String name) {
        this();
        this.name = name;
    }

    public SceneModel(final UUID uuid, final String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
