package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.QuestionModel;
import fr.eplicgames.back.sql_type.ModuleType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for questions tables
 */
public interface QuestionRepository extends JpaRepository<QuestionModel, Long> {

    boolean existsByName(String name);

    List<QuestionModel> findAllByModule(ModuleType module);
}
