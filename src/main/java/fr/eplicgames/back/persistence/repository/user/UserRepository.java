package fr.eplicgames.back.persistence.repository.user;

import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for user table
 */
@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {

    @Query(value = "SELECT u FROM UserModel u WHERE u.pseudo = ?1 OR u.email = ?1")
    UserModel findByPseudoOrEmail(String username);

    UserModel findByPseudo(String pseudo);

    UserModel findByEmail(String email);

}
