package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.sql_type.ModuleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<RoomModel, Long> {

    @Query(value = "SELECT rooms.* FROM rooms INNER JOIN circuits_rooms ON circuits_rooms.room_id = rooms.id " +
            "WHERE circuits_rooms.circuit_id = :id", nativeQuery = true)
    List<RoomModel> findAllRoomsByCircuitId(@Param("id") Long id);

    List<RoomModel> findAllByModule(ModuleType module);

    Optional<RoomModel> findByQuestionId(Long id);
}
