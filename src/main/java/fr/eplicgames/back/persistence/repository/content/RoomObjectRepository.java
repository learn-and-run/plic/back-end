package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.persistence.model.content.RoomObjetModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface RoomObjectRepository extends JpaRepository<RoomObjetModel, UUID> {
    List<RoomObjetModel> findAllByRoom(RoomModel roomModel);

    @Transactional
    void deleteAllByRoom(RoomModel roomModel);
}
