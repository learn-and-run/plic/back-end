package fr.eplicgames.back.persistence.repository;

import fr.eplicgames.back.persistence.model.PendingGameSessionModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for pending game session
 */
@Repository
public interface PendingGameSessionRepository extends JpaRepository<PendingGameSessionModel, Long> {

    PendingGameSessionModel findByUser(UserModel user);
}
