package fr.eplicgames.back.persistence.repository.relation;

import fr.eplicgames.back.persistence.model.relation.InvitationModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface InvitationRepository extends JpaRepository<InvitationModel, Long> {

    @Query(value = "SELECT i.id FROM InvitationModel i WHERE i.userModel = ?1 AND i.relationUserModel = ?2")
    Long findByUserModelAndRelationUserModel(UserModel userModel, UserModel relationUserModel);

    @Query(value = "SELECT i FROM InvitationModel i WHERE i.relationUserModel = ?1")
    List<InvitationModel> findAllByRelationUserModel(UserModel userModel);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM InvitationModel i WHERE (i.userModel = ?1 AND i.relationUserModel = ?2) OR (i.userModel = ?2 AND i.relationUserModel = ?1)")
    void deleteInvitations(UserModel userModel, UserModel relationUserModel);

}
