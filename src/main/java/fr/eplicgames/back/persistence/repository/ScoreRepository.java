package fr.eplicgames.back.persistence.repository;

import fr.eplicgames.back.persistence.model.ScoreModel;
import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.sql_type.ModuleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for scores table
 */
@Repository
public interface ScoreRepository extends JpaRepository<ScoreModel, Long> {

    ScoreModel findByUserAndCircuit(UserModel user, CircuitModel circuit);

    @Query(value = "FROM ScoreModel s INNER JOIN CircuitModel c ON s.circuit = c WHERE s.user = ?1 AND c.module = ?2 ORDER BY c.id")
    List<ScoreModel> findByUserAndModule(UserModel user, ModuleType moduleType);
}
