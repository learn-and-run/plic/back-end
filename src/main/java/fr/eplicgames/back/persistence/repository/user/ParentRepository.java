package fr.eplicgames.back.persistence.repository.user;

import fr.eplicgames.back.persistence.model.user.ParentModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for parent table
 */
@Repository
public interface ParentRepository extends JpaRepository<ParentModel, Long> {

    ParentModel findByUserModel(UserModel userModel);

}
