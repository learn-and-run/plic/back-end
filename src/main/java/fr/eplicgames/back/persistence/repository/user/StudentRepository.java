package fr.eplicgames.back.persistence.repository.user;

import fr.eplicgames.back.persistence.model.user.StudentModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for student table
 */
@Repository
public interface StudentRepository extends JpaRepository<StudentModel, Long> {

    StudentModel findByUserModel(UserModel userModel);

}
