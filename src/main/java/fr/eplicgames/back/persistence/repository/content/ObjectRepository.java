package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.ObjectModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Repository for object table
 */
public interface ObjectRepository extends JpaRepository<ObjectModel, UUID> {
    boolean existsByName(String name);
}
