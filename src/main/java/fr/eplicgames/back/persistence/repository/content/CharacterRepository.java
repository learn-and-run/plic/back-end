package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.CharacterModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for character table
 */
@Repository
public interface CharacterRepository extends JpaRepository<CharacterModel, Long> {

    List<CharacterModel> findAllByInGameOrderBySorted(Boolean inGame);

    Optional<CharacterModel> findByName(String name);
}
