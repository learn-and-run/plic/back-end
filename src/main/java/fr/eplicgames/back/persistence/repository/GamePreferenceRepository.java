package fr.eplicgames.back.persistence.repository;

import fr.eplicgames.back.persistence.model.GamePreferenceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for game preferences
 */
@Repository
public interface GamePreferenceRepository extends JpaRepository<GamePreferenceModel, Long> {
    Optional<GamePreferenceModel> findByUser_Id(Long userId);
}
