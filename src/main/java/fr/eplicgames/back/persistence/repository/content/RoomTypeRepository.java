package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.RoomTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoomTypeRepository extends JpaRepository<RoomTypeModel, UUID> {
    boolean existsByType(String type);
}
