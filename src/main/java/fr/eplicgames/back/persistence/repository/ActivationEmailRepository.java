package fr.eplicgames.back.persistence.repository;

import fr.eplicgames.back.persistence.model.ActivationEmailModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for activation_email table
 */
@Repository
public interface ActivationEmailRepository extends JpaRepository<ActivationEmailModel, String> {

    ActivationEmailModel findByUser(UserModel userModel);

}
