package fr.eplicgames.back.persistence.repository;

import fr.eplicgames.back.persistence.model.ResetPasswordModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for reset_password table
 */
@Repository
public interface ResetPasswordRepository extends JpaRepository<ResetPasswordModel, String> {

    ResetPasswordModel findByUser(UserModel userModel);

}
