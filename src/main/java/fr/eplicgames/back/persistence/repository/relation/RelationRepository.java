package fr.eplicgames.back.persistence.repository.relation;

import fr.eplicgames.back.persistence.model.relation.RelationModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.sql_type.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RelationRepository extends JpaRepository<RelationModel, Long> {

    @Query(value = "SELECT r FROM RelationModel r WHERE r.userModel = ?1")
    List<RelationModel> findAllByUserModel(UserModel userModel);

    @Query(value = "SELECT r FROM RelationModel r WHERE r.userModel = ?1 AND r.relationUserModel.userType = ?2")
    List<RelationModel> findAllByUserModelAndByType(UserModel userModel, UserType userType);

    RelationModel findByUserModelAndRelationUserModel(UserModel userModel, UserModel relationUserModel);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM RelationModel r WHERE (r.userModel = ?1 AND r.relationUserModel = ?2) OR (r.userModel = ?2 AND r.relationUserModel = ?1)")
    void deleteRelations(UserModel userModel, UserModel relationUserModel);

}
