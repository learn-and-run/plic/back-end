package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.SceneModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SceneRepository extends JpaRepository<SceneModel, UUID> {
    boolean existsByName(String name);
}
