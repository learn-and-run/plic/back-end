package fr.eplicgames.back.persistence.repository.content;

import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.sql_type.ModuleType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for circuit table
 */
public interface CircuitRepository extends JpaRepository<CircuitModel, Long> {
    Optional<CircuitModel> findByName(String name);

    List<CircuitModel> findAllByModule(ModuleType module);
}
