package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.*;
import fr.eplicgames.back.domain.service.ScoreService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.persistence.model.content.SceneModel;
import fr.eplicgames.back.persistence.repository.content.CircuitRepository;
import fr.eplicgames.back.persistence.repository.content.RoomRepository;
import fr.eplicgames.back.persistence.repository.content.SceneRepository;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CircuitService {

    final CircuitRepository circuitRepository;
    final RoomRepository roomRepository;
    final SceneRepository sceneRepository;
    final ScoreService scoreService;

    /**
     * Get all circuit that correspond to a specific module
     *
     * @param module The module type of circuits
     * @return Returns the list of all modules that correspond to the module or all if TOUS
     */
    public List<CircuitEntity> getAllCircuitOfModule(final ModuleType module) {
        final List<CircuitModel> circuitModels = module.equals(ModuleType.TOUS)
                ? circuitRepository.findAll()
                : circuitRepository.findAllByModule(module);

        return circuitModels.stream().map(circuitModel ->
                new CircuitEntity(
                        circuitModel.getId(),
                        circuitModel.getName(),
                        circuitModel.getPictureName(),
                        circuitModel.getModule(),
                        new SceneEntity(
                                circuitModel.getScene().getUuid(),
                                circuitModel.getScene().getName()
                        ),
                        circuitModel.getRooms().stream().map(roomModel ->
                                new RoomEntity(
                                        roomModel.getId(),
                                        roomModel.getModule(),
                                        new QuestionEntity(
                                                roomModel.getQuestion().getId(),
                                                roomModel.getQuestion().getName(),
                                                roomModel.getQuestion().getQuestion(),
                                                roomModel.getQuestion().getResponse(),
                                                roomModel.getQuestion().getBadAnswer(),
                                                roomModel.getQuestion().getExplanation(),
                                                roomModel.getQuestion().getClue1(),
                                                roomModel.getQuestion().getClue2(),
                                                roomModel.getQuestion().getClue3(),
                                                roomModel.getQuestion().getClue4(),
                                                roomModel.getQuestion().getModule(),
                                                roomModel.getQuestion().getLevel(),
                                                roomModel.getQuestion().getDifficulty()
                                        ),
                                        roomModel.getExpectedTime(),
                                        new RoomTypeEntity(
                                                roomModel.getRoomType().getUuid(),
                                                roomModel.getRoomType().getType()
                                        )

                                )
                        ).collect(Collectors.toCollection(HashSet::new))
                )
        ).collect(Collectors.toList());
    }

    /**
     * Create a circuit in the database if it does not exist
     *
     * @param circuitEntity The circuit entity that contains elements
     * @throws CodeMessageException Throws an exception if the circuit is already present
     */
    @IsAdmin
    public CircuitEntity createCircuitIfNotExist(final CircuitEntity circuitEntity) throws CodeMessageException {

        if (circuitRepository.findByName(circuitEntity.name).isPresent())
            throw new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_ALREADY_EXIST);

        final CircuitModel circuitModel = new CircuitModel();
        circuitModel.setName(circuitEntity.name);
        circuitModel.setPictureName(circuitEntity.pictureName);
        circuitModel.setModule(circuitEntity.module);

        final SceneModel sceneModel = sceneRepository.findById(circuitEntity.scene.uuid).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.SCENE_NOT_EXIST)
        );

        circuitModel.setScene(sceneModel);

        final CircuitModel newCircuitModel = circuitRepository.save(circuitModel);

        scoreService.AddScoresToStudents(newCircuitModel);

        return new CircuitEntity(
                newCircuitModel.getId(),
                newCircuitModel.getName(),
                newCircuitModel.getPictureName(),
                newCircuitModel.getModule(),
                new SceneEntity(
                        newCircuitModel.getScene().getUuid(),
                        newCircuitModel.getScene().getName()
                ),
                new HashSet<>()
        );
    }

    /**
     * Update a circuit to another
     *
     * @param circuitEntity The new elements for the circuit
     * @throws CodeMessageException Throws an exception if the circuit does not exist
     */
    @IsAdmin
    public CircuitEntity updateCircuit(final CircuitEntity circuitEntity) throws CodeMessageException {

        final CircuitModel circuitModel = circuitRepository.findById(circuitEntity.id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_NOT_EXIST)
        );

        circuitModel.setName(circuitEntity.name);
        circuitModel.setPictureName(circuitEntity.pictureName);
        circuitModel.setModule(circuitEntity.module);

        final SceneModel sceneModel = sceneRepository.findById(circuitEntity.scene.uuid).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.SCENE_NOT_EXIST)
        );

        circuitModel.setScene(sceneModel);

        final CircuitModel newCircuitModel = circuitRepository.save(circuitModel);
        return new CircuitEntity(
                newCircuitModel.getId(),
                newCircuitModel.getName(),
                newCircuitModel.getPictureName(),
                newCircuitModel.getModule(),
                new SceneEntity(
                        newCircuitModel.getScene().getUuid(),
                        newCircuitModel.getScene().getName()
                ),
                newCircuitModel.getRooms().stream().map(roomModel ->
                        new RoomEntity(
                                roomModel.getId(),
                                roomModel.getModule(),
                                new QuestionEntity(
                                        roomModel.getQuestion().getId(),
                                        roomModel.getQuestion().getName(),
                                        roomModel.getQuestion().getQuestion(),
                                        roomModel.getQuestion().getResponse(),
                                        roomModel.getQuestion().getBadAnswer(),
                                        roomModel.getQuestion().getExplanation(),
                                        roomModel.getQuestion().getClue1(),
                                        roomModel.getQuestion().getClue2(),
                                        roomModel.getQuestion().getClue3(),
                                        roomModel.getQuestion().getClue4(),
                                        roomModel.getQuestion().getModule(),
                                        roomModel.getQuestion().getLevel(),
                                        roomModel.getQuestion().getDifficulty()
                                ),
                                roomModel.getExpectedTime(),
                                new RoomTypeEntity(
                                        roomModel.getRoomType().getUuid(),
                                        roomModel.getRoomType().getType()
                                )

                        )
                ).collect(Collectors.toSet())
        );
    }

    /**
     * Delete a circuit from the database if it exist
     *
     * @param id The id of the circuit to delete
     * @throws CodeMessageException Throws exception if the circuit does not exist
     */
    @IsAdmin
    public CircuitEntity deleteCircuit(final Long id) throws CodeMessageException {

        final CircuitModel circuitModel = circuitRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_NOT_EXIST)
        );
        circuitRepository.deleteById(id);
        return new CircuitEntity(
                circuitModel.getId(),
                circuitModel.getName(),
                circuitModel.getPictureName(),
                circuitModel.getModule(),
                new SceneEntity(
                        circuitModel.getScene().getUuid(),
                        circuitModel.getScene().getName()
                ),
                circuitModel.getRooms().stream().map(roomModel ->
                        new RoomEntity(
                                roomModel.getId(),
                                roomModel.getModule(),
                                new QuestionEntity(
                                        roomModel.getQuestion().getId(),
                                        roomModel.getQuestion().getName(),
                                        roomModel.getQuestion().getQuestion(),
                                        roomModel.getQuestion().getResponse(),
                                        roomModel.getQuestion().getBadAnswer(),
                                        roomModel.getQuestion().getExplanation(),
                                        roomModel.getQuestion().getClue1(),
                                        roomModel.getQuestion().getClue2(),
                                        roomModel.getQuestion().getClue3(),
                                        roomModel.getQuestion().getClue4(),
                                        roomModel.getQuestion().getModule(),
                                        roomModel.getQuestion().getLevel(),
                                        roomModel.getQuestion().getDifficulty()
                                ),
                                roomModel.getExpectedTime(),
                                new RoomTypeEntity(
                                        roomModel.getRoomType().getUuid(),
                                        roomModel.getRoomType().getType()
                                )
                        )
                ).collect(Collectors.toSet())
        );
    }

    /**
     * Update the list of rooms of the given circuit
     *
     * @param circuitId The circuit id of the circuit to add rooms
     * @param roomsIds  The list of room ids to add to the given circuit
     * @return Return the circuit entity with its list of circuit
     * @throws CodeMessageException Throw an exception if the circuit or one of the rooms does not exist
     */
    @IsAdmin
    public CircuitEntity updateRoomsOfCircuit(final Long circuitId, final List<Long> roomsIds)
            throws CodeMessageException {
        final CircuitModel circuitModel = circuitRepository.findById(circuitId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_NOT_EXIST)
        );

        circuitModel.setRooms(new HashSet<>());

        for (final Long roomId : roomsIds) {
            final RoomModel roomModel = roomRepository.findById(roomId).orElseThrow(() ->
                    new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST)
            );
            circuitModel.getRooms().add(roomModel);
        }

        final CircuitModel newCircuitModel = circuitRepository.save(circuitModel);

        return new CircuitEntity(
                newCircuitModel.getId(),
                newCircuitModel.getName(),
                newCircuitModel.getPictureName(),
                newCircuitModel.getModule(),
                new SceneEntity(
                        newCircuitModel.getScene().getUuid(),
                        newCircuitModel.getScene().getName()
                ),
                newCircuitModel.getRooms().stream().map(roomModel ->
                        new RoomEntity(
                                roomModel.getId(),
                                roomModel.getModule(),
                                new QuestionEntity(
                                        roomModel.getQuestion().getId(),
                                        roomModel.getQuestion().getName(),
                                        roomModel.getQuestion().getQuestion(),
                                        roomModel.getQuestion().getResponse(),
                                        roomModel.getQuestion().getBadAnswer(),
                                        roomModel.getQuestion().getExplanation(),
                                        roomModel.getQuestion().getClue1(),
                                        roomModel.getQuestion().getClue2(),
                                        roomModel.getQuestion().getClue3(),
                                        roomModel.getQuestion().getClue4(),
                                        roomModel.getQuestion().getModule(),
                                        roomModel.getQuestion().getLevel(),
                                        roomModel.getQuestion().getDifficulty()
                                ),
                                roomModel.getExpectedTime(),
                                new RoomTypeEntity(
                                        roomModel.getRoomType().getUuid(),
                                        roomModel.getRoomType().getType()
                                )
                        )
                ).collect(Collectors.toSet())
        );
    }
}
