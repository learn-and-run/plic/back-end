package fr.eplicgames.back.domain.service.user;

import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.ActivationEmailModel;
import fr.eplicgames.back.persistence.model.ResetPasswordModel;
import fr.eplicgames.back.persistence.model.content.CharacterModel;
import fr.eplicgames.back.persistence.model.user.StudentModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.ActivationEmailRepository;
import fr.eplicgames.back.persistence.repository.ResetPasswordRepository;
import fr.eplicgames.back.persistence.repository.content.CharacterRepository;
import fr.eplicgames.back.persistence.repository.user.StudentRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.security.component.CustomSessionHandler;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateService extends AbstractService {

    final ActivationEmailRepository activationEmailRepository;
    final ResetPasswordRepository resetPasswordRepository;
    final CharacterRepository characterRepository;
    final StudentRepository studentRepository;
    final EmailService emailService;
    final BCryptPasswordEncoder bCryptPasswordEncoder;
    final PersistentTokenRepository persistentTokenRepository;
    final CustomSessionHandler customSessionHandler;

    public UpdateService(final UserRepository userRepository,
                         final ModelMapper modelMapper,
                         final ActivationEmailRepository activationEmailRepository,
                         final ResetPasswordRepository resetPasswordRepository,
                         final CharacterRepository characterRepository,
                         final StudentRepository studentRepository,
                         final EmailService emailService,
                         final BCryptPasswordEncoder bCryptPasswordEncoder,
                         final PersistentTokenRepository persistentTokenRepository,
                         final CustomSessionHandler customSessionHandler) {
        super(userRepository, modelMapper);

        this.activationEmailRepository = activationEmailRepository;
        this.resetPasswordRepository = resetPasswordRepository;
        this.characterRepository = characterRepository;
        this.studentRepository = studentRepository;
        this.emailService = emailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.persistentTokenRepository = persistentTokenRepository;
        this.customSessionHandler = customSessionHandler;
    }

    /**
     * Update the character of the current user
     *
     * @param id The id that correspond to the character
     */
    public void updateCharacter(final Long id) throws CodeMessageException {
        final CharacterModel characterEntity = characterRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CHARACTER_NOT_EXIST)
        );

        final UserModel currentUser = getCurrentUser();
        currentUser.setCharacter(characterEntity);
        userRepository.save(currentUser);
    }

    /**
     * Update the current user
     *
     * @param userEntity userEntity with new data
     * @return Returns the updated user entity
     * @throws CodeMessageException Throws an exception if the user cannot be updated
     */
    public UserEntity updateUser(final UserEntity userEntity) throws CodeMessageException {
        final UserModel userModel = getCurrentUser();

        if (!userModel.getPseudo().equals(userEntity.pseudo) && !isPseudoAvailable(userEntity.pseudo))
            throw new CodeMessageException(CodeMessageExceptionEnum.PSEUDO_ALREADY_TAKEN);

        userModel.setPseudo(userEntity.pseudo);
        userModel.setFirstname(userEntity.firstname);
        userModel.setLastname(userEntity.lastname);
        final UserModel updatedUserModel = userRepository.save(userModel);
        return new UserEntity(
                updatedUserModel.getId(),
                updatedUserModel.getPseudo(),
                updatedUserModel.getFirstname(),
                updatedUserModel.getLastname(),
                updatedUserModel.getEmail(),
                null,
                null,
                updatedUserModel.getActive(),
                updatedUserModel.getUserType()
        );
    }

    /**
     * Update the current user (student)
     *
     * @param studentEntity The new data of the student
     * @throws CodeMessageException Throws an exception if the student failed to be updated
     */
    public StudentEntity updateStudent(final StudentEntity studentEntity) throws CodeMessageException {
        final Long id = getCurrentUserId();
        final StudentModel studentModel = studentRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_USER)
        );

        UserEntity userEntity = updateUser(studentEntity);

        studentModel.setLevelType(studentEntity.levelType);
        final StudentModel studentSaved = studentRepository.save(studentModel);

        return new StudentEntity(userEntity.id,
                userEntity.pseudo,
                userEntity.firstname,
                userEntity.lastname,
                userEntity.email,
                userEntity.password,
                userEntity.character,
                userEntity.active,
                studentSaved.getLevelType());
    }

    /**
     * Send a confirmation email to change the email of the current user
     *
     * @param email The new email
     * @throws CodeMessageException Throws an exception if the email cannot be changed to the new email
     */
    public void updateEmail(final String email) throws CodeMessageException {
        final UserModel userModel = getCurrentUser();
        if (userModel.getEmail().equals(email))
            return;

        if (!isEmailAvailable(email))
            throw new CodeMessageException(CodeMessageExceptionEnum.EMAIL_ALREADY_USED);

        emailService.sendUpdateEmail(userModel, email);
    }

    /**
     * Validate the change of the email
     *
     * @param id The id of the activation mail
     * @throws CodeMessageException Throws an exception if the new email cannot be validated
     */
    public void validateNewEmail(final String id) throws CodeMessageException {
        final ActivationEmailModel activationEmailModel = activationEmailRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.WRONG_ACTIVATION_LINK)
        );

        final UserModel userModel = activationEmailModel.getUser();
        if (!userModel.getActive())
            throw new CodeMessageException(CodeMessageExceptionEnum.ILLEGAL_ACCESS);

        final String tmpEmail = activationEmailModel.getEmail();
        activationEmailRepository.delete(activationEmailModel);

        if (!isEmailAvailable(tmpEmail))
            throw new CodeMessageException(CodeMessageExceptionEnum.EMAIL_ALREADY_USED);

        userModel.setEmail(tmpEmail);
        userRepository.save(userModel);

        emailService.sendUpdateEmail(tmpEmail);
    }

    /**
     * Update the password of the current user
     *
     * @param password The new password to set to the user
     * @param session  The HttpSession linked to the user
     * @throws CodeMessageException Throws an exception if the password cannot be changed
     */
    public void updatePassword(final String password, final HttpSession session) throws CodeMessageException {
        final UserModel userModel = getCurrentUser();
        userModel.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(userModel);
        invalidateUser(userModel.getId(), session);

        emailService.sendUpdatePassword(userModel.getEmail());
    }

    /**
     * Send a link to reset the password
     *
     * @param email The email on which send a link to reset the password
     * @throws CodeMessageException Throws an exception if the links cannot be sent
     */
    public void askResetPassword(final String email) throws CodeMessageException {
        final UserModel userModel = userRepository.findByEmail(email);
        if (userModel == null)
            return;

        emailService.sendResetPasswordEmail(userModel);
    }

    /**
     * Reset the password of a user with the link he received by email
     *
     * @param id       The id of the reset password link
     * @param password The new password to give to the account
     * @throws CodeMessageException Throws an exception if the password failed to be reset
     */
    public void resetPassword(final String id, final String password, final HttpSession session)
            throws CodeMessageException {
        final ResetPasswordModel resetPasswordModel = resetPasswordRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.WRONG_RESET_PASSWORD_LINK)
        );

        final UserModel userModel = resetPasswordModel.getUser();
        resetPasswordRepository.deleteById(id);

        userModel.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(userModel);
        invalidateUser(userModel.getId(), session);

        emailService.sendUpdatePassword(userModel.getEmail());
    }

    /**
     * Delete the current user
     *
     * @param session The Http Session associated to the user
     * @throws CodeMessageException Throws an exception if the user failed to be deleted
     */
    public void deleteUser(final HttpSession session) throws CodeMessageException {
        final UserModel userModel = getCurrentUser();
        invalidateUser(userModel.getId(), session);
        userRepository.delete(userModel);

        emailService.sendDeleteUser(userModel.getEmail());
    }

    private void invalidateUser(final Long id, final HttpSession session) {
        session.invalidate();
        customSessionHandler.expireUserSessions(String.valueOf(id));
        persistentTokenRepository.removeUserTokens(String.valueOf(id));
    }
}
