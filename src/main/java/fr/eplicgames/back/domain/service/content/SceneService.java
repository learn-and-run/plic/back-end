package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.SceneEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.SceneModel;
import fr.eplicgames.back.persistence.repository.content.SceneRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SceneService {

    final SceneRepository sceneRepository;

    /**
     * Create an scene in database
     *
     * @param sceneEntity The new scene to create
     * @return Return the created scene
     * @throws CodeMessageException Throws an exception if the scene already exists
     */
    public SceneEntity createScene(final SceneEntity sceneEntity) throws CodeMessageException {
        final SceneModel sceneModel = new SceneModel(sceneEntity.name);
        return saveScene(sceneModel);
    }

    /**
     * Update an scene from database
     *
     * @param sceneEntity The updated scene entity
     * @return Return the updated scene
     * @throws CodeMessageException Throws an exception if the new name is already taken or if the uuid does not exist
     */
    public SceneEntity updateScene(final SceneEntity sceneEntity) throws CodeMessageException {

        if (!sceneRepository.existsById(sceneEntity.uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.SCENE_NOT_EXIST);

        final SceneModel sceneModel = new SceneModel(sceneEntity.uuid, sceneEntity.name);

        return saveScene(sceneModel);
    }

    /**
     * Save the scene in database
     *
     * @param sceneModel The scene model to save
     * @return Returns the saved scene
     * @throws CodeMessageException Throws an exception if the name is already taken
     */
    private SceneEntity saveScene(final SceneModel sceneModel) throws CodeMessageException {
        if (sceneRepository.existsByName(sceneModel.getName()))
            throw new CodeMessageException(CodeMessageExceptionEnum.SCENE_ALREADY_EXIST);

        final SceneModel newSceneModel = sceneRepository.save(sceneModel);

        return new SceneEntity(newSceneModel.getUuid(), newSceneModel.getName());
    }

    /**
     * Delete an scene with its uuid
     *
     * @param uuid The uuid of the scene to delete
     * @throws CodeMessageException Throws an exception if the uuid does not exist
     */
    public void deleteSceneByUuid(final UUID uuid) throws CodeMessageException {
        if (!sceneRepository.existsById(uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.SCENE_NOT_EXIST);

        sceneRepository.deleteById(uuid);
    }

    /**
     * Get all scenes in database
     *
     * @return Returns scenes in database
     */
    public List<SceneEntity> getScenes() {
        return sceneRepository.findAll().stream().map(sceneModel -> new SceneEntity(
                sceneModel.getUuid(),
                sceneModel.getName()
        )).collect(Collectors.toList());
    }
}
