package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.*;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.QuestionModel;
import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.persistence.model.content.RoomObjetModel;
import fr.eplicgames.back.persistence.model.content.RoomTypeModel;
import fr.eplicgames.back.persistence.repository.content.*;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.utils.Vector3D;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomService {

    final RoomRepository roomRepository;
    final RoomObjectRepository roomObjectRepository;
    final ObjectRepository objectRepository;
    final RoomTypeRepository roomTypeRepository;
    final QuestionRepository questionRepository;

    /**
     * Get all rooms associated to the circuit
     *
     * @param circuitId The id of the circuit to load rooms
     * @return Returns rooms associated to the circuit
     */
    public List<RoomEntity> getRoomsByCircuitId(final Long circuitId) {
        return roomRepository.findAllRoomsByCircuitId(circuitId).stream().map(roomModel ->
                new RoomEntity(
                        roomModel.getId(),
                        roomModel.getModule(),
                        new QuestionEntity(
                                roomModel.getQuestion().getId(),
                                roomModel.getQuestion().getName(),
                                roomModel.getQuestion().getQuestion(),
                                roomModel.getQuestion().getResponse(),
                                roomModel.getQuestion().getBadAnswer(),
                                roomModel.getQuestion().getExplanation(),
                                roomModel.getQuestion().getClue1(),
                                roomModel.getQuestion().getClue2(),
                                roomModel.getQuestion().getClue3(),
                                roomModel.getQuestion().getClue4(),
                                roomModel.getQuestion().getModule(),
                                roomModel.getQuestion().getLevel(),
                                roomModel.getQuestion().getDifficulty()
                        ),
                        roomModel.getExpectedTime(),
                        new RoomTypeEntity(
                                roomModel.getRoomType().getUuid(), roomModel.getRoomType().getType()
                        )
                )

        ).collect(Collectors.toList());
    }

    /**
     * Generate random int values separate by space
     *
     * @param nbValues The nb of value to generate
     * @param bound    The max values excluded of the random
     * @return Returns the string with the number of random values
     */
    private String generateRandomIntValues(final Integer nbValues, final Integer bound) {
        final StringBuilder stringBuilder = new StringBuilder();
        final Random random = new Random();

        for (int i = 0; i < nbValues - 1; ++i) stringBuilder.append(random.nextInt(bound)).append(' ');
        if (nbValues > 0) stringBuilder.append(random.nextInt(bound));

        return stringBuilder.toString();
    }

    /**
     * Get all rooms of the chosen module
     *
     * @param module The module to filter rooms
     * @return Returns rooms associated to the module
     */
    public List<RoomEntity> getRoomsByModule(final ModuleType module) {

        final List<RoomModel> roomModels = module.equals(ModuleType.TOUS)
                ? roomRepository.findAll()
                : roomRepository.findAllByModule(module);

        return roomModels.stream().map(roomModel ->
                new RoomEntity(
                        roomModel.getId(),
                        roomModel.getModule(),
                        new QuestionEntity(
                                roomModel.getQuestion().getId(),
                                roomModel.getQuestion().getName(),
                                roomModel.getQuestion().getQuestion(),
                                roomModel.getQuestion().getResponse(),
                                roomModel.getQuestion().getBadAnswer(),
                                roomModel.getQuestion().getExplanation(),
                                roomModel.getQuestion().getClue1(),
                                roomModel.getQuestion().getClue2(),
                                roomModel.getQuestion().getClue3(),
                                roomModel.getQuestion().getClue4(),
                                roomModel.getQuestion().getModule(),
                                roomModel.getQuestion().getLevel(),
                                roomModel.getQuestion().getDifficulty()
                        ),
                        roomModel.getExpectedTime(),
                        new RoomTypeEntity(
                                roomModel.getRoomType().getUuid(),
                                roomModel.getRoomType().getType()
                        )
                )
        ).collect(Collectors.toList());
    }

    /**
     * Create a room if it does not exist and the question id exists
     *
     * @param roomEntity Data of the room
     * @param questionId The id of the question to link
     * @throws CodeMessageException Throws an exception if the room already exist or if the question id does not exist
     */
    @IsAdmin
    public RoomEntity createRoom(final RoomEntity roomEntity, final Long questionId) throws CodeMessageException {

        if (roomRepository.findByQuestionId(questionId).isPresent())
            throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_ALREADY_EXIST);

        final QuestionModel questionModel = questionRepository.findById(questionId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.QUESTION_NOT_EXIST)
        );

        final RoomModel roomModel = new RoomModel();
        roomModel.setModule(roomEntity.module);
        roomModel.setQuestion(questionModel);
        roomModel.setExpectedTime(roomEntity.expectedTime);


        final RoomTypeModel roomTypeModel = roomTypeRepository.findById(roomEntity.roomType.uuid).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.ROOM_TYPE_NOT_EXIST)
        );

        roomModel.setRoomType(roomTypeModel);

        final RoomModel newRoomModel = roomRepository.save(roomModel);
        return new RoomEntity(
                newRoomModel.getId(),
                newRoomModel.getModule(),
                new QuestionEntity(
                        newRoomModel.getQuestion().getId(),
                        newRoomModel.getQuestion().getName(),
                        newRoomModel.getQuestion().getQuestion(),
                        newRoomModel.getQuestion().getResponse(),
                        newRoomModel.getQuestion().getBadAnswer(),
                        newRoomModel.getQuestion().getExplanation(),
                        newRoomModel.getQuestion().getClue1(),
                        newRoomModel.getQuestion().getClue2(),
                        newRoomModel.getQuestion().getClue3(),
                        newRoomModel.getQuestion().getClue4(),
                        newRoomModel.getQuestion().getModule(),
                        newRoomModel.getQuestion().getLevel(),
                        newRoomModel.getQuestion().getDifficulty()
                ),
                newRoomModel.getExpectedTime(),
                new RoomTypeEntity(
                        newRoomModel.getRoomType().getUuid(),
                        newRoomModel.getRoomType().getType()
                )
        );
    }

    /**
     * Update a room
     *
     * @param roomEntity The new content of the room
     * @param questionId The id of the question associated to the room
     * @throws CodeMessageException Throws an exception in the room does not exist or if the question id
     *                              is already assign to another room.
     */
    @IsAdmin
    public RoomEntity updateRoom(final RoomEntity roomEntity, final Long questionId) throws CodeMessageException {

        final RoomModel roomModel = roomRepository.findById(roomEntity.id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST)
        );

        if (!roomModel.getQuestion().getId().equals(questionId)) {
            // search new question
            final QuestionModel questionModel = questionRepository.findById(questionId).orElseThrow(() ->
                    new CodeMessageException(CodeMessageExceptionEnum.QUESTION_NOT_EXIST)
            );
            roomModel.setQuestion(questionModel);
        }

        roomModel.setModule(roomEntity.module);
        roomModel.setExpectedTime(roomEntity.expectedTime);

        final RoomTypeModel roomTypeModel = roomTypeRepository.findById(roomEntity.roomType.uuid).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.ROOM_TYPE_NOT_EXIST)
        );

        roomModel.setRoomType(roomTypeModel);

        final RoomModel newRoomModel = roomRepository.save(roomModel);

        return new RoomEntity(
                newRoomModel.getId(),
                newRoomModel.getModule(),
                new QuestionEntity(
                        newRoomModel.getQuestion().getId(),
                        newRoomModel.getQuestion().getName(),
                        newRoomModel.getQuestion().getQuestion(),
                        newRoomModel.getQuestion().getResponse(),
                        newRoomModel.getQuestion().getBadAnswer(),
                        newRoomModel.getQuestion().getExplanation(),
                        newRoomModel.getQuestion().getClue1(),
                        newRoomModel.getQuestion().getClue2(),
                        newRoomModel.getQuestion().getClue3(),
                        newRoomModel.getQuestion().getClue4(),
                        newRoomModel.getQuestion().getModule(),
                        newRoomModel.getQuestion().getLevel(),
                        newRoomModel.getQuestion().getDifficulty()
                ),
                newRoomModel.getExpectedTime(),
                new RoomTypeEntity(
                        newRoomModel.getRoomType().getUuid(),
                        newRoomModel.getRoomType().getType()
                )
        );
    }

    /**
     * Delete a room from database
     *
     * @param id The id of the room to delete
     * @throws CodeMessageException Throws an exception if the room does not exist
     */
    @IsAdmin
    public void deleteRoom(final Long id) throws CodeMessageException {
        if (!roomRepository.existsById(id)) throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST);
        roomRepository.deleteById(id);
    }

    /**
     * Get all Objects present in the room given by roomId
     *
     * @param roomId The id of the room
     * @return Returns all objects present in the room
     * @throws CodeMessageException Throws an exception if the room does not exist
     */
    public List<RoomObjectEntity> getAllObjectsOfRoom(final Long roomId) throws CodeMessageException {
        final RoomModel roomModel = roomRepository.findById(roomId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST)
        );
        return roomObjectRepository.findAllByRoom(roomModel).stream().map(roomObjetModel ->
                new RoomObjectEntity(
                        new ObjectEntity(
                                roomObjetModel.getObject().getUuid(),
                                roomObjetModel.getObject().getName(),
                                roomObjetModel.getObject().getFolder(),
                                roomObjetModel.getObject().isInGame()
                        ),
                        new Vector3D<>(
                                roomObjetModel.getPositionX(),
                                roomObjetModel.getPositionY(),
                                roomObjetModel.getPositionZ()
                        ),
                        new Vector3D<>(
                                roomObjetModel.getRotationX(),
                                roomObjetModel.getRotationY(),
                                roomObjetModel.getRotationZ()
                        )

                )
        ).collect(Collectors.toList());
    }

    /**
     * Update the list of object present in the given room
     *
     * @param roomObjectEntities The list of objects present in room
     * @param roomId             The id of the room
     * @return The objects that are put in the database
     * @throws CodeMessageException Throws an exception if a given id does not exist
     */
    public List<RoomObjectEntity> updateObjectsOfRoom(final List<RoomObjectEntity> roomObjectEntities,
                                                      final Long roomId) throws CodeMessageException {
        final RoomModel roomModel = roomRepository.findById(roomId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST)
        );

        roomObjectRepository.deleteAllByRoom(roomModel);

        final List<RoomObjetModel> roomObjetModels = new ArrayList<>();

        for (final RoomObjectEntity roomObjectEntity : roomObjectEntities)
            roomObjetModels.add(new RoomObjetModel(
                    roomModel,
                    objectRepository.findById(roomObjectEntity.object.uuid).orElseThrow(() ->
                            new CodeMessageException(CodeMessageExceptionEnum.OBJECT_NOT_EXIST)
                    ),
                    roomObjectEntity.position.x,
                    roomObjectEntity.position.y,
                    roomObjectEntity.position.z,
                    roomObjectEntity.rotation.x,
                    roomObjectEntity.rotation.y,
                    roomObjectEntity.rotation.z
            ));

        final List<RoomObjetModel> updatedRoomObjectModels = roomObjectRepository.saveAll(roomObjetModels);


        return updatedRoomObjectModels.stream().map(updatedRoomObjectModel ->
                new RoomObjectEntity(
                        new ObjectEntity(
                                updatedRoomObjectModel.getObject().getUuid(),
                                updatedRoomObjectModel.getObject().getName(),
                                updatedRoomObjectModel.getObject().getFolder(),
                                updatedRoomObjectModel.getObject().isInGame()
                        ),
                        new Vector3D<>(
                                updatedRoomObjectModel.getPositionX(),
                                updatedRoomObjectModel.getPositionY(),
                                updatedRoomObjectModel.getPositionZ()
                        ),
                        new Vector3D<>(
                                updatedRoomObjectModel.getRotationX(),
                                updatedRoomObjectModel.getRotationY(),
                                updatedRoomObjectModel.getRotationZ()
                        )
                )).collect(Collectors.toList());
    }
}
