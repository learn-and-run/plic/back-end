package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.persistence.model.content.CharacterModel;
import fr.eplicgames.back.persistence.repository.content.CharacterRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterService {

    final CharacterRepository characterRepository;

    /**
     * Get all created characters
     *
     * @return Returns the list of created characters
     */
    public List<CharacterEntity> getAllCharacters() {
        final List<CharacterModel> characterModels = characterRepository.findAll();
        return characterModels.stream().map(characterModel ->
                new CharacterEntity(
                        characterModel.getId(),
                        characterModel.getName(),
                        characterModel.getDescription(),
                        characterModel.getBackground(),
                        characterModel.getInGame(),
                        characterModel.getSorted(),
                        characterModel.getSmallImage(),
                        characterModel.getBigImage()
                )
        ).collect(Collectors.toList());
    }


    /**
     * Get all characters that are in game ordered by sort preference
     *
     * @return Returns all characters available in game sorted by preference
     */
    public List<CharacterEntity> getAllCharactersInGame() {
        final List<CharacterModel> characterModels = characterRepository.findAllByInGameOrderBySorted(true);
        return characterModels.stream().map(characterModel ->
                new CharacterEntity(
                        characterModel.getId(),
                        characterModel.getName(),
                        characterModel.getDescription(),
                        characterModel.getBackground(),
                        characterModel.getInGame(),
                        characterModel.getSorted(),
                        characterModel.getSmallImage(),
                        characterModel.getBigImage()
                )
        ).collect(Collectors.toList());
    }
}
