package fr.eplicgames.back.domain.service.user;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.ActivationEmailModel;
import fr.eplicgames.back.persistence.model.ResetPasswordModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.ActivationEmailRepository;
import fr.eplicgames.back.persistence.repository.ResetPasswordRepository;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.UUID;

/**
 * Service to send email to users
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailService {

    final ActivationEmailRepository activationEmailRepository;
    final ResetPasswordRepository resetPasswordRepository;
    @Value("${server.angular.address}")
    String frontAddress;
    @Value("${spring.email.username}")
    String username;
    @Value("${spring.email.password}")
    String password;

    public EmailService(final ActivationEmailRepository activationEmailRepository,
                        final ResetPasswordRepository resetPasswordRepository) {
        this.activationEmailRepository = activationEmailRepository;
        this.resetPasswordRepository = resetPasswordRepository;
    }

    /**
     * Send an email
     *
     * @param email   The email address on which send the email
     * @param header  The header of the email to send
     * @param message The message to send to the email
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    private void sendEmail(final String email, final String header, final String message) throws CodeMessageException {
        final Email from = new Email(username);
        final Email to = new Email(email);
        final Content content = new Content("text/plain", message);
        final Mail mail = new Mail(from, header, to, content);

        final SendGrid sg = new SendGrid(password);
        final Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sg.api(request);
        } catch (final IOException e) {
            throw new CodeMessageException(CodeMessageExceptionEnum.ERROR_SENDING_EMAIL);
        }
    }

    /**
     * Tell if the ActivationEmailId is unique
     *
     * @param id The id of the activation link id
     * @return Returns true if the activation link does not exist, false otherwise
     */
    private Boolean isUniqueActivationEmailId(final String id) {
        return !activationEmailRepository.existsById(id);
    }

    /**
     * Tell if the ResetPasswordId is unique
     *
     * @param id The id of the reset password link id
     * @return Returns true if the activation link does not exist, false otherwise
     */
    private Boolean isUniqueResetPasswordId(final String id) {
        return !resetPasswordRepository.existsById(id);
    }

    /**
     * Send a link to activate a new account
     *
     * @param user  The new user account
     * @param email The email on witch send the link
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendActivationEmail(final UserModel user, final String email) throws CodeMessageException {
        sendValidationEmail(user, email, "Activate your new account !", "activer-compte/");
    }

    /**
     * Send a link to validate the email update

     * @param user  The new user account
     * @param email The email on witch send the link
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendUpdateEmail(final UserModel user, final String email) throws CodeMessageException {
        sendValidationEmail(user, email, "Click to update your email account !", "validation-email/");
    }

    /**
     * Generic function to send an email to validate an email address
     *
     * @param user  The new user account
     * @param email The email on witch send the link
     * @param message The message in the mail
     * @param path The path in the address of front end
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    private void sendValidationEmail(final UserModel user, final String email, final String message, final String path) throws CodeMessageException {
        String id;
        do id = UUID.randomUUID().toString(); while (!isUniqueActivationEmailId(id));

        sendEmail(email,
                message,
                frontAddress + path + id);

        ActivationEmailModel activationEmailModel = activationEmailRepository.findByUser(user);
        if (activationEmailModel != null)
            activationEmailRepository.delete(activationEmailModel);

        activationEmailModel = new ActivationEmailModel();
        activationEmailModel.setId(id);
        activationEmailModel.setUser(user);
        activationEmailModel.setEmail(email);
        activationEmailRepository.save(activationEmailModel);
    }

    /**
     * Send a welcome message
     *
     * @param userModel The user on which send the welcome message
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendWelcomeMessage(final UserModel userModel) throws CodeMessageException {
        sendEmail(userModel.getEmail(),
                "Welcome to Learn&Run " + userModel.getPseudo() + " !",
                "Welcome " + userModel.getFirstname() + " " + userModel.getLastname() + " (" + userModel.getUserType() + ")");
    }

    /**
     * Send an email to tell the email has been correctly updated
     *
     * @param email The email that have to be notified
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendUpdateEmail(final String email) throws CodeMessageException {
        sendEmail(email,
                "Your email has been updated !",
                "Your email has been updated successfully !");
    }

    /**
     * Send an email to tell the password has been correctly updated
     *
     * @param email The email that have to be notified
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendUpdatePassword(final String email) throws CodeMessageException {
        sendEmail(email,
                "Your password has been updated !",
                "Your password has been updated successfully !");
    }

    /**
     * Send a link to reset the password
     *
     * @param userModel The user on which send the link to update the password
     * @throws CodeMessageException Throws an exception if the mail failed to be sent
     */
    public void sendResetPasswordEmail(final UserModel userModel) throws CodeMessageException {
        String id;
        do id = UUID.randomUUID().toString(); while (!isUniqueResetPasswordId(id));

        sendEmail(userModel.getEmail(),
                "Reset your password !",
                frontAddress + "mot-de-passe/" + id);

        ResetPasswordModel resetPasswordModel = resetPasswordRepository.findByUser(userModel);
        if (resetPasswordModel != null)
            resetPasswordRepository.delete(resetPasswordModel);

        resetPasswordModel = new ResetPasswordModel();
        resetPasswordModel.setId(id);
        resetPasswordModel.setUser(userModel);
        resetPasswordRepository.save(resetPasswordModel);
    }

    /**
     * Send an email to tell the account has been correctly deleted
     *
     * @param email The email on which send the delete confirmation
     * @throws CodeMessageException Throws an exception if the email failed to be sent
     */
    public void sendDeleteUser(final String email) throws CodeMessageException {
        sendEmail(email,
                "Your account has been deleted !",
                "We hope to see again soon !");
    }

}
