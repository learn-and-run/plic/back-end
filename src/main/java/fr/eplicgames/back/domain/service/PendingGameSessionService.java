package fr.eplicgames.back.domain.service;

import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.PendingGameSessionModel;
import fr.eplicgames.back.persistence.model.ScoreModel;
import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.content.RoomModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.PendingGameSessionRepository;
import fr.eplicgames.back.persistence.repository.ScoreRepository;
import fr.eplicgames.back.persistence.repository.content.CircuitRepository;
import fr.eplicgames.back.persistence.repository.content.RoomRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.utils.Score;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PendingGameSessionService {

    final PendingGameSessionRepository pendingGameSessionRepository;
    final CircuitRepository circuitRepository;
    final UserRepository userRepository;
    final ScoreRepository scoreRepository;
    final RoomRepository roomRepository;

    /**
     * Find the current user from authentication
     *
     * @return Returns the current user
     */
    private UserModel getUser() {
        final Long userId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getName());
        return userRepository.findById(userId).orElse(null);
    }

    /**
     * Create a session in database when we enter in a circuit
     *
     * @param circuitId The circuit on which we enter
     * @throws CodeMessageException Throw an exception in the circuit does not exist
     */
    public void enterCircuit(final Long circuitId) throws CodeMessageException {

        final CircuitModel circuitModel = circuitRepository.findById(circuitId).orElse(null);
        if (circuitModel == null) throw new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_NOT_EXIST);

        final UserModel currentUser = getUser();
        PendingGameSessionModel gameSessionEntity = pendingGameSessionRepository.findByUser(currentUser);

        // If the session already exist there is an problem, to prevent game crash we recreate an new session
        // on the previous
        if (gameSessionEntity == null) {
            gameSessionEntity = new PendingGameSessionModel();
            gameSessionEntity.setUser(currentUser);
        } else {
            gameSessionEntity.setEnterTime(null);
            gameSessionEntity.setRoom(null);
        }
        gameSessionEntity.setCircuit(circuitModel);
        final Timestamp enterTime = new Timestamp(System.currentTimeMillis());
        gameSessionEntity.setSpendTime(enterTime);
        pendingGameSessionRepository.save(gameSessionEntity);

        resetCurrentScore(currentUser, circuitModel);
    }

    /**
     * Reset the current score to 0.
     *
     * @param currentUser    The user on which reset the current score
     * @param currentCircuit The circuit on which the current score has to be reset
     */
    private void resetCurrentScore(final UserModel currentUser, final CircuitModel currentCircuit) {
        ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(currentUser, currentCircuit);

        if (scoreModel == null) {
            scoreModel = new ScoreModel();
            scoreModel.setId(null);
            scoreModel.setUser(currentUser);
            scoreModel.setCircuit(currentCircuit);
            scoreModel.setBestScore(0);
        }

        scoreModel.setCurrentScore(0);
        scoreRepository.save(scoreModel);
    }

    /**
     * Remove the game session from the database
     *
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public void leaveGameSession() throws CodeMessageException {
        final PendingGameSessionModel pendingGameSessionModel = getGameSession(getUser());
        final Timestamp enterTime = pendingGameSessionModel.getSpendTime();
        final long spendTimeInSeconds = (getNow().getTime() - enterTime.getTime()) / 1000;

        final UserModel currentUser = getUser();
        final CircuitModel circuitModel = getGameSession(currentUser).getCircuit();

        final ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(currentUser, circuitModel);
        scoreModel.setSpendTime(scoreModel.getSpendTime() + (int) spendTimeInSeconds);

        scoreRepository.save(scoreModel);

        pendingGameSessionRepository.delete(pendingGameSessionModel);
    }

    /**
     * Update best score if current score is higher than previous
     *
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public void updateBestScore() throws CodeMessageException {

        final UserModel currentUser = getUser();
        final CircuitModel circuitModel = getGameSession(currentUser).getCircuit();

        final ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(currentUser, circuitModel);
        final int currentScore = circuitModel.getRooms().size() != 0
                ? scoreModel.getCurrentScore() / circuitModel.getRooms().size()
                : 0;

        final Timestamp enterTime = getGameSession(currentUser).getSpendTime();
        final Integer actualSpendTime = (int) (getNow().getTime() - enterTime.getTime()) / 1000;
        if (currentScore > scoreModel.getBestScore()) {
            // Update best time by current time
            scoreModel.setBestTime(actualSpendTime);
            scoreModel.setBestScore(currentScore);

            scoreRepository.save(scoreModel);
        } else if (currentScore == scoreModel.getBestScore() && actualSpendTime < scoreModel.getBestTime()) {
            scoreModel.setBestTime(actualSpendTime);
            scoreRepository.save(scoreModel);
        }
    }

    /**
     * Update the bestTime if the current is less than previous
     *
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public void updateBestTime() throws CodeMessageException {
        final UserModel currentUser = getUser();
        final CircuitModel circuitModel = getGameSession(currentUser).getCircuit();

        final ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(currentUser, circuitModel);

        final Timestamp enterTime = getGameSession(currentUser).getSpendTime();
        final Integer actualSpendTime = (int) (getNow().getTime() - enterTime.getTime()) / 1000;

        if (scoreModel.getBestTime() == -1 || scoreModel.getBestTime() > actualSpendTime)
            scoreModel.setBestTime(actualSpendTime);
    }

    /**
     * Get the game session associated to the given user
     *
     * @param user The user of the game session
     * @return Returns the game session
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    private PendingGameSessionModel getGameSession(final UserModel user) throws CodeMessageException {

        final PendingGameSessionModel gameSessionEntity = pendingGameSessionRepository.findByUser(user);
        if (gameSessionEntity == null) throw new CodeMessageException(CodeMessageExceptionEnum.GAME_SESSION_NOT_EXIST);
        return gameSessionEntity;
    }

    /**
     * Permit to enter in a room and fix the time
     *
     * @param roomId The id of the room we enter
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public void enterRoom(final Long roomId) throws CodeMessageException {
        final PendingGameSessionModel gameSessionEntity = getGameSession(getUser());

        final RoomModel roomModel = roomRepository.findById(roomId).orElse(null);
        if (roomModel == null) throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_NOT_EXIST);

        gameSessionEntity.setRoom(roomModel);
        gameSessionEntity.setEnterTime(getNow());

        pendingGameSessionRepository.save(gameSessionEntity);
    }

    /**
     * Leave the current room and calculate the new current score
     *
     * @param success Boolean that determine if the room is succeed or not
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public void leaveRoomAndCalculateScore(final boolean success) throws CodeMessageException {
        final UserModel currentUser = getUser();
        final PendingGameSessionModel gameSessionEntity = getGameSession(currentUser);

        if (success) {

            final long now = getNow().getTime();
            final long spendTime = now - gameSessionEntity.getEnterTime().getTime();

            final RoomModel room = gameSessionEntity.getRoom();

            final CircuitModel circuit = gameSessionEntity.getCircuit();
            final Integer score = Score.calculateScore((int) spendTime, room.getExpectedTime());

            final ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(currentUser, circuit);
            scoreModel.setCurrentScore(scoreModel.getCurrentScore() + score);

            scoreRepository.save(scoreModel);
        }

        gameSessionEntity.setRoom(null);
        gameSessionEntity.setEnterTime(null);
        pendingGameSessionRepository.save(gameSessionEntity);

    }

    private Timestamp getNow() {
        return new Timestamp(System.currentTimeMillis());
    }
}
