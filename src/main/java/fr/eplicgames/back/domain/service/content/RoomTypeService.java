package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.RoomTypeEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.RoomTypeModel;
import fr.eplicgames.back.persistence.repository.content.RoomTypeRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomTypeService {

    final RoomTypeRepository roomTypeRepository;

    /**
     * Create an roomType in database
     *
     * @param roomTypeEntity The new roomType to create
     * @return Return the created roomType
     * @throws CodeMessageException Throws an exception if the roomType already exists
     */
    public RoomTypeEntity createRoomType(final RoomTypeEntity roomTypeEntity) throws CodeMessageException {
        final RoomTypeModel roomTypeModel = new RoomTypeModel(roomTypeEntity.type);
        return saveRoomType(roomTypeModel);
    }

    /**
     * Update an roomType from database
     *
     * @param roomTypeEntity The updated roomType entity
     * @return Return the updated roomType
     * @throws CodeMessageException Throws an exception if the new type is already taken or if the uuid does not exist
     */
    public RoomTypeEntity updateRoomType(final RoomTypeEntity roomTypeEntity) throws CodeMessageException {

        if (!roomTypeRepository.existsById(roomTypeEntity.uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_TYPE_NOT_EXIST);

        final RoomTypeModel roomTypeModel = new RoomTypeModel(roomTypeEntity.uuid, roomTypeEntity.type);

        return saveRoomType(roomTypeModel);
    }

    /**
     * Save the roomType in database
     *
     * @param roomTypeModel The roomType model to save
     * @return Returns the saved roomType
     * @throws CodeMessageException Throws an exception if the type is already taken
     */
    private RoomTypeEntity saveRoomType(final RoomTypeModel roomTypeModel) throws CodeMessageException {
        if (roomTypeRepository.existsByType(roomTypeModel.getType()))
            throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_TYPE_ALREADY_EXIST);

        final RoomTypeModel newRoomTypeModel = roomTypeRepository.save(roomTypeModel);

        return new RoomTypeEntity(newRoomTypeModel.getUuid(), newRoomTypeModel.getType());
    }

    /**
     * Delete an roomType with its uuid
     *
     * @param uuid The uuid of the roomType to delete
     * @throws CodeMessageException Throws an exception if the uuid does not exist
     */
    public void deleteRoomTypeByUuid(final UUID uuid) throws CodeMessageException {
        if (!roomTypeRepository.existsById(uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.ROOM_TYPE_NOT_EXIST);

        roomTypeRepository.deleteById(uuid);
    }

    /**
     * Get all roomTypes in database
     *
     * @return Returns roomTypes in database
     */
    public List<RoomTypeEntity> getRoomTypes() {
        return roomTypeRepository.findAll().stream().map(roomTypeModel -> new RoomTypeEntity(
                roomTypeModel.getUuid(),
                roomTypeModel.getType()
        )).collect(Collectors.toList());
    }
}
