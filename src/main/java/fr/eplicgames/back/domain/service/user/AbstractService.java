package fr.eplicgames.back.domain.service.user;

import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;

@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class AbstractService {

    final UserRepository userRepository;
    final ModelMapper modelMapper;

    public AbstractService(final UserRepository userRepository, final ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    protected Long getCurrentUserId() {
        return Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    /**
     * Give the current connected user
     *
     * @return Returns the user model associated to the current user
     */
    protected UserModel getCurrentUser() {
        final Long id = getCurrentUserId();
        return userRepository.findById(id).orElse(null);
    }

    /**
     * Tell if the pseudo is available
     *
     * @param pseudo The pseudo to check
     * @return Returns true if the pseudo is available, false otherwise
     */
    protected Boolean isPseudoAvailable(final String pseudo) {
        return userRepository.findByPseudo(pseudo) == null;
    }

    /**
     * Tell if the email is available
     *
     * @param email The email to check
     * @return Returns true if the email is available, false otherwise
     */
    protected Boolean isEmailAvailable(final String email) {
        return userRepository.findByEmail(email) == null;
    }

}
