package fr.eplicgames.back.domain.service.user;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.domain.service.ScoreService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.ActivationEmailModel;
import fr.eplicgames.back.persistence.model.content.CharacterModel;
import fr.eplicgames.back.persistence.model.user.ParentModel;
import fr.eplicgames.back.persistence.model.user.ProfessorModel;
import fr.eplicgames.back.persistence.model.user.StudentModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.ActivationEmailRepository;
import fr.eplicgames.back.persistence.repository.content.CharacterRepository;
import fr.eplicgames.back.persistence.repository.user.ParentRepository;
import fr.eplicgames.back.persistence.repository.user.ProfessorRepository;
import fr.eplicgames.back.persistence.repository.user.StudentRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateService extends AbstractService {

    final StudentRepository studentRepository;
    final ParentRepository parentRepository;
    final ProfessorRepository professorRepository;
    final CharacterRepository characterRepository;
    final ActivationEmailRepository activationEmailRepository;
    final BCryptPasswordEncoder bCryptPasswordEncoder;
    final EmailService emailService;

    final ScoreService scoreService;

    public CreateService(final UserRepository userRepository,
                         final ModelMapper modelMapper,
                         final StudentRepository studentRepository,
                         final ParentRepository parentRepository,
                         final ProfessorRepository professorRepository,
                         final CharacterRepository characterRepository,
                         final ActivationEmailRepository activationEmailRepository,
                         final BCryptPasswordEncoder bCryptPasswordEncoder,
                         final EmailService emailService,
                         final ScoreService scoreService) {
        super(userRepository, modelMapper);

        this.studentRepository = studentRepository;
        this.parentRepository = parentRepository;
        this.professorRepository = professorRepository;
        this.characterRepository = characterRepository;
        this.activationEmailRepository = activationEmailRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.emailService = emailService;
        this.scoreService = scoreService;
    }

    /**
     * Create a new student (inactive)
     *
     * @param studentEntity Student entity to create with infos
     * @return Returns the created student entity
     * @throws CodeMessageException Throws an exception if the student cannot be created
     */
    public StudentEntity createStudent(final StudentEntity studentEntity) throws CodeMessageException {
        final UserModel userModel = createUser(studentEntity, UserType.STUDENT);

        final StudentModel studentModel = new StudentModel();
        studentModel.setUserModel(userModel);
        studentModel.setLevelType(studentEntity.levelType);
        final StudentModel createdStudentModel = studentRepository.save(studentModel);

        return new StudentEntity(
                createdStudentModel.getUserModel().getId(),
                createdStudentModel.getUserModel().getPseudo(),
                createdStudentModel.getUserModel().getFirstname(),
                createdStudentModel.getUserModel().getLastname(),
                createdStudentModel.getUserModel().getEmail(),
                createdStudentModel.getUserModel().getPassword(),
                new CharacterEntity(
                        createdStudentModel.getUserModel().getCharacter().getId(),
                        createdStudentModel.getUserModel().getCharacter().getName(),
                        createdStudentModel.getUserModel().getCharacter().getDescription(),
                        createdStudentModel.getUserModel().getCharacter().getBackground(),
                        createdStudentModel.getUserModel().getCharacter().getInGame(),
                        createdStudentModel.getUserModel().getCharacter().getSorted(),
                        createdStudentModel.getUserModel().getCharacter().getSmallImage(),
                        createdStudentModel.getUserModel().getCharacter().getBigImage()
                ),
                createdStudentModel.getUserModel().getActive(),
                createdStudentModel.getLevelType()
        );
    }

    /**
     * Create a new parent (inactive)
     *
     * @param parentEntity Parent entity to create with infos
     * @return Returns the created parent entity
     * @throws CodeMessageException Throws an exception if the parent cannot be created
     */
    public ParentEntity createParent(final ParentEntity parentEntity) throws CodeMessageException {
        final UserModel userModel = createUser(parentEntity, UserType.PARENT);

        final ParentModel parentModel = new ParentModel();
        parentModel.setUserModel(userModel);
        final ParentModel createdParentModel = parentRepository.save(parentModel);

        return new ParentEntity(
                createdParentModel.getUserModel().getId(),
                createdParentModel.getUserModel().getPseudo(),
                createdParentModel.getUserModel().getFirstname(),
                createdParentModel.getUserModel().getLastname(),
                createdParentModel.getUserModel().getEmail(),
                createdParentModel.getUserModel().getPassword(),
                new CharacterEntity(
                        createdParentModel.getUserModel().getCharacter().getId(),
                        createdParentModel.getUserModel().getCharacter().getName(),
                        createdParentModel.getUserModel().getCharacter().getDescription(),
                        createdParentModel.getUserModel().getCharacter().getBackground(),
                        createdParentModel.getUserModel().getCharacter().getInGame(),
                        createdParentModel.getUserModel().getCharacter().getSorted(),
                        createdParentModel.getUserModel().getCharacter().getSmallImage(),
                        createdParentModel.getUserModel().getCharacter().getBigImage()
                ),
                createdParentModel.getUserModel().getActive()
        );
    }

    /**
     * Create a new professor (inactive)
     *
     * @param professorEntity Professor entity to create with infos
     * @return Returns the created professor entity
     * @throws CodeMessageException Throws an exception if the professor cannot be created
     */
    public ProfessorEntity createProfessor(final ProfessorEntity professorEntity) throws CodeMessageException {
        final UserModel userModel = createUser(professorEntity, UserType.PROFESSOR);

        final ProfessorModel professorModel = new ProfessorModel();
        professorModel.setUserModel(userModel);
        final ProfessorModel createdProfessorModel = professorRepository.save(professorModel);

        return new ProfessorEntity(
                createdProfessorModel.getUserModel().getId(),
                createdProfessorModel.getUserModel().getPseudo(),
                createdProfessorModel.getUserModel().getFirstname(),
                createdProfessorModel.getUserModel().getLastname(),
                createdProfessorModel.getUserModel().getEmail(),
                createdProfessorModel.getUserModel().getPassword(),
                new CharacterEntity(
                        createdProfessorModel.getUserModel().getCharacter().getId(),
                        createdProfessorModel.getUserModel().getCharacter().getName(),
                        createdProfessorModel.getUserModel().getCharacter().getDescription(),
                        createdProfessorModel.getUserModel().getCharacter().getBackground(),
                        createdProfessorModel.getUserModel().getCharacter().getInGame(),
                        createdProfessorModel.getUserModel().getCharacter().getSorted(),
                        createdProfessorModel.getUserModel().getCharacter().getSmallImage(),
                        createdProfessorModel.getUserModel().getCharacter().getBigImage()
                ),
                createdProfessorModel.getUserModel().getActive()
        );
    }

    /**
     * Create a new user (inactive)
     *
     * @param userEntity The user to create
     * @param userType   The type of the user to create
     * @throws CodeMessageException Throws an exception if the user cannot be created
     */
    private <T extends UserEntity> UserModel createUser(final T userEntity, final UserType userType) throws CodeMessageException {
        if (!isPseudoAvailable(userEntity.pseudo))
            throw new CodeMessageException(CodeMessageExceptionEnum.PSEUDO_ALREADY_TAKEN);
        if (!isEmailAvailable(userEntity.email))
            throw new CodeMessageException(CodeMessageExceptionEnum.EMAIL_ALREADY_USED);

        userEntity.password = bCryptPasswordEncoder.encode(userEntity.password);

        final UserModel userModel = new UserModel();
        userModel.setPseudo(userEntity.pseudo);
        userModel.setFirstname(userEntity.firstname);
        userModel.setLastname(userEntity.lastname);
        userModel.setEmail(userEntity.email);
        userModel.setPassword(userEntity.password);

        userModel.setActive(false);
        userModel.setEmail(null);
        userModel.setUserType(userType);

        final CharacterModel defaultCharacterModel = characterRepository.findByName("Jack").orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CHARACTER_NOT_EXIST)
        );
        userModel.setCharacter(defaultCharacterModel);
        final UserModel createdUserModel = userRepository.save(userModel);

        try {
            emailService.sendActivationEmail(userModel, userEntity.email);
        } catch (final CodeMessageException e) {
            userRepository.delete(userModel);
            throw e;
        }

        return createdUserModel;
    }

    /**
     * Activate a new user
     *
     * @param id The id that correspond to the user to activate (be careful, it is not the user_id)
     * @throws CodeMessageException Throws an exception if the user cannot be activated
     */
    public void activateUser(final String id) throws CodeMessageException {
        final ActivationEmailModel activationEmailModel = activationEmailRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.WRONG_ACTIVATION_LINK)
        );

        final UserModel userModel = activationEmailModel.getUser();
        if (userModel.getActive())
            throw new CodeMessageException(CodeMessageExceptionEnum.ILLEGAL_ACCESS);

        final String tmpEmail = activationEmailModel.getEmail();
        activationEmailRepository.delete(activationEmailModel);

        if (!isEmailAvailable(tmpEmail))
            throw new CodeMessageException(CodeMessageExceptionEnum.EMAIL_ALREADY_USED);

        userModel.setActive(true);
        userModel.setEmail(tmpEmail);
        userRepository.save(userModel);

        emailService.sendWelcomeMessage(userModel);

        // Creates scores if it is a student
        if (studentRepository.findById(userModel.getId()).isPresent()) scoreService.createScores(userModel.getPseudo());
    }
}
