package fr.eplicgames.back.domain.service;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.domain.entity.relation.InvitationEntity;
import fr.eplicgames.back.domain.entity.relation.RelationEntity;
import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.domain.service.user.AbstractService;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.relation.InvitationModel;
import fr.eplicgames.back.persistence.model.relation.RelationModel;
import fr.eplicgames.back.persistence.model.user.*;
import fr.eplicgames.back.persistence.repository.relation.InvitationRepository;
import fr.eplicgames.back.persistence.repository.relation.RelationRepository;
import fr.eplicgames.back.persistence.repository.user.ParentRepository;
import fr.eplicgames.back.persistence.repository.user.ProfessorRepository;
import fr.eplicgames.back.persistence.repository.user.StudentRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service to handle user relations
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RelationService extends AbstractService {

    final RelationRepository relationRepository;
    final InvitationRepository invitationRepository;
    final StudentRepository studentRepository;
    final ParentRepository parentRepository;
    final ProfessorRepository professorRepository;

    public RelationService(final UserRepository userRepository,
                           final ModelMapper modelMapper,
                           final RelationRepository relationRepository,
                           final InvitationRepository invitationRepository,
                           final StudentRepository studentRepository,
                           final ParentRepository parentRepository,
                           final ProfessorRepository professorRepository) {
        super(userRepository, modelMapper);

        this.relationRepository = relationRepository;
        this.invitationRepository = invitationRepository;
        this.studentRepository = studentRepository;
        this.parentRepository = parentRepository;
        this.professorRepository = professorRepository;
    }

    public void sendInvitation(final String username) throws CodeMessageException {
        final UserModel relationUserModel = userRepository.findByPseudoOrEmail(username);
        if (relationUserModel == null)
            throw new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_USER);

        final UserModel currentUser = getCurrentUser();

        if (relationUserModel.getId().equals(currentUser.getId()))
            throw new CodeMessageException(CodeMessageExceptionEnum.INVITATION_TO_YOURSELF);

        if (relationRepository.findByUserModelAndRelationUserModel(currentUser, relationUserModel) != null)
            throw new CodeMessageException(CodeMessageExceptionEnum.RELATION_ALREADY_EXIST);

        if (invitationRepository.findByUserModelAndRelationUserModel(currentUser, relationUserModel) != null)
            throw new CodeMessageException(CodeMessageExceptionEnum.INVITATION_ALREADY_SEND);

        final Long id = invitationRepository.findByUserModelAndRelationUserModel(relationUserModel, currentUser);
        if (id != null) {
            acceptInvitation(id);
            return;
        }

        final InvitationModel invitationModel = new InvitationModel();
        invitationModel.setUserModel(currentUser);
        invitationModel.setRelationUserModel(relationUserModel);
        invitationRepository.save(invitationModel);
    }

    public List<InvitationEntity> getPendingInvitations() {
        return invitationRepository.findAllByRelationUserModel(getCurrentUser())
                .stream().map(invitationModel -> {
                    final UserModel userModel = invitationModel.getUserModel();
                    return new InvitationEntity(
                            invitationModel.getId(),
                            getCompleteUserEntityFromModel(userModel)
                    );
                }).collect(Collectors.toList());
    }

    public void acceptInvitation(final Long id) throws CodeMessageException {
        final InvitationModel invitationModel = removeInvitation(id);
        final UserModel currentUser = getCurrentUser();

        final RelationModel relationModel1 = new RelationModel();
        final RelationModel relationModel2 = new RelationModel();

        relationModel1.setUserModel(currentUser);
        relationModel2.setUserModel(invitationModel.getUserModel());
        relationModel1.setRelationUserModel(invitationModel.getUserModel());
        relationModel2.setRelationUserModel(currentUser);

        relationRepository.save(relationModel1);
        relationRepository.save(relationModel2);
    }

    public InvitationModel removeInvitation(final Long id) throws CodeMessageException {
        final InvitationModel invitationModel = invitationRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_INVITATION)
        );

        final UserModel currentUser = getCurrentUser();
        if (!invitationModel.getRelationUserModel().getId().equals(currentUser.getId()))
            throw new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_INVITATION);

        invitationRepository.deleteInvitations(invitationModel.getUserModel(), currentUser);

        return invitationModel;
    }

    public List<RelationEntity> getRelations(final UserType userType) {
        final List<RelationModel> relations = userType != null
                ? relationRepository.findAllByUserModelAndByType(getCurrentUser(), userType)
                : relationRepository.findAllByUserModel(getCurrentUser());

        return relations.stream().map(relationModel -> {
            final UserModel userModel = relationModel.getRelationUserModel();
            return new RelationEntity(
                    relationModel.getId(),
                    getCompleteUserEntityFromModel(userModel)
            );
        }).collect(Collectors.toList());
    }

    public void removeRelation(final Long id) throws CodeMessageException {
        final RelationModel relationModel = relationRepository.findById(id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_RELATION)
        );

        final UserModel currentUser = getCurrentUser();
        if (!relationModel.getUserModel().getId().equals(getCurrentUser().getId()))
            throw new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_RELATION);

        relationRepository.deleteRelations(currentUser, relationModel.getRelationUserModel());
    }

    private UserEntity getCompleteUserEntityFromModel(UserModel userModel) {
        UserEntity userEntity;
        switch (userModel.getUserType()) {
            case STUDENT:
                StudentModel studentModel = studentRepository.findByUserModel(userModel);
                userEntity = new StudentEntity(
                        userModel.getId(),
                        userModel.getPseudo(),
                        userModel.getFirstname(),
                        userModel.getLastname(),
                        userModel.getEmail(),
                        null,
                        new CharacterEntity(
                                userModel.getCharacter().getId(),
                                userModel.getCharacter().getName(),
                                userModel.getCharacter().getDescription(),
                                userModel.getCharacter().getBackground(),
                                userModel.getCharacter().getInGame(),
                                userModel.getCharacter().getSorted(),
                                userModel.getCharacter().getSmallImage(),
                                userModel.getCharacter().getBigImage()
                        ),
                        userModel.getActive(),
                        studentModel.getLevelType()
                );
                break;
            case PARENT:
                ParentModel parentModel = parentRepository.findByUserModel(userModel);
                userEntity = new ParentEntity(
                        userModel.getId(),
                        userModel.getPseudo(),
                        userModel.getFirstname(),
                        userModel.getLastname(),
                        userModel.getEmail(),
                        null,
                        new CharacterEntity(
                                userModel.getCharacter().getId(),
                                userModel.getCharacter().getName(),
                                userModel.getCharacter().getDescription(),
                                userModel.getCharacter().getBackground(),
                                userModel.getCharacter().getInGame(),
                                userModel.getCharacter().getSorted(),
                                userModel.getCharacter().getSmallImage(),
                                userModel.getCharacter().getBigImage()
                        ),
                        userModel.getActive()
                );
                break;
            case PROFESSOR:
                ProfessorModel professorModel = professorRepository.findByUserModel(userModel);
                userEntity = new ProfessorEntity(
                        userModel.getId(),
                        userModel.getPseudo(),
                        userModel.getFirstname(),
                        userModel.getLastname(),
                        userModel.getEmail(),
                        null,
                        new CharacterEntity(
                                userModel.getCharacter().getId(),
                                userModel.getCharacter().getName(),
                                userModel.getCharacter().getDescription(),
                                userModel.getCharacter().getBackground(),
                                userModel.getCharacter().getInGame(),
                                userModel.getCharacter().getSorted(),
                                userModel.getCharacter().getSmallImage(),
                                userModel.getCharacter().getBigImage()
                        ),
                        userModel.getActive()
                );
                break;
            default:
                userEntity = null;
        }

        return userEntity;
    }
}
