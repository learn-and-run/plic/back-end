package fr.eplicgames.back.domain.service;

import fr.eplicgames.back.domain.entity.GamePreferenceEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.GamePreferenceModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.GamePreferenceRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Service for game preferences
 */
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GamePreferenceService {
    final GamePreferenceRepository gamePreferenceRepository;
    final UserRepository userRepository;

    /**
     * Get preferences in game of the current user
     *
     * @return Returns preferences of the current user
     * @throws CodeMessageException Throws exception if user is unknown
     */
    public GamePreferenceEntity getGamePreferences() throws CodeMessageException {
        final GamePreferenceModel gamePreferenceModel = getCurrentGamePreferencesModel();

        return new GamePreferenceEntity(
                gamePreferenceModel.getId(),
                gamePreferenceModel.getUser(),
                gamePreferenceModel.getControllerDualShock(),
                gamePreferenceModel.getRetro(),
                gamePreferenceModel.getIdentity()
        );
    }

    /**
     * Update game preferences of the current user
     *
     * @param newGamePreferenceEntity The ew game preferences entity with new preferences
     * @return Returns the new preferences of the user
     * @throws CodeMessageException Throws exception if user is unknown
     */
    public GamePreferenceEntity updateGamePreferences(final GamePreferenceEntity newGamePreferenceEntity)
            throws CodeMessageException {
        final GamePreferenceModel gamePreferenceModel = getCurrentGamePreferencesModel();

        gamePreferenceModel.setControllerDualShock(newGamePreferenceEntity.controllerDualShock);
        gamePreferenceModel.setRetro(newGamePreferenceEntity.retro);
        gamePreferenceModel.setIdentity(newGamePreferenceEntity.identity);

        final GamePreferenceModel newGamePreferenceModel = gamePreferenceRepository.save(gamePreferenceModel);

        return new GamePreferenceEntity(
                newGamePreferenceModel.getId(),
                newGamePreferenceModel.getUser(),
                newGamePreferenceModel.getControllerDualShock(),
                newGamePreferenceModel.getRetro(),
                newGamePreferenceModel.getIdentity()
        );
    }

    /**
     * Get the current game preferences model of the user
     *
     * @return Returns the preferences of the current user
     * @throws CodeMessageException Throws an exception if the user is unknown
     */
    private GamePreferenceModel getCurrentGamePreferencesModel() throws CodeMessageException {
        final long userId = Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getName());
        final UserModel currentUser = userRepository.findById(userId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_USER)
        );

        return gamePreferenceRepository.findByUser_Id(userId).orElseGet(() -> {
            final GamePreferenceModel gamePreferences = new GamePreferenceModel();
            gamePreferences.setUser(currentUser);
            return gamePreferenceRepository.save(gamePreferences);
        });
    }
}
