package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.QuestionEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.QuestionModel;
import fr.eplicgames.back.persistence.repository.content.QuestionRepository;
import fr.eplicgames.back.security.annotation.IsAdmin;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class QuestionService {

    final QuestionRepository questionRepository;

    public List<QuestionEntity> getAllQuestionByModule(final ModuleType module) {
        final List<QuestionModel> questionModels;

        if (module.equals(ModuleType.TOUS)) questionModels = questionRepository.findAll();
        else questionModels = questionRepository.findAllByModule(module);

        return questionModels.stream().map(questionModel ->
                new QuestionEntity(
                        questionModel.getId(),
                        questionModel.getName(),
                        questionModel.getQuestion(),
                        questionModel.getResponse(),
                        questionModel.getBadAnswer(),
                        questionModel.getExplanation(),
                        questionModel.getClue1(),
                        questionModel.getClue2(),
                        questionModel.getClue3(),
                        questionModel.getClue4(),
                        questionModel.getModule(),
                        questionModel.getLevel(),
                        questionModel.getDifficulty()
                )
        ).collect(Collectors.toList());
    }

    /**
     * Create a question in database
     *
     * @param questionEntity The question to add
     * @throws CodeMessageException Throws an exception if the question already exists
     */
    @IsAdmin
    public QuestionEntity createQuestionIfNotExists(final QuestionEntity questionEntity) throws CodeMessageException {

        if (questionRepository.existsByName(questionEntity.name))
            throw new CodeMessageException(CodeMessageExceptionEnum.QUESTION_ALREADY_EXIST);

        final QuestionModel questionModel = new QuestionModel();
        final QuestionModel newQuestionModel = updateQuestionModel(questionModel, questionEntity);
        return new QuestionEntity(
                newQuestionModel.getId(),
                newQuestionModel.getName(),
                newQuestionModel.getQuestion(),
                newQuestionModel.getResponse(),
                newQuestionModel.getBadAnswer(),
                newQuestionModel.getExplanation(),
                newQuestionModel.getClue1(),
                newQuestionModel.getClue2(),
                newQuestionModel.getClue3(),
                newQuestionModel.getClue4(),
                newQuestionModel.getModule(),
                newQuestionModel.getLevel(),
                newQuestionModel.getDifficulty()
        );
    }

    /**
     * Update a question
     *
     * @param questionEntity The new content of question to update
     * @return Returns the created question
     * @throws CodeMessageException Throws an exception if the question does not exists
     */
    @IsAdmin
    public QuestionEntity updateQuestion(final QuestionEntity questionEntity) throws CodeMessageException {

        final QuestionModel questionModel = questionRepository.findById(questionEntity.id).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.QUESTION_NOT_EXIST)
        );

        final QuestionModel newQuestionModel = updateQuestionModel(questionModel, questionEntity);
        return new QuestionEntity(
                newQuestionModel.getId(),
                newQuestionModel.getName(),
                newQuestionModel.getQuestion(),
                newQuestionModel.getResponse(),
                newQuestionModel.getBadAnswer(),
                newQuestionModel.getExplanation(),
                newQuestionModel.getClue1(),
                newQuestionModel.getClue2(),
                newQuestionModel.getClue3(),
                newQuestionModel.getClue4(),
                newQuestionModel.getModule(),
                newQuestionModel.getLevel(),
                newQuestionModel.getDifficulty()
        );
    }

    /**
     * Delete a question from the database
     *
     * @param id The question to delete
     * @throws CodeMessageException Throws an exception if the question does not exists
     */
    @IsAdmin
    public void deleteQuestion(final Long id) throws CodeMessageException {

        if (!questionRepository.existsById(id))
            throw new CodeMessageException(CodeMessageExceptionEnum.QUESTION_NOT_EXIST);
        questionRepository.deleteById(id);
    }

    /**
     * Update a question entity in the database
     *
     * @param questionModel  The previous question entity
     * @param questionEntity The new question
     */
    @IsAdmin
    private QuestionModel updateQuestionModel(final QuestionModel questionModel, final QuestionEntity questionEntity) {

        questionModel.setName(questionEntity.name);
        questionModel.setQuestion(questionEntity.question);
        questionModel.setResponse(questionEntity.response);
        questionModel.setBadAnswer(questionEntity.badAnswer);
        questionModel.setExplanation(questionEntity.explanation);
        questionModel.setClue1(questionEntity.clue1);
        questionModel.setClue2(questionEntity.clue2);
        questionModel.setClue3(questionEntity.clue3);
        questionModel.setClue4(questionEntity.clue4);
        questionModel.setModule(questionEntity.module);
        questionModel.setLevel(questionEntity.level);
        questionModel.setDifficulty(questionEntity.difficulty);

        return questionRepository.save(questionModel);
    }
}
