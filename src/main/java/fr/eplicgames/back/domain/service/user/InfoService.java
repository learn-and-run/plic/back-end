package fr.eplicgames.back.domain.service.user;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.domain.entity.user.ParentEntity;
import fr.eplicgames.back.domain.entity.user.ProfessorEntity;
import fr.eplicgames.back.domain.entity.user.StudentEntity;
import fr.eplicgames.back.domain.entity.user.UserEntity;
import fr.eplicgames.back.persistence.model.user.ParentModel;
import fr.eplicgames.back.persistence.model.user.ProfessorModel;
import fr.eplicgames.back.persistence.model.user.StudentModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.user.ParentRepository;
import fr.eplicgames.back.persistence.repository.user.ProfessorRepository;
import fr.eplicgames.back.persistence.repository.user.StudentRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InfoService extends AbstractService {

    final StudentRepository studentRepository;
    final ParentRepository parentRepository;
    final ProfessorRepository professorRepository;

    public InfoService(final UserRepository userRepository,
                       final ModelMapper modelMapper,
                       final StudentRepository studentRepository,
                       final ParentRepository parentRepository,
                       final ProfessorRepository professorRepository) {
        super(userRepository, modelMapper);

        this.studentRepository = studentRepository;
        this.parentRepository = parentRepository;
        this.professorRepository = professorRepository;
    }

    /**
     * Give the current user info and its type
     *
     * @return Returns the current userEntity with its infos
     */
    public UserEntity getCurrentUserInfo() {
        final UserModel userModel = getCurrentUser();

        if (userModel != null) switch (userModel.getUserType()) {
            case STUDENT:
                final StudentModel studentModel = studentRepository.findByUserModel(userModel);
                return new StudentEntity(
                        studentModel.getUserModel().getId(),
                        studentModel.getUserModel().getPseudo(),
                        studentModel.getUserModel().getFirstname(),
                        studentModel.getUserModel().getLastname(),
                        studentModel.getUserModel().getEmail(),
                        studentModel.getUserModel().getPassword(),
                        new CharacterEntity(
                                studentModel.getUserModel().getCharacter().getId(),
                                studentModel.getUserModel().getCharacter().getName(),
                                studentModel.getUserModel().getCharacter().getDescription(),
                                studentModel.getUserModel().getCharacter().getBackground(),
                                studentModel.getUserModel().getCharacter().getInGame(),
                                studentModel.getUserModel().getCharacter().getSorted(),
                                studentModel.getUserModel().getCharacter().getSmallImage(),
                                studentModel.getUserModel().getCharacter().getBigImage()
                        ),
                        studentModel.getUserModel().getActive(),
                        studentModel.getLevelType()
                );
            case PARENT:
                final ParentModel parentModel = parentRepository.findByUserModel(userModel);
                return new ParentEntity(
                        parentModel.getUserModel().getId(),
                        parentModel.getUserModel().getPseudo(),
                        parentModel.getUserModel().getFirstname(),
                        parentModel.getUserModel().getLastname(),
                        parentModel.getUserModel().getEmail(),
                        parentModel.getUserModel().getPassword(),
                        new CharacterEntity(
                                parentModel.getUserModel().getCharacter().getId(),
                                parentModel.getUserModel().getCharacter().getName(),
                                parentModel.getUserModel().getCharacter().getDescription(),
                                parentModel.getUserModel().getCharacter().getBackground(),
                                parentModel.getUserModel().getCharacter().getInGame(),
                                parentModel.getUserModel().getCharacter().getSorted(),
                                parentModel.getUserModel().getCharacter().getSmallImage(),
                                parentModel.getUserModel().getCharacter().getBigImage()
                        ),
                        parentModel.getUserModel().getActive()
                );
            case PROFESSOR:
                final ProfessorModel professorModel = professorRepository.findByUserModel(userModel);
                return new ProfessorEntity(
                        professorModel.getUserModel().getId(),
                        professorModel.getUserModel().getPseudo(),
                        professorModel.getUserModel().getFirstname(),
                        professorModel.getUserModel().getLastname(),
                        professorModel.getUserModel().getEmail(),
                        professorModel.getUserModel().getPassword(),
                        new CharacterEntity(
                                professorModel.getUserModel().getCharacter().getId(),
                                professorModel.getUserModel().getCharacter().getName(),
                                professorModel.getUserModel().getCharacter().getDescription(),
                                professorModel.getUserModel().getCharacter().getBackground(),
                                professorModel.getUserModel().getCharacter().getInGame(),
                                professorModel.getUserModel().getCharacter().getSorted(),
                                professorModel.getUserModel().getCharacter().getSmallImage(),
                                professorModel.getUserModel().getCharacter().getBigImage()
                        ),
                        professorModel.getUserModel().getActive()
                );
            default:
                return null;
        }

        return null;
    }

}
