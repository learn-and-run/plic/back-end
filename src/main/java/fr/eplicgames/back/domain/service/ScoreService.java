package fr.eplicgames.back.domain.service;

import fr.eplicgames.back.domain.entity.FriendScoreEntity;
import fr.eplicgames.back.domain.entity.ScoreEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.ScoreModel;
import fr.eplicgames.back.persistence.model.content.CircuitModel;
import fr.eplicgames.back.persistence.model.relation.RelationModel;
import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.persistence.repository.ScoreRepository;
import fr.eplicgames.back.persistence.repository.content.CircuitRepository;
import fr.eplicgames.back.persistence.repository.relation.RelationRepository;
import fr.eplicgames.back.persistence.repository.user.StudentRepository;
import fr.eplicgames.back.persistence.repository.user.UserRepository;
import fr.eplicgames.back.sql_type.ModuleType;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ScoreService {

    final ScoreRepository scoreRepository;
    final UserRepository userRepository;
    final CircuitRepository circuitRepository;
    final RelationRepository relationRepository;
    final StudentRepository studentRepository;

    /**
     * Get scores associated to an user on the specific circuit
     *
     * @param pseudo      The pseudo of the user to find score
     * @param circuitName The circuitName on which find scores
     * @return Return a object with score for the front
     */
    public ScoreEntity getScoresOfPseudoOnCircuit(final String pseudo, final String circuitName)
            throws CodeMessageException {
        final UserModel user = userRepository.findByPseudo(pseudo);

        final CircuitModel circuit = circuitRepository.findByName(circuitName).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.CIRCUIT_NOT_EXIST)
        );

        final ScoreModel scoreModel = scoreRepository.findByUserAndCircuit(user, circuit);

        final ScoreEntity scoreEntity = new ScoreEntity(
                scoreModel.getUser().getPseudo(),
                scoreModel.getCircuit(),
                scoreModel.getBestScore(),
                scoreModel.getCurrentScore(),
                scoreModel.getSpendTime(),
                scoreModel.getBestTime()
        );

        // current score is too high in database because it is the sum of all room score
        scoreEntity.currentScore = circuit.getRooms().size() != 0
                ? scoreEntity.currentScore / circuit.getRooms().size()
                : 0;

        return scoreEntity;
    }

    /**
     * Create a score Entity and add it to the database
     *
     * @param user    The user associated to the score
     * @param circuit The circuit on which look the score
     */
    private void createScore(final UserModel user, final CircuitModel circuit) {

        final ScoreModel newScore = new ScoreModel();
        newScore.setUser(user);
        newScore.setCircuit(circuit);
        newScore.setBestScore(0);
        newScore.setCurrentScore(0);
        newScore.setSpendTime(0);
        newScore.setBestTime(-1);

        scoreRepository.save(newScore);
    }

    /**
     * Create scores for the given user
     *
     * @param pseudo The pseudo of the user to create scores
     * @throws CodeMessageException Throws an exception if the user does not exist
     */
    public void createScores(final String pseudo) throws CodeMessageException {

        final UserModel userModel = userRepository.findByPseudo(pseudo);
        if (userModel == null)
            throw new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_USER);

        circuitRepository.findAll().forEach(circuitModel -> createScore(userModel, circuitModel));
    }

    /**
     * Add scores on the given circuit to all existing students.
     *
     * @param circuitModel The circuit model associated to the new circuit
     */
    public void AddScoresToStudents(final CircuitModel circuitModel) {
        studentRepository.findAll().forEach(studentModel -> createScore(studentModel.getUserModel(), circuitModel));
    }

    /**
     * Get the list of scores of a user with its pseudo and the wanted module
     *
     * @param pseudo     The pseudo on which search scores
     * @param moduleType The module on which search scores
     * @return Returns the list of Score Entities of the wanted user on the wanted module
     * @throws CodeMessageException Throws an exception if something goes wrong
     */
    public List<ScoreEntity> getScoreByPseudoAndModule(final String pseudo, final ModuleType moduleType)
            throws CodeMessageException {
        final UserModel userModel = userRepository.findByPseudo(pseudo);
        if (userModel == null)
            throw new CodeMessageException(CodeMessageExceptionEnum.UNKNOWN_USER);


        final List<ScoreModel> scoreModels = scoreRepository.findByUserAndModule(userModel, moduleType);

        return scoreModels.stream().map(scoreModel ->
                new ScoreEntity(
                        scoreModel.getUser().getPseudo(),
                        scoreModel.getCircuit(),
                        scoreModel.getBestScore(),
                        scoreModel.getCurrentScore(),
                        scoreModel.getSpendTime(),
                        scoreModel.getBestTime()
                )
        ).collect(Collectors.toList());
    }

    /**
     * Get all scores of all friend ordered by module.
     * Friends are ordered by the best average, then the higher time spend
     *
     * @return Returns the list of friends with their scores
     */
    public List<FriendScoreEntity> getFriendsScoreOrderedByModule(final ModuleType moduleType) throws CodeMessageException {
        final Long currentUserId = Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getName());
        final UserModel currentUserModel = userRepository.findById(currentUserId).orElseThrow(() ->
                new CodeMessageException(CodeMessageExceptionEnum.SESSION_EXPIRED)
        );
        final List<RelationModel> relationModels = relationRepository.findAllByUserModelAndByType(
                currentUserModel, UserType.STUDENT
        );

        final List<FriendScoreEntity> friendScoreEntities = relationModels.stream().map(relationModel -> {
            final UserModel friendModel = relationModel.getRelationUserModel();
            final List<ScoreModel> scores = scoreRepository.findByUserAndModule(friendModel, moduleType);
            final Integer average = scores.size() > 0
                    ? scores.stream().map(ScoreModel::getBestScore).reduce(0, Integer::sum) / scores.size()
                    : 0;

            return new FriendScoreEntity(
                    friendModel.getPseudo(),
                    average,
                    scores.stream().map(scoreModel ->
                            new FriendScoreEntity.ScoreEntity(
                                    scoreModel.getCircuit().getName(),
                                    scoreModel.getBestScore(),
                                    scoreModel.getSpendTime(),
                                    scoreModel.getBestTime()
                            )
                    ).collect(Collectors.toList())
            );
        }).collect(Collectors.toList());

        final List<ScoreModel> currentUserScores = scoreRepository.findByUserAndModule(currentUserModel, moduleType);

        final FriendScoreEntity currentUserFriendScoresEntity = new FriendScoreEntity(
                currentUserModel.getPseudo(),
                getScoresAverage(currentUserScores),
                currentUserScores.stream().map(scoreModel ->
                        new FriendScoreEntity.ScoreEntity(
                                scoreModel.getCircuit().getName(),
                                scoreModel.getBestScore(),
                                scoreModel.getSpendTime(),
                                scoreModel.getBestTime()
                        )
                ).collect(Collectors.toList())
        );

        friendScoreEntities.add(currentUserFriendScoresEntity);

        return friendScoreEntities.stream()
                .sorted((friendA, friendB) -> {
                    final int comparison = friendB.average.compareTo(friendA.average);

                    if (comparison != 0) return comparison;

                    return getSpendTimeInModule(friendB).compareTo(getSpendTimeInModule(friendA));
                }).collect(Collectors.toList());
    }

    private Integer getSpendTimeInModule(final FriendScoreEntity friendScoreEntity) {
        return friendScoreEntity.scores.stream()
                .map(scoreEntity -> scoreEntity.spendTime)
                .reduce(0, Integer::sum);
    }

    private Integer getScoresAverage(final List<ScoreModel> scoreModels) {
        return scoreModels.size() > 0
                ? scoreModels.stream().map(ScoreModel::getBestScore).reduce(0, Integer::sum) / scoreModels.size()
                : 0;
    }
}
