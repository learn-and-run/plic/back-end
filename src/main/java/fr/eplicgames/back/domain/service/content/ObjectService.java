package fr.eplicgames.back.domain.service.content;

import fr.eplicgames.back.domain.entity.content.ObjectEntity;
import fr.eplicgames.back.exception.CodeMessageException;
import fr.eplicgames.back.exception.CodeMessageExceptionEnum;
import fr.eplicgames.back.persistence.model.content.ObjectModel;
import fr.eplicgames.back.persistence.repository.content.ObjectRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ObjectService {

    final ObjectRepository objectRepository;

    /**
     * Create an object in database
     *
     * @param objectEntity The new object to create
     * @return Return the created object
     * @throws CodeMessageException Throws an exception if the object already exists
     */
    public ObjectEntity createObject(final ObjectEntity objectEntity) throws CodeMessageException {
        final ObjectModel objectModel = new ObjectModel(objectEntity.name, objectEntity.folder, objectEntity.inGame);
        return saveObject(objectModel);
    }

    /**
     * Update an object from database
     *
     * @param objectEntity The updated object entity
     * @return Return the updated object
     * @throws CodeMessageException Throws an exception if the new name is already taken or if the uuid does not exist
     */
    public ObjectEntity updateObject(final ObjectEntity objectEntity) throws CodeMessageException {

        if (!objectRepository.existsById(objectEntity.uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.OBJECT_NOT_EXIST);

        final ObjectModel objectModel = new ObjectModel(
                objectEntity.uuid,
                objectEntity.name,
                objectEntity.folder,
                objectEntity.inGame
        );

        return saveObject(objectModel);
    }

    /**
     * Save the object in database
     *
     * @param objectModel The object model to save
     * @return Returns the saved object
     * @throws CodeMessageException Throws an exception if the name is already taken
     */
    private ObjectEntity saveObject(final ObjectModel objectModel) throws CodeMessageException {
        if (objectRepository.existsByName(objectModel.getName()))
            throw new CodeMessageException(CodeMessageExceptionEnum.OBJECT_ALREADY_EXIST);

        final ObjectModel newObjectModel = objectRepository.save(objectModel);

        return new ObjectEntity(
                newObjectModel.getUuid(),
                newObjectModel.getName(),
                newObjectModel.getFolder(),
                newObjectModel.isInGame()
        );
    }

    /**
     * Delete an object with its uuid
     *
     * @param uuid The uuid of the object to delete
     * @throws CodeMessageException Throws an exception if the uuid does not exist
     */
    public void deleteObjectByUuid(final UUID uuid) throws CodeMessageException {
        if (!objectRepository.existsById(uuid))
            throw new CodeMessageException(CodeMessageExceptionEnum.OBJECT_NOT_EXIST);

        objectRepository.deleteById(uuid);
    }

    /**
     * Get all objects in database
     *
     * @return Returns objects in database
     */
    public List<ObjectEntity> getObjects() {
        return objectRepository.findAll().stream().map(objectModel -> new ObjectEntity(
                objectModel.getUuid(),
                objectModel.getName(),
                objectModel.getFolder(),
                objectModel.isInGame()
        )).collect(Collectors.toList());
    }
}
