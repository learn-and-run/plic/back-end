package fr.eplicgames.back.domain.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class FriendScoreEntity {
    String pseudo;
    Integer average;

    List<ScoreEntity> scores;

    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PUBLIC)
    public static class ScoreEntity {
        String circuitName;
        Integer bestScore;

        Integer spendTime;
        Integer bestTime;
    }
}
