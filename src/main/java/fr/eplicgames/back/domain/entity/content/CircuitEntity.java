package fr.eplicgames.back.domain.entity.content;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class CircuitEntity {
    Long id;
    String name;
    String pictureName;

    ModuleType module;
    SceneEntity scene;

    Set<RoomEntity> rooms;
}
