package fr.eplicgames.back.domain.entity.content;

import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class QuestionEntity {
    Long id;
    String name;

    String question;
    String response;
    String badAnswer;
    String explanation;

    String clue1;
    String clue2;
    String clue3;
    String clue4;

    ModuleType module;
    LevelType level;
    Integer difficulty;
}
