package fr.eplicgames.back.domain.entity.relation;

import fr.eplicgames.back.domain.entity.user.UserEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class RelationEntity {
    Long id;
    UserEntity userEntity;
}
