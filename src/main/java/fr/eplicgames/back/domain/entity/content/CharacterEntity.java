package fr.eplicgames.back.domain.entity.content;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class CharacterEntity {
    Long id;
    String name;
    String description;
    String background;

    Boolean inGame;
    Integer sorted;

    String smallImage;
    String bigImage;
}
