package fr.eplicgames.back.domain.entity;

import fr.eplicgames.back.persistence.model.user.UserModel;
import fr.eplicgames.back.sql_type.IdentityType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class GamePreferenceEntity {
    final Long id;
    UserModel user;
    Boolean controllerDualShock;
    Boolean retro;
    IdentityType identity;
}
