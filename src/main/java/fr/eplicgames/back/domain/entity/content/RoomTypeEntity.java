package fr.eplicgames.back.domain.entity.content;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class RoomTypeEntity {
    UUID uuid;
    String type;
}
