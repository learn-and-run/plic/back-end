package fr.eplicgames.back.domain.entity.user;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class UserEntity {
    Long id;
    String pseudo;
    String firstname;
    String lastname;

    String email;
    String password;

    CharacterEntity character;

    Boolean active;
    UserType userType;
}
