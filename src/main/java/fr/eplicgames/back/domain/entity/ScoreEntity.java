package fr.eplicgames.back.domain.entity;

import fr.eplicgames.back.persistence.model.content.CircuitModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class ScoreEntity {
    String pseudo;
    CircuitModel circuitModel;

    Integer bestScore;
    Integer currentScore;

    Integer spendTime;
    Integer bestTime;
}
