package fr.eplicgames.back.domain.entity.user;

import fr.eplicgames.back.domain.entity.content.CharacterEntity;
import fr.eplicgames.back.sql_type.LevelType;
import fr.eplicgames.back.sql_type.UserType;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class StudentEntity extends UserEntity {

    LevelType levelType;

    public StudentEntity(final Long id,
                         final String pseudo,
                         final String firstname,
                         final String lastname,
                         final String email,
                         final String password,
                         final CharacterEntity character,
                         final Boolean active,
                         final LevelType levelType) {
        super(id, pseudo, firstname, lastname, email, password, character, active, UserType.STUDENT);
        this.levelType = levelType;
    }
}
