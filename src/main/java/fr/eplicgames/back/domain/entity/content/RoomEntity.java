package fr.eplicgames.back.domain.entity.content;

import fr.eplicgames.back.sql_type.ModuleType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class RoomEntity {
    Long id;

    ModuleType module;
    QuestionEntity question;

    // In milliseconds
    Integer expectedTime;

    RoomTypeEntity roomType;
}
