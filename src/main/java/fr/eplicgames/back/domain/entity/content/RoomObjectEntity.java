package fr.eplicgames.back.domain.entity.content;

import fr.eplicgames.back.utils.Vector3D;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PUBLIC)
public class RoomObjectEntity {
    ObjectEntity object;
    Vector3D<Float> position;
    Vector3D<Float> rotation;
}
