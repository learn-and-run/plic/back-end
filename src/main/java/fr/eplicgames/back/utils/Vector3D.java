package fr.eplicgames.back.utils;

public class Vector3D<TYPE_NUMBER extends Number> {
    public TYPE_NUMBER x;
    public TYPE_NUMBER y;
    public TYPE_NUMBER z;

    /**
     * Constructor of vector 3D that define (x, y, z)
     *
     * @param x the x coordinate
     * @param y the x coordinate
     * @param z the x coordinate
     */
    public Vector3D(final TYPE_NUMBER x, final TYPE_NUMBER y, final TYPE_NUMBER z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Generate a default Int Vector 3D
     *
     * @return Return the vector 3D (0, 0, 0)
     */
    public static Vector3D<Integer> intVectorZero() {
        return new Vector3D<>(0, 0, 0);
    }

    /**
     * Generate a default Float Vector 3D
     *
     * @return Return the vector 3D (0F, 0F, 0F)
     */
    public static Vector3D<Float> floatVectorZero() {
        return new Vector3D<>(0F, 0F, 0F);
    }

    /**
     * Generate a default Double Vector 3D
     *
     * @return Return the vector 3D (0F, 0F, 0F)
     */
    public static Vector3D<Double> doubleVectorZero() {
        return new Vector3D<>(0D, 0D, 0D);
    }

    /**
     * Generate a default Long Vector 3D
     *
     * @return Return the vector 3D (0F, 0F, 0F)
     */
    public static Vector3D<Long> longVectorZero() {
        return new Vector3D<>(0L, 0L, 0L);
    }
}
