package fr.eplicgames.back.utils;

public class Score {

    /**
     * Calculate the score when a player succeed a room
     *
     * @param spendTime    Time spend in the room in milliseconds
     * @param expectedTime Time expected to do the room in milliseconds
     * @return Returns the score calculated with given parameters
     */
    public static int calculateScore(final Integer spendTime, final Integer expectedTime) {
        int score = 100;

        if (spendTime > expectedTime) {
            final float deficitCoeff = (float) Math.min(spendTime - expectedTime, expectedTime) / (float) expectedTime;
            score -= 50F * deficitCoeff;
        }
        return score;
    }
}
