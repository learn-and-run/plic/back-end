package fr.eplicgames.back.sql_type;

/**
 * SQL Enum for module
 */
public enum ModuleType {
    GENERAL,
    MATHS,
    FRANCAIS,
    SVT,
    PHYSIQUE,
    CHIMIE,
    ANGLAIS,
    HISTOIRE,
    GEOGRAPHIE,
    TECHNOLOGIE,
    TOUS
}
