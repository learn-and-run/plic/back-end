package fr.eplicgames.back.sql_type;

/**
 * SQL enum for identity type to display identity correctly
 */
public enum IdentityType {
    PSEUDO,
    FIRSTNAME,
    FULLNAME
}
