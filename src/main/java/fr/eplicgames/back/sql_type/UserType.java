package fr.eplicgames.back.sql_type;

/**
 * SQL Enum for user
 */
public enum UserType {

    STUDENT,
    PARENT,
    PROFESSOR

}
