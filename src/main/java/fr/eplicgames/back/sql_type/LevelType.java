package fr.eplicgames.back.sql_type;

/**
 * SQL Enum for level
 */
public enum LevelType {

    SIXIEME,
    CINQUIEME,
    QUATRIEME,
    TROISIEME,
    SECONDE,
    PREMIERE,
    TERMINALE

}
