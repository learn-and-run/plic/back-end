# Learn & Run : Back-End API

[TOC]

Here is the API of the back-end

# Server domain

Le nom de domaine sera noté [DOMAIN]

# Endpoints

Tous les endpoints se base sur l'url suivante:
`https://[DOMAIN]/api`

## User Endpoints

### User info
> Endpoint `/user/info`

> Méthode : **GET**

> Description : Cette API permet de récupérer des informations relatives à l'utilisateur

Réponse :
```json
{
    "pseudo": "JeanJacquelin",
    "firstname": "Nicolas",
    "lastname": "Acart",
    "email": "nico.acart@gmail.com",
    "birthdate": "20//09/1998",
    "user_type": "STUDENT"
}
```

### User friends
> Endpoint `/user/friends`

> Méthode : **GET**

> Description : Cette API permet de récupérer la liste des amis de l'utilisateur

Réponse :
```json
{
    "friends": [
        {
            "pseudo": "Ryubi"
        },
        {
            "pseudo": "Bigfoot"
        },
        ...
    ]
}
```

### User parents
> Endpoint `/user/parents`

> Méthode : **GET**

> Description : Cette API permet de récupérer la liste des parents de l'utilisateur (si il est un étudiant)

Réponse :
```json
{
    "parents": [
        {
            "pseudo": "parent1"
        },
        {
            "pseudo": "parent2"
        },
        ...
    ]
}
```

### User professors
> Endpoint `/user/professors`

> Méthode : **GET**

> Description : Cette API permet de récupérer la liste des professeurs de l'utilisateur (si il est un étudiant)

Réponse :
```json
{
    "professors": [
        {
            "pseudo": "professor1"
        },
        {
            "pseudo": "professor2"
        },
        ...
    ]
}
```

### User Statistics
#### User circuits
> Endpoint `/user/statistics/{pseudo}/circuits`

> Méthode : **GET**

> Description : Cette API permet de récupérer la liste des statistiques des circuits de l'utilisateur (si il est un étudiant)

Réponse :
```json
{
    "circuits": {
        "circuit": "Circuit 1",
        "score": "12"
    }
}
```

### Questions
> Endpoint `/question/list`

> Méthode : **GET**

> Description : Liste tous les formats de questions

Réponse :
```json
{
    "questions": [
        {
            "question": "2 + 2 = ?",
            "response": "4",
            "explanation": "C'est une addition donc bon voila quoi",
            "clue1": "premier indice",
            "clue2": "deuxième indice",
            "clue3": "troisième indice",
            "clue4": "quatrième indice",
            "module": "MATHS",
            "level": "SIXIEME"
        },
        ...
    ]
}
```